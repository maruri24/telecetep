<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailSender extends Mailable
{
    use Queueable, SerializesModels;


    public $paciente;
    public $hora;
    public $prestador;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($paciente, $hora)
    {
        $this->paciente = $paciente;
        $this->hora = $hora;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('reserva@telecetep.cl')->view('mails.correo_reserva');
    }
}
