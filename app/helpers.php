<?php

use App\Http\Controllers\PacienteController;
use Illuminate\Support\Facades\Crypt;

function calcularEdad($fechaNacimiento){
    $fechaNacimiento = new DateTime($fechaNacimiento);
    $currentDate = new DateTime();

    return $currentDate->diff($fechaNacimiento);
}

function formatoRut($rut){
    return PacienteController::formatoRut($rut);
}

function getEstadoCivil($idEstadoCivil){
    $estadoCivil = PacienteController::getEstadoCivil($idEstadoCivil);
    return replaceIfNull($estadoCivil);
}

function getIsapre($idIsapre){
    $isapre = PacienteController::getIsapre($idIsapre);
    return replaceIfNull($isapre);
}

function getGenero($idGenero){
    $genero = PacienteController::getGenero($idGenero);
    return replaceIfNull($genero);
}

function getComuna($comuna){
    $comuna = PacienteController::getComuna($comuna);
    return $comuna ? replaceIfNull($comuna->NombreComuna) : replaceIfNull(null);
}

function getFechaNacimiento($fechaNacimiento){
    return $fechaNacimiento === '0000-00-00' || $fechaNacimiento == null ? 'No especifica' : date('d-m-Y', strtotime($fechaNacimiento)) ;
}

function replaceIfNull($value){
    return $value == null ? 'No especifica' :$value;
}

function encriptar($value){
    return Crypt::encrypt($value);
}