<?php
namespace App\Notifications;
use App\Entities\Paciente;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
class ConfirmacionHora extends Notification implements ShouldQueue
{
    use Queueable;
    use Notifiable;
    protected $detalleReserva;
    protected $email;
    /**
    * Create a new notification instance.
    *
    * @return void
    */
    public function __construct($email_telecetep, $detalleReserva)
    {
        $this->email = $email_telecetep;
        $this->detalleReserva  = (object) $detalleReserva;
    }
    /*/
    public function toDatabase($notifiable)
    {

        return [
            'detalleReserva' =>  $this->detalleReserva
        ];

    }
    /*/
    /**
    * Get the notification's delivery channels.
    *
    * @param  mixed  $notifiable
    * @return array
    */

    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
    * Get the mail representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return \Illuminate\Notifications\Messages\MailMessage
    */
    public function toMail($notifiable)
    {
        $notifiable->email = $this->email;
        return (new MailMessage)
            ->bcc('dti@cetep.cl', 'Departamento de tecnoglogía informatica')
            ->bcc('servicioalcliente@cetep.cl', 'Servicio al cliente')
            ->bcc('coordinacionusl@cetep.cl', 'Coordinacion USL')
            ->bcc('uac@cetep.cl', 'Centro médico CETEP')
            ->subject('Detalle hora de atención')
            ->markdown('vendor.notifications.confirmacion_hora', ['detalleReserva' => $this->detalleReserva])
            ->line('Usted ha agendado una hora de '.$this->detalleReserva->especialidad.'. El detalle es el siguiente:');

    }
/**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}