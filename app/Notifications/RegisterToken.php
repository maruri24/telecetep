<?php
namespace App\Notifications;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
class RegisterToken extends Notification implements ShouldQueue
{
    use Queueable;
    protected $token;
    protected $user;
    /**
    * Create a new notification instance.
    *
    * @return void
    */
    public function __construct($token, $user)
    {
        $this->token = $token;
        $this->user  = $user;
    }
    /**
    * Get the notification's delivery channels.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function via($notifiable)
    {
        return ['mail'];
    }
    /**
    * Get the mail representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return \Illuminate\Notifications\Messages\MailMessage
    */
    public function toMail($notifiable)
    {

        $url = url('/activar_cuenta/'.$this->token);
        return (new MailMessage)
            ->subject('Activación de cuenta Telecetep')
            ->line('Te registraste recientemente en Telecetep. Para completar tu registro confirma tu cuenta en el siguiente enlace:')
            ->action('Activar Cuenta', url($url))
            ->markdown('vendor.notifications.activar_cuenta', ['url' => url($url), 'user' => $this->user]);
    }
    
    /**
    * Get the array representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}