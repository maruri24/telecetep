<?php
namespace App\Notifications;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
class Correo extends Notification implements ShouldQueue
{
    use Queueable;
    protected $detalleReserva;
    /**
    * Create a new notification instance.
    *
    * @return void
    */
    public function __construct($detalleReserva)
    {
        $this->detalleReserva  = (object) $detalleReserva;                                                       
    }
    /*/
    public function toDatabase($notifiable)
    {

        return [
            'detalleReserva' =>  $this->detalleReserva
        ];

    }
    /*/
    /**
    * Get the notification's delivery channels.
    *
    * @param  mixed  $notifiable
    * @return array
    */

    public function via($notifiable)
    {
        return ['mail'];
    }



    /**
    * Get the mail representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return \Illuminate\Notifications\Messages\MailMessage
    */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Detalle hora de atención')
            ->markdown('vendor.notifications.confirmacion_hora', ['detalleReserva' => $this->detalleReserva])
            ->line('Usted ha agendado una hora de telepsicología. El detalle es el siguiente:');

    }
/**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}