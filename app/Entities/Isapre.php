<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Isapre extends Model
{
    //
    protected $table = 'isapre';
    public $timestamps = false;

    // Una isapre tiene muchos pacientes.
    public function pacientes(){
        return $this->hasMany(Paciente::class);
    }
}
