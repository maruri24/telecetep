<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Comuna extends Model
{
    //
    protected $table = 'comuna';
    protected $primaryKey = 'codcomuna';
    public $timestamps = false;
}
