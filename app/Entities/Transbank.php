<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Transbank extends Model
{
    protected $table = 'registroTransbank';
    public $timestamps = false;
    protected $primaryKey = 'idregistrotransbank';
    protected $fillable = [
        'accountingData',
        'buyOrder',
        'idhora',
        'idpaciente',
        'cardNumber',
        'cardExperidationDate',
        'authorizationCode',
        'paymentTypecode',
        'respondeCode',
        'sharesNumber',
        'amount',
        'commerceCode',
        'detailbuyOrder',
        'sessionId',
        'transactionDate',
        'urlDirection',
        'vci'
    ];
}
