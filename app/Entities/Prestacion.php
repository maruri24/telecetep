<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Prestacion extends Model
{
    protected $table = 'prestacion_achs';
    protected $primaryKey = 'idprestacion';
    public $timestamps = false;

    public function convenios(){
        return $this->belongsToMany(Convenio::class, 'prestacion_programa_achs', 'idprestacion', 'idconvenio');
    }

    public function prestadores(){
        return $this->belongsToMany(Prestador::class, 'convenioPrestacionPrograma', 'idprestacion', 'idprestador');
    }

    public function especialidad(){
        return $this->belongsToMany(Especialidad::class, 'prestacion_programa_achs', 'idprestacion', 'idespecialidad');
    }

}
