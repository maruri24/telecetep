<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DetalleRegla extends Model
{
    protected $table = 'detalleregla';
    protected $primaryKey = 'idetalleregla';
    public $timestamps = false;
}
