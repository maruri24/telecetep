<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ConvenioPrestacion extends Model
{
    protected $table = 'convenioPrestacionPrograma';
    public $timestamps = false;

    public function prestacion(){
        return $this->hasMany(Prestacion::class, 'idprestacion');
    }

    public function prestador(){
        return $this->hasMany(Prestador::class, 'idprestador');
    }
}
