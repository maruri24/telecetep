<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Hora extends Model
{
    protected $table = 'hora';
    public $timestamps = false;
}
