<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Prestador extends Authenticatable
{
    use Notifiable;

    public $timestamps = false;
    protected $guard = 'prestador';
    protected $table = 'prestador';
    protected $fillable = [
        'nombres',
        'apellidoPaterno',
        'apellidoMaterno',
        'email',
        'celular',
        'password'
    ];

    public function prestaciones(){
        return $this->belongsToMany(Prestacion::class, 'convenioPrestacionPrograma', 'idprestador', 'idprestacion');
    }

    public function convenios(){
        return $this->belongsToMany(Convenio::class, 'convenioPrestador', 'idprestador', 'idconvenio');
    }
    public function sedes(){
        return $this->belongsToMany(Sede::class, 'prestador_especialidad', 'idprestador', 'idsede');
    }

    /**
     * Overrides the method to ignore the remember token.
     */
    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
            parent::setAttribute($key, $value);
        }
    }

}
