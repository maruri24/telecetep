<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'hora';
    public $timestamps = false;
}
