<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ConvenioPaciente extends Model
{
    public $timestamps = false;
    protected $table = 'convenioPaciente';
    protected $fillable = [
        'idpaciente',
        'idconvenio'
    ];

}
