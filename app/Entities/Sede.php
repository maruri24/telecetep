<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Sede extends Model
{
    //
    protected $table = 'sede';
    protected $primaryKey = 'idsede';
    public $timestamps = false;
}
