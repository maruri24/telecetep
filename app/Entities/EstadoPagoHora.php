<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class EstadoPagoHora extends Model
{
    //
    public $timestamps = false;
    protected $table = 'estado_pago_hora';
    protected $primaryKey = 'id_estado_pago_hora';
    protected $fillable = [
        'id_hora',
        'id_registro_transbank',
        'id_registro_bono',
        'id_estado_pago',
        'id_medio_pago',
        'monto',
        'fecha'
    ];
}
