<?php

namespace App\Entities;
use Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;

    protected $guard = 'web';
    protected $table = 'users';

    public $fillable = [
		'idpaciente',
		'rut',
		'name',
		'email',
		'email_verified_at',
		'password',
		'remember_token'
    ];    

    /**
     * Overrides the method to ignore the remember token.
     */
	public function paciente()
	{
	    return $this->belongsTo(Paciente::class, 'idpaciente');
	}   

    /**
     * Overrides the method to ignore the remember token.
     */
    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
            parent::setAttribute($key, $value);
        }
    }
}
