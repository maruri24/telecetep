<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class RegistroTransbank extends Model
{
    protected $table = 'registroTransbank';
    protected $primaryKey = 'idregistrotransbank';
    public $timestamps = false;
    protected $fillable = [
        'accountingDate',
        'buyOrder',
        'idhora',
        'idpaciente',
        'cardNumber',
        'cardExpirationDate',
        'authorizationCode',
        'paymentTypeCode',
        'responseCode',
        'sharesNumber',
        'amount',
        'commerceCode',
        'detailbuyOrder',
        'sessionId',
        'transactionDate',
        'urlRedirection',
        'vci'
    ];
}
