<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ConvenioPrestador extends Model
{
    protected $table = 'convenioPrestador';
    public $timestamps = false;

    public function convenio(){
        return $this->hasMany(Convenio::class, 'idconvenio');
    }

    public function prestador(){
        return $this->hasMany(Prestador::class, 'idprestador');
    }
}
