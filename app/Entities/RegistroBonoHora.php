<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class RegistroBonoHora extends Model
{
    //
    protected $table = 'registroBonoHora';
    protected $primaryKey = 'idRegBonoHora';
    public $timestamps = false;
    protected $fillable = [
        'idhora',
        'idbono',
        'create_at',
        'monto',
        'fechabono',
        'archivo',
        'id_proceso_estado',
        'extension'
    ];
}
