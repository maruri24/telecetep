<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Convenio extends Model
{
    protected $table = 'convenio';
    protected $primaryKey = 'idConvenio';
    public $timestamps = false;

    public function pacientes(){
        return $this->belongsToMany(Paciente::class, 'convenioPaciente', 'idconvenio', 'idpaciente');
    }

    public function prestaciones(){
        return $this->belongsToMany(Prestacion::class, 'prestacion_programa_achs', 'idconvenio', 'idprestacion');
    }

    public function prestadores(){
        return $this->belongsToMany(Prestador::class, 'convenioPrestador', 'idconvenio', 'idprestador');
    }

    public function reglas(){
        return $this->belongsToMany(Regla::class, 'detalleregla', 'idconvenio', 'idregla');
    }
}
