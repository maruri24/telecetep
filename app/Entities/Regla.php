<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Regla extends Model
{
    protected $table = 'regla';
    protected $primaryKey = 'idregla';
    public $timestamps = false;

    public function detallesreglas(){
        return $this->belongsTo(DetalleRegla::class, 'iddetalleregla', 'idregla');
    }
}
