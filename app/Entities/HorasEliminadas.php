<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class HorasEliminadas extends Model
{
    protected $table = 'horas_eliminadas';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'fecha',
        'idHora',
        'hora',
        'ciudad',
        'paciente',
        'prestador',
        'motivoEliminacion',
        'motivoOtro',
        'prestacion'
    ];
}
