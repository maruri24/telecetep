<?php

namespace App\Entities;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Paciente extends Authenticatable
{
    use Notifiable;

    public $timestamps = false;
    protected $guard = 'paciente';
    protected $table = 'paciente';
    protected $primaryKey = 'id';
    protected $fillable = [
        'rut',
        'nombres',
        'apellidoPaterno',
        'apellidoMaterno',
        'email',
        'telefono',
        'celular',
        'password',
        'contactoEmergencia_1',
        'isapre'
    ];

    /**
     * Overrides the method to ignore the remember token.
     */
    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
            parent::setAttribute($key, $value);
        }
    }

    public function convenios(){
        return $this->belongsToMany(Convenio::class, 'convenioPaciente', 'idpaciente', 'idconvenio');
    }

    public function pacienteComuna(){
        return $this->hasOne(Comuna::class, 'codcomuna', 'comuna');
    }
}
