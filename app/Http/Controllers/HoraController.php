<?php

namespace App\Http\Controllers;

use App\Entities\Convenio;
use App\Entities\DetalleRegla;
use App\Entities\EstadoPagoHora;
use App\Entities\Hora;
use App\Entities\HorasEliminadas;
use App\Entities\Prestacion;
use App\Entities\Prestador;
use App\Entities\Paciente;
use App\Entities\registroBonoHora;
use App\Entities\Regla;
use App\Notifications\ConfirmacionHora;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use MongoDB\Driver\Session;
use Transbank\Webpay\Configuration;
use Transbank\Webpay\Webpay;
use Carbon\Carbon;

class HoraController extends Controller
{

    /**
     * Al ingresar a la vista Agenda, se recupera la informacion
     * necesaria para desplegar en dicha pantalla.
     *
     * @param Paciente $paciente
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reservaHora(){
        $paciente = Auth::guard('paciente')->user()->paciente;
        $location = 'all';
        if($paciente->pacienteComuna !== null){
            switch ($paciente->pacienteComuna->codregion){
                case '13':
                    $location = 1;
                    break;
                case '6':
                    $location = 3;
                    break;
                case '8':
                    $location = 4;
                    break;
            }
        }
        // Se limitan los agendamientos solo a Las Condes
        $location = '1';

        $convenios = ConvenioController::getConveniosPaciente($paciente);
        $prestaciones = ConvenioController::getPrestacionesConvenio($convenios->first());
        $prestadores = ConvenioController::getPrestadoresPrestacion($prestaciones->first(), $convenios->first(), $location);
        /*----------------------------------------------------------------------*/

        /*Fechas disponibles*/
        $fechas_disponibles = PrestadorController::getFechasDisponiblesPrestacion($convenios->first(), $prestaciones->first(), $prestadores);
        $proxima_hora_disponible = PrestadorController::getProxHoraDisponible($convenios->first(), $prestaciones->first(), $prestadores, $paciente);

        return view('reservaPaciente', compact('paciente', 'location', 'convenios', 'prestaciones', 'prestadores', 'fechas_disponibles', 'proxima_hora_disponible'));
    }

    public function agendaPrestadorAjax(Request $request){
        $paciente = Auth::guard('paciente')->user()->paciente;
        if($request->idPrestador == 'x'){
            $prestadores = ConvenioController::getPrestadoresPrestacion(Prestacion::find($request->idPrestacion), Convenio::find($request->idConvenio), $request->location);
        } else{
            $prestadores = Prestador::find($request->idPrestador);
        }

        /*Fechas disponibles*/
        $fechas_disponibles = PrestadorController::getFechasDisponiblesPrestacion(Convenio::find($request->idConvenio), Prestacion::find($request->idPrestacion), $prestadores);
        $proxima_hora_disponible = PrestadorController::getProxHoraDisponible(Convenio::find($request->idConvenio), Prestacion::find($request->idPrestacion), $prestadores, $paciente);

        return response()->json(array(true, $fechas_disponibles, $proxima_hora_disponible));
    }

    public function horasAjax(Request $request){
        if($request->idPrestador == 'x'){
            $prestadores = ConvenioController::getPrestadoresPrestacion(Prestacion::find($request->idPrestacion), Convenio::find($request->idConvenio), $request->location);
        } else{
            $prestadores = Prestador::find($request->idPrestador);
        }

        $horas_paciente = HoraController::horasPaciente($request->idPaciente);
        $horas_disponibles = PrestadorController::getHorasDisponibles(Convenio::find($request->idConvenio), Prestacion::find($request->idPrestacion), $prestadores, $request->fecha);

        foreach ($horas_disponibles as $hDisponible){
            $hDisponible->bloquear = false;
            foreach ($horas_paciente as $hPaciente){
                $fromHour = date('Y-m-d H:i:s',strtotime('-1 hour -30 minutes',strtotime($hPaciente->hora)));
                $toHour = date('Y-m-d H:i:s',strtotime('+1 hour +30 minutes',strtotime($hPaciente->hora)));
                $hourToCompare = date('Y-m-d H:i:s', strtotime($hDisponible->horaCompleta));
                if($hourToCompare >= $fromHour && $hourToCompare <= $toHour){
                    $hDisponible->bloquear = true;
                }
            }
        }

        return response()->json(array($horas_disponibles, $horas_paciente));
    }

    public function validarStillAvailableAjax(Request $request){
        /**
         * Validar si la hora sigue estando disponible
         */
        $hora = Hora::find($request->idHora);

        if(!is_null($hora->paciente)){
            return response()->json(array(0));
        }
        return response()->json(array(1));
    }

    public function validarReservaConReglasAjax(Request $request){
        /**
         * Primero veremos si la hora sigue estando disponible
         */
        $hora = Hora::find($request->idHora);
//        dd($hora->paciente);
        if(!is_null($hora->paciente)){
            return response()->json(array(-1));
        }

        /**
         * Vamos a validar si el usuario puede realizar el agendamiento
         * tomando en cuenta la cantidad de horas que ya tiene reservadas
         * y las que el convenio le permite agendar.
         */
        $regla = DetalleRegla::where( 'idregla', '=',4)->where('idconvenio', '=', $request->idConvenio)->first();
        $countReservas = $this->cantidadHorasReservadas(Paciente::find($request->idPaciente), $request->idConvenio);
        $flag = $countReservas >= $regla->valor ? 0 : 1;

        $idhora = Crypt::encrypt($request->idHora);
        $idPrestacion = Crypt::encrypt($request->idPrestacion);
        $idConvenio = Crypt::encrypt($request->idConvenio);

        return response()->json(array($flag, $countReservas, $regla, $idhora, $idPrestacion, $idConvenio));
    }

    public function detalleReserva($idhora, $idPrestacion, $idConvenio, Request $parametros){
        $idhoraEncrypted = $idhora;
        $idhora = Crypt::decrypt($idhora);
        $prestacion = Prestacion::find(Crypt::decrypt($idPrestacion));
        $convenio = Convenio::find(Crypt::decrypt($idConvenio));
        $paciente = Auth::guard('paciente')->user()->paciente;

        $detalleReserva = Hora::from('hora as hr')
            ->Join('prestador as pre', 'hr.prestador', '=', 'pre.id')
            ->Join('especialidad as esp', 'hr.especialidad', '=', 'esp.id')
            ->select(
                "hr.id as idHora",
                DB::raw("DATE_FORMAT(hr.hora,'%d-%m-%Y') as fecha"),
                DB::raw("TIME_FORMAT(hr.hora, '%H:%i %p') as hora"),
                "pre.id as idPrestador",
                DB::raw("CONCAT(pre.nombres, ' ', pre.apellidoPaterno, ' ', pre.apellidoMaterno) AS nombrePrestador"),
                "esp.id as idEspecialidad",
                "esp.especialidad")
            ->where('hr.id', '=', $idhora)
            ->first();

        $sessionId = "pago_telecetep";
        $idhorapaciente = $idhora.$paciente->id;

        $buyOrder = ($idhorapaciente);
        $entorno = 2; //testing 0; QA 1; PROD 2

        if($entorno == 0){
            //API aranceles UAC
            $resultAPI = 'https://cetep.cl/qa/uac/apiArancel.php?idIsapre='.$paciente->isapre.'&idPrestacion='.$prestacion->idprestacion.'&idConvenio='.$convenio->idConvenio.'';
            $data = json_decode(file_get_contents($resultAPI), true);
            $amount = $data["valorArancel"];

            //URL testing webpay
            $transaction = (new Webpay(Configuration::forTestingWebpayPlusNormal()))->getNormalTransaction();

            $returnUrl = "https://telecetep.test/comprobante/returnpago/".$idhoraEncrypted."/".$convenio->idConvenio."/".$prestacion->idprestacion;
            $finalUrl = "https://telecetep.test/comprobante/returnsuccess/".$idhoraEncrypted."/".$convenio->idConvenio."/".$prestacion->idprestacion;

            $initResult = $transaction->initTransaction($amount, $buyOrder, $sessionId, $returnUrl, $finalUrl);
        } elseif($entorno == 1){
            //API aranceles UAC
            $resultAPI = 'https://cetep.cl/qa/uac/apiArancel.php?idIsapre='.$paciente->isapre.'&idPrestacion='.$prestacion->idprestacion.'&idConvenio='.$convenio->idConvenio.'';
            $data = json_decode(file_get_contents($resultAPI), true);
            $amount = $data["valorArancel"];

            //URL QA webpay
            $transaction = (new Webpay(Configuration::forTestingWebpayPlusNormal()))->getNormalTransaction();

            $returnUrl = "https://test.telecetep.cl/comprobante/returnpago/".$idhoraEncrypted."/".$convenio->idConvenio."/".$prestacion->idprestacion;
            $finalUrl = "https://test.telecetep.cl/comprobante/returnsuccess/".$idhoraEncrypted."/".$convenio->idConvenio."/".$prestacion->idprestacion;

            $initResult = $transaction->initTransaction($amount, $buyOrder, $sessionId, $returnUrl, $finalUrl);
        }
        elseif($entorno == 2){
            //API aranceles UAC
            $resultAPI = 'https://cetep.cl/uac/apiArancel.php?idIsapre='.$paciente->isapre.'&idPrestacion='.$prestacion->idprestacion.'&idConvenio='.$convenio->idConvenio.'';
            $data = json_decode(file_get_contents($resultAPI), true);
            $amount = $data["valorArancel"];

            //Configuration Pro
            $configuration = new Configuration();
            $configuration->setEnvironment('PRODUCCION');
            $configuration->setCommerceCode(597035456341);
            $publiccert = 'https://www.cetep.cl/tbank/597035456341.crt';
            $privatekey = 'https://www.cetep.cl/tbank/597035456341.key';
            $configuration->setPublicCert(file_get_contents($publiccert));
            $configuration->setPrivateKey(preg_replace('~[\r]+~','',file_get_contents($privatekey)));

            //URL produccion
            $transaction = (new WebPay($configuration))->getNormalTransaction();

            $returnUrl = "https://telecetep.cl/comprobante/returnpago/".$idhoraEncrypted."/".$convenio->idConvenio."/".$prestacion->idprestacion;
            $finalUrl = "https://telecetep.cl/comprobante/returnsuccess/".$idhoraEncrypted."/".$convenio->idConvenio."/".$prestacion->idprestacion;

            $initResult = $transaction->initTransaction($amount, $buyOrder, $sessionId, $returnUrl, $finalUrl);
        }

        $parametros->monto = $amount;
        $parametros->ordencompra = $buyOrder;

        $permitePagoWebpay = DetalleRegla::where( 'idregla', '=', 3)->where('idconvenio', '=', $convenio->idConvenio)->first();
        $permitePagoBono = DetalleRegla::where( 'idregla', '=', 10)->where('idconvenio', '=', $convenio->idConvenio)->first();

        return view('detalleReserva', compact('paciente', 'prestacion', 'convenio', 'detalleReserva', 'idhoraEncrypted', 'permitePagoBono', 'permitePagoWebpay','initResult', 'parametros'));
    }

    public function confirmarReserva($idHora, $idConvenio, $idPrestacion){
        $convenio = Convenio::find($idConvenio);
        $prestacion = Prestacion::find($idPrestacion);
        $paciente = Auth::guard('paciente')->user()->paciente;
        date_default_timezone_set("Chile/Continental");

        /*Actualizamos la hora con la información del paciente que reserva.*/
        $hora = Hora::find(Crypt::decrypt($idHora));
        $hora->paciente = $paciente->id;
        $hora->fecha_agendamiento = Date('Y-m-d H:i:s');
        $hora->prestacion = $prestacion->idprestacion;
        $hora->idconvenio = $convenio->idConvenio;
        $hora->telecetep = 1;
        $hora->idmodalidad = 1;
        $hora->observacion = 'Corresponde a una atención de teleconsulta';
        $hora->token_paciente = md5($hora->id . $paciente->id);
        $hora->token_prestador = md5($hora->id . $hora->prestador);

        $flag = $hora->save();

        //API aranceles UAC
        $resultAPI = 'https://cetep.cl/qa/uac/apiArancel.php?idIsapre='.$paciente->isapre.'&idPrestacion='.$prestacion->idprestacion.'&idConvenio='.$convenio->idConvenio.'';
        $data = json_decode(file_get_contents($resultAPI), true);
        $amount = $data["valorArancel"];

        EstadoPagoHora::create([
            'id_hora' => $hora->id,
            'id_estado_pago' => 7,
            'monto' => $amount,
            'fecha' => Date('Y-m-d H:i:s')
        ]);

        if ($flag && count($hora->getChanges()) > 0) {
            /**
             * Se crea query con el detalle de la hora agendada
             * Marcello lópez C.
             */
            $detalleReserva = Hora::from('hora as hr')
                ->Join('prestador as pre', 'hr.prestador', '=', 'pre.id')
                ->Join('paciente as pa', 'hr.paciente', '=', 'pa.id')
                ->Join('especialidad as esp', 'hr.especialidad', '=', 'esp.id')
                ->select(
                    "hr.id as idHora",
                    DB::raw("DATE_FORMAT(hr.hora,'%d-%m-%Y') as fecha"),
                    DB::raw("TIME_FORMAT(hr.hora, '%H:%i %p') as hora"),
                    "pre.id as idPrestador",
                    DB::raw("CONCAT(pre.nombres, ' ', pre.apellidoPaterno, ' ', pre.apellidoMaterno) AS nombrePrestador"),
                    "esp.id as idEspecialidad",
                    DB::raw("CONCAT(pa.nombres, ' ', pa.apellidoPaterno, ' ', pa.apellidoMaterno) AS nombrePaciente"),
                    "esp.especialidad",
                    "pa.rut")
                ->where('hr.id', '=', $hora->id)
                ->first()
                ->toArray();
            /**
             * Se gatilla el detalle de la reserva en un email hacia el paciente
             * Marcello lópez C.
             */
            $paciente->notify(new ConfirmacionHora($paciente->email_telecetep, $detalleReserva));

            $flag = 1;
        } else {
            $flag = 0;
        }

        return redirect()->route('mensajeConfirmacion', compact( 'flag'));
    }

    public function confirmarReservaPago($idHora, $idConvenio, $idPrestacion){
        $convenio = Convenio::find($idConvenio);
        $prestacion = Prestacion::find($idPrestacion);
        $paciente = Auth::guard('paciente')->user()->paciente;
        date_default_timezone_set("Chile/Continental");

        /*Actualizamos la hora con la información del paciente que reserva.*/
        $hora = Hora::find(Crypt::decrypt($idHora));
        $hora->paciente = $paciente->id;
        $hora->fecha_agendamiento = Date('Y-m-d H:i:s');
        $hora->prestacion = $prestacion->idprestacion;
        $hora->idconvenio = $convenio->idConvenio;
        $hora->telecetep = 1;
        $hora->idmodalidad = 1;
        $hora->observacion = 'Corresponde a una atención de teleconsulta';
        $hora->token_paciente = md5($hora->id . $paciente->id);
        $hora->token_prestador = md5($hora->id . $hora->prestador);

        $flag = $hora->save();

        if ($flag && count($hora->getChanges()) > 0) {

            #Se crea query con el detalle de la hora agendada
            $detalleReserva = Hora::from('hora as hr')
                ->Join('prestador as pre', 'hr.prestador', '=', 'pre.id')
                ->Join('especialidad as esp', 'hr.especialidad', '=', 'esp.id')
                ->select(
                    "hr.id as idHora",
                    DB::raw("DATE_FORMAT(hr.hora,'%d-%m-%Y') as fecha"),
                    DB::raw("TIME_FORMAT(hr.hora, '%H:%i %p') as hora"),
                    "pre.id as idPrestador",
                    DB::raw("CONCAT(pre.nombres, ' ', pre.apellidoPaterno, ' ', pre.apellidoMaterno) AS nombrePrestador"),
                    "esp.id as idEspecialidad",
                    "esp.especialidad")
                ->where('hr.id', '=', $hora->id)
                ->first()
                ->toArray();

            # Envio de correos de confirmación de hora y de pago
            $paciente->notify(new ConfirmacionHora($detalleReserva));

            $flag = 1;
        } else {
            $flag = 0;
        }

        return redirect()->route('mensajeConfirmacion', compact( 'flag'));
    }


    public function mensajeConfirmacion($flag){
        $paciente = Auth::guard('paciente')->user()->paciente;
        return view('mensajeConfirmacion', compact('paciente', 'flag'));
    }

    public function ifAllowToLiberarAjax(Request $request){
        date_default_timezone_set("Chile/Continental");

        $hora = Hora::find($request->idHora);
        $idhora = $request->idHora;
//        $registroBonoHora = RegistroBonoHora::where('idhora','=',$idhora);
        $regla = DetalleRegla::where('idregla', '=', 6)->where('idconvenio', '=', $hora->idconvenio)->first();
        $cantHorasParaLiberar = $regla->valor;

        $dateTimeLimite = date('Y-m-d H:i:s', strtotime($cantHorasParaLiberar .' hour'));
        $flag = strtotime($hora->hora) >= strtotime($dateTimeLimite);

        return  response()->json(array($flag, $cantHorasParaLiberar));
    }

    public function liberarHoraAjax(Request $request){
        date_default_timezone_set("Chile/Continental");
        $hora = Hora::find($request->idHora);
        $paciente = Auth::guard('paciente')->user()->paciente;

        $horaEliminada = HorasEliminadas::create([
            'fecha' => date('Y-m-d H:i:s'),
            'idHora' => $request->idHora,
            'hora' => $hora->hora,
            'ciudad' => '1',
            'paciente' => $paciente->id,
            'prestador' => $hora->prestador,
            'motivoEliminacion' => '17',
            'motivoOtro' => 'Liberada por paciente mediante plataforma Telecetep',
            'prestacion' => $hora->prestacion,
        ]);

        $estado_pago_hora = EstadoPagoHora::where('id_hora', '=', $request->idHora)
            ->orderBy('id_estado_pago_hora', 'DESC')
            ->first();

        if($estado_pago_hora === null){
            EstadoPagoHora::create([
                'id_hora' => $request->idHora,
                'id_estado_pago' => 8,
                'fecha' => date('Y-m-d H:i:s')
            ]);
        } else {
            EstadoPagoHora::create([
                'id_hora' => $estado_pago_hora->id_hora,
                'id_registro_transbank' => $estado_pago_hora->id_registro_transbank,
                'id_registro_bono' => $estado_pago_hora->id_registro_bono,
                'id_estado_pago' => 8,
                'id_medio_pago' => $estado_pago_hora->id_medio_pago,
                'monto' => $estado_pago_hora->monto,
                'fecha' => date('Y-m-d H:i:s')
            ]);
        }


        /*Actualizamos la hora con la información del paciente que reserva.*/
        $hora->paciente = null;
        $hora->fecha_agendamiento = null;
        $hora->paciente = null;
        $hora->idconvenio = null;
        $hora->prestacion = null;
        $hora->telecetep = 2;
        $hora->confirmada = null;
        $hora->observacion = null;
        $hora->token_paciente = null;
        $hora->token_prestador = null;

        $flag = $hora->save();

        if($flag && count($hora->getChanges()) > 0){
            return  response()->json(array(1));
        }
        return  response()->json(array(0));
    }

    public function showPacientePrestador(){
        $prestador = Auth::guard('prestador')->user();
        return view('prestador.paciente', compact('prestador'));
    }

    /**-------------------------------------------
     * - Funciones estaticas
     * -------------------------------------------
     */
    public static function cantidadHorasReservadas(Paciente $paciente, $idconvenio){
        date_default_timezone_set("Chile/Continental");

        $dateTimeActual = date('Y-m-d H:i:s');

        /*En caso de que en un futuro se puedan realizar más de 2 reservas,
        modificar la siguiente variables*/

        $horasReservadas = Hora::from('hora AS hr')
            ->select(
                'hr.id as idHora'
            )
            ->where('hr.idconvenio', $idconvenio)
            ->where('hr.paciente', $paciente->id)
            ->where(DB::raw('(ADDTIME(hr.hora,"1:30:00"))'), '>=', $dateTimeActual)
            ->orderby('hr.hora', 'ASC')
            ->get();

        $countReservas = $horasReservadas->count();
        return $countReservas;
    }

    public static function historialPaciente($idPaciente){
        return Hora::from('hora as hr')
            ->Join('prestador AS pre', 'hr.prestador', '=', 'pre.id')
            ->Join('especialidad AS esp', 'hr.especialidad', '=', 'esp.id')
            ->Join('paciente AS pac', 'hr.paciente', '=', 'pac.id')
            ->Join('prestacion_achs AS presta', 'hr.prestacion', '=','presta.idprestacion')
            ->select(
                DB::raw('(DATE_FORMAT(hr.hora,"%H:%i %p")) AS hora'),
                DB::raw('(DATE_FORMAT(hr.hora,"%d-%m-%Y")) AS fecha'),
                DB::raw('CONCAT(pre.nombres," ",pre.apellidoPaterno, " ", pre.apellidoMaterno) AS prestador'),
                'esp.especialidad AS especialidad',
                'presta.descripcionespecialidad AS prestacion',
                DB::raw('if(hr.asiste IS NULL, "NO", hr.asiste) AS asiste')
            )
            ->where('hr.hora','<=', DB::raw('NOW()'))
            ->where('pac.id','=', $idPaciente)
            ->get();
    }

    public static function horasPaciente($idPaciente){
        return Hora::from('hora as hr')
            ->select('hr.hora')
            ->where('hr.hora', '>=', DB::raw('NOW()'))
            ->where('hr.paciente', $idPaciente)
            ->get();
    }

    public static function historialDelPaciente($idPaciente){
        return Hora::from('hora as hr')
            ->Join('prestador AS pre', 'hr.prestador', '=', 'pre.id')
            ->Join('especialidad AS esp', 'hr.especialidad', '=', 'esp.id')
            ->Join('paciente AS pac', 'hr.paciente', '=', 'pac.id')
            ->Join('prestacion_achs AS presta', 'hr.prestacion', '=','presta.idprestacion')
            ->select(
                DB::raw('(DATE_FORMAT(hr.hora,"%H:%i %p")) AS hora'),
                DB::raw('(DATE_FORMAT(hr.hora,"%d-%m-%Y")) AS fecha'),
                DB::raw('CONCAT(pre.nombres," ",pre.apellidoPaterno, " ", pre.apellidoMaterno) AS prestador'),
                'esp.especialidad AS especialidad',
                'presta.descripcionespecialidad AS prestacion',
                DB::raw('if(hr.asiste IS NULL, "NO", hr.asiste) AS asiste')
            )
            ->where('hr.hora','<=', DB::raw('NOW()'))
            ->where('pac.id','=', $idPaciente)
            ->get();
    }


    public static function agendaDelPaciente(Paciente $paciente){
        date_default_timezone_set("Chile/Continental");

        $dateTimeActual = date('Y-m-d H:i:s');
        $tomorrowDate = date('Y-m-d H:i:s', strtotime($dateTimeActual . " +1 days"));

        $horasReservadas = Hora::from('hora AS hr')
            ->join('prestador AS pre', 'hr.prestador', '=', 'pre.id')
            ->join('especialidad AS esp', 'hr.especialidad', '=', 'esp.id')
            ->join('paciente AS pac', 'hr.paciente', '=', 'pac.id')
            ->join('convenio as con', 'hr.idconvenio', '=', 'con.idConvenio')
            ->select(
                'hr.id as idHora',
                DB::raw('(DATE_FORMAT(hr.hora,"%d-%m-%Y")) AS fecha'),
                DB::raw('(TIME_FORMAT(hr.hora,"%H:%i %p")) AS hora'),
                'hr.hora as horaCompleta',
                'hr.idconvenio',
                'con.descripcion AS convenio',
                DB::raw('CONCAT(pre.nombres," ",pre.apellidoPaterno) AS prestador'),
                'esp.especialidad',
                DB::raw('(ADDTIME(hr.hora,"-00:30:00")) AS horaComienzo'),
                DB::raw('(ADDTIME(hr.hora,"1:30:00")) AS horaExpiracion')
            )
            ->where('hr.telecetep', '=', '1')
            ->where('hr.paciente', $paciente->id)
            ->where(DB::raw('(ADDTIME(hr.hora,"1:30:00"))'), '>=', $dateTimeActual)
            ->orderby('hr.hora', 'ASC')
            ->get();


        foreach ($horasReservadas as $key=>$hora){
            $estado_pago_hora = EstadoPagoHora::where('id_hora', '=', $hora->idHora)
                ->orderBy('id_estado_pago_hora', 'DESC')
                ->first();

            $hora->estado_pago = $estado_pago_hora !== null ? $estado_pago_hora->id_estado_pago : 9;
        }
//        dd($horasReservadas);
        return array('dateTimeActual' => $dateTimeActual,
                     'horasReservadas' => $horasReservadas,
                     'tomorrowDate' => $tomorrowDate);
    }


    public static function bono(){
        return Hora::from('hora as hr');
        $paciente = Auth::guard('paciente')->user()->paciente;
        $dateTimeActualb = HoraController::agendadosBono()['dateTimeActualb'];
        $horasReservadasBono = HoraController::agendadosBono()['horasBono'];
        $tomorrowDateb = HoraController::agendadosBono()['tomorrowDateb'];

        return view('prueba', compact('paciente',  'dateTimeActualb', 'horasBono', 'tomorrowDateb'));
    }

    public function getInfoPagarHora(Request $request){
        $hora = Hora::find($request->idHora);
        $idhoraEncrypted = Crypt::encrypt($request->idHora);
        $prestacion = Prestacion::find($hora->prestacion);
        $convenio = Convenio::find($hora->idconvenio);
        $paciente = Auth::guard('paciente')->user()->paciente;

        $sessionId = "pago_telecetep";
        $idhorapaciente = $hora->id.$paciente->id;

        $buyOrder = ($idhorapaciente);
        $entorno = 2; //testing 0; QA 1; PROD 2

        if($entorno == 0){
            //API aranceles UAC
            $resultAPI = 'https://cetep.cl/qa/uac/apiArancel.php?idIsapre='.$paciente->isapre.'&idPrestacion='.$prestacion->idprestacion.'&idConvenio='.$convenio->idConvenio.'';
            $data = json_decode(file_get_contents($resultAPI), true);
            $amount = $data["valorArancel"];

            //URL testing webpay
            $transaction = (new Webpay(Configuration::forTestingWebpayPlusNormal()))->getNormalTransaction();

            $returnUrl = "https://telecetep.test/comprobante/returnpago/".$idhoraEncrypted."/".$convenio->idConvenio."/".$prestacion->idprestacion;
            $finalUrl = "https://telecetep.test/comprobante/returnsuccess/".$idhoraEncrypted."/".$convenio->idConvenio."/".$prestacion->idprestacion;

            $initResult = $transaction->initTransaction($amount, $buyOrder, $sessionId, $returnUrl, $finalUrl);
        } elseif($entorno == 1){
            //API aranceles UAC
            $resultAPI = 'https://cetep.cl/qa/uac/apiArancel.php?idIsapre='.$paciente->isapre.'&idPrestacion='.$prestacion->idprestacion.'&idConvenio='.$convenio->idConvenio.'';
            $data = json_decode(file_get_contents($resultAPI), true);
            $amount = $data["valorArancel"];

            //URL QA webpay
            $transaction = (new Webpay(Configuration::forTestingWebpayPlusNormal()))->getNormalTransaction();

            $returnUrl = "https://test.telecetep.cl/comprobante/returnpago/".$idhoraEncrypted."/".$convenio->idConvenio."/".$prestacion->idprestacion;
            $finalUrl = "https://test.telecetep.cl/comprobante/returnsuccess/".$idhoraEncrypted."/".$convenio->idConvenio."/".$prestacion->idprestacion;

            $initResult = $transaction->initTransaction($amount, $buyOrder, $sessionId, $returnUrl, $finalUrl);
        }
        elseif($entorno == 2){
            //API aranceles UAC
            $resultAPI = 'https://cetep.cl/uac/apiArancel.php?idIsapre='.$paciente->isapre.'&idPrestacion='.$prestacion->idprestacion.'&idConvenio='.$convenio->idConvenio.'';
            $data = json_decode(file_get_contents($resultAPI), true);
            $amount = $data["valorArancel"];

            //Configuration Pro
            $configuration = new Configuration();
            $configuration->setEnvironment('PRODUCCION');
            $configuration->setCommerceCode(597035456341);
            $publiccert = 'https://www.cetep.cl/tbank/597035456341.crt';
            $privatekey = 'https://www.cetep.cl/tbank/597035456341.key';
            $configuration->setPublicCert(file_get_contents($publiccert));
            $configuration->setPrivateKey(preg_replace('~[\r]+~','',file_get_contents($privatekey)));

            //URL produccion
            $transaction = (new WebPay($configuration))->getNormalTransaction();

            $returnUrl = "https://telecetep.cl/comprobante/returnpago/".$idhoraEncrypted."/".$convenio->idConvenio."/".$prestacion->idprestacion;
            $finalUrl = "https://telecetep.cl/comprobante/returnsuccess/".$idhoraEncrypted."/".$convenio->idConvenio."/".$prestacion->idprestacion;

            $initResult = $transaction->initTransaction($amount, $buyOrder, $sessionId, $returnUrl, $finalUrl);
        }

        $permitePagoWebpay = DetalleRegla::where( 'idregla', '=', 3)->where('idconvenio', '=', $convenio->idConvenio)->first();
        $permitePagoBono = DetalleRegla::where( 'idregla', '=', 10)->where('idconvenio', '=', $convenio->idConvenio)->first();

        return response()->json(
            array(
                'permitePagoWebpay' => $permitePagoWebpay !== null ? intval($permitePagoWebpay->valor) : 0,
                'permitePagoBono' => $permitePagoBono !== null ? intval($permitePagoBono->valor) : 0,
                'initResult_url' => $initResult->url,
                'initResult_token' => $initResult->token));
    }

    public function getInfoHoraBono(Request $request){
        $hora = Hora::find($request->idHora);
        return response()->json(
            array('idHora' => $hora->id,
                  'monto' => '27760',
                  'idConvenio' => $hora->idconvenio,
                  'idPrestacion' => $hora->prestacion));
    }

    public static function agendadosBono(){
        date_default_timezone_set("Chile/Continental");

        $dateTimeActual = date('Y-m-d H:i:s');
        $tomorrowDate = date('Y-m-d H:i:s', strtotime($dateTimeActual . " +1 days"));

        $horasReservadas = Hora::from('hora AS hr')
            ->join('prestador AS pre', 'hr.prestador', '=', 'pre.id')
            ->join('especialidad AS esp', 'hr.especialidad', '=', 'esp.id')
            ->join('paciente AS pac', 'hr.paciente', '=', 'pac.id')
            ->join('convenio as con', 'hr.idconvenio', '=', 'con.idConvenio')
            ->join('registroBonoHora as rbh','rbh.idhora','=','hr.id')
            ->select(
                'hr.id as idHora',
                DB::raw('(DATE_FORMAT(hr.hora,"%d-%m-%Y")) AS fecha'),
                DB::raw('(TIME_FORMAT(hr.hora,"%H:%i %p")) AS hora'),
                'hr.hora as horaCompleta',
                'hr.idconvenio',
                'con.descripcion AS convenio',
                DB::raw('CONCAT(pre.nombres," ",pre.apellidoPaterno) AS prestador'),
                'esp.especialidad',
                DB::raw('(ADDTIME(hr.hora,"-00:30:00")) AS horaComienzo'),
                DB::raw('(ADDTIME(hr.hora,"1:30:00")) AS horaExpiracion'),
                DB::raw('CONCAT(pac.nombres," ",pac.apellidoPaterno," ",pac.apellidoMaterno) AS nombre'),
                DB::raw('CONCAT(pac.rut) AS rut'),
                DB::raw('(pac.celular) as celular'),
                DB::raw('(rbh.idbono) as numbono'),
                DB::raw('(DATE_FORMAT(rbh.fechabono,"%d-%m-%Y")) as fechab'),
                DB::raw('(rbh.archivo) as archivo'),
                DB::raw('(rbh.extension) as extension'),
                DB::raw('(rbh.monto) as monto'),
                DB::raw('(rbh.idRegBonoHora) as idRegBono')
            )

            ->where('hr.telecetep', '=', '1')
            ->where(DB::raw('(ADDTIME(hr.hora,"1:30:00"))'), '>=', $dateTimeActual)
            ->where('rbh.id_proceso_estado','=','21')
            ->orderby('hr.hora', 'ASC')
            ->get();


        return array('dateTimeActual' => $dateTimeActual,
            'horasReservadas' => $horasReservadas,
            'tomorrowDate' => $tomorrowDate);
    }


    public function ingBono(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idBono' => 'required|numeric|unique:registroBonoHora,idbono|digits_between:7,10',
            'bonoFile' => 'required|mimes:jpeg,png,jpg,pdf|max:7168',
            'idHora' => 'required',
            'fechab' => 'required|date'
        ], [
            'idBono.required' => 'El folio de bono es un campo requerido.',
            'idBono.numeric' => 'El folio del bono solo puede contener valores numéricos.',
            'idBono.unique' => 'El número de bono ingresado ya está en uso.',
            'idBono.digits_betweeen' => 'El número de bono debe ser de 7 a 10 dígitos.',
            'bonoFile.max'=>'El peso del archivo no debe ser superior a 7Mb',
            'bonoFile.required' => 'Debe adjuntar un archivo.',
            'fechab.required' => 'La fecha de emisión es obligatoria.',
            'fechab.date' => 'La fecha de emisión no posee un formato adecuado.'
        ]);

        if ($validator->fails()) {

            $result = array(
                'registered' => false,
                'errors' => $validator->errors()
            );
            return response()->json($result);
        }
        $nameF = $request->bonoFile->getClientOriginalName();
        $registroBonoHora = registroBonoHora::create([
            'idbono' => $request->idBono,
            'extension' => substr($nameF, strrpos($nameF, '.')+1),
            'archivo' => $blob = base64_encode(file_get_contents(Input::file('bonoFile')->getRealPath())),
            'idhora' => $request->idHora,
            'create_at' => $horaActual = Carbon::now(),
            'monto' => $request->monto,
            'fechabono' => $request->fechab,
            'id_proceso_estado' => '21'
        ]);

        $flag = $registroBonoHora->save();
        if($flag) {
            $result = array(
                'registered' => true
            );

            return response()->json($result);
        }
    }




    public function liberarHoraBonoAjax(Request $request){

        $validator = Validator::make($request->all(), [
                'motivo'=> 'required|max:50']
            ,[  'motivo.digits_between'=>'El campo tiene un maximo de 50 caracteres',
                'motivo.required'=>'El campo debe ser rellenado.'
            ]);

        if ($validator->fails()) {

            $result = array(
                'registered' => false,
                'errors' => $validator->errors()
            );
            return response()->json($result);
        }else{
            date_default_timezone_set("Chile/Continental");
            $hora = Hora::find($request->idHoraLi);
            /*Actualizamos la hora con la información del paciente que reserva.*/
            $hora->paciente = null;
            $hora->fecha_agendamiento = null;
            $hora->paciente = null;
            $hora->idconvenio = null;
            $hora->prestacion = null;
            $hora->telecetep = 2;
            $hora->confirmada = null;
            $hora->observacion = null;
            $hora->token_paciente = null;
            $hora->token_prestador = null;

            $flag =$hora->save();
            /**
             * Se elimina registro de tabla registro bono hora
             * para evitar futuros problemas con id bono. Este bono
             * puede ser reutilizado en otra reserva
             */
            $horaEliminada = HorasEliminadas::create([
                'fecha' => date('Y-m-d H:i:s'),
                'idHora' => $request->idHoraLi,
                'hora' => $hora->hora,
                'ciudad' => '1',
                'paciente' => $hora->paciente,
                'prestador' => $hora->prestador,
                'motivoEliminacion' => '17',
                'motivoOtro' => $request->motivo,
                'prestacion' => $hora->prestacion,
            ]);

            $horaEliminada->save();

             registroBonoHora::destroy($request->idRegBonoHorasLi);

            if($flag){
                $result = array(
                    'registered' => true
                );
            }
            return  response()->json($result);
        }
    }

    public function rechazarHoraBonoAjax(Request $request){

        $validator = Validator::make($request->all(), [
                'motivo'=> 'required|max:50']
            ,[  'motivo.digits_between'=>'El campo tiene un maximo de 50 caracteres',
                'motivo.required'=>'El campo debe ser rellenado.'
            ]);

        if ($validator->fails()) {

            $result = array(
                'registered' => false,
                'errors' => $validator->errors()
            );
            return response()->json($result);
        }else{
            date_default_timezone_set("Chile/Continental");
            $hora = Hora::find($request->idHoraRe);
            /**
             *
             * Se elimina registro de tabla registro bono hora
             * para evitar futuros problemas con id bono. Este bono
             * puede ser reutilizado en otra reserva
             */
            $horaEliminada = HorasEliminadas::create([
                'fecha' => date('Y-m-d H:i:s'),
                'idHora' => $request->idHoraRe,
                'hora' => $hora->hora,
                'ciudad' => '1',
                'paciente' => $hora->paciente,
                'prestador' => $hora->prestador,
                'motivoEliminacion' => '17',
                'motivoOtro' => $request->motivo,
                'prestacion' => $hora->prestacion,
            ]);

            $horaEliminada->save();

            $flag = registroBonoHora::destroy($request->idRegBonoHorasRe);

            if($flag){
                $result = array(
                    'registered' => true
                );
            }
            return  response()->json($result);
        }
    }

    public function confirmarBonos(Request $request)
    {
        $errores = array();

        $validator = Validator::make($request->all(), [
                'numBonoComprobar' => [
                    'required',
                    'numeric',
                    'digits_between:7,10',
                    Rule::unique('registroBonoHora', 'idbono')->ignore($request->idRegBonoHora, 'idRegBonoHora')
            ]]
            , ['numBonoComprobar.numeric' => 'El dato debe ser numérico ',
                'numBonoComprobar.digits_between' => 'El campo debe poseer entre 7 y 10 dígitos',
                'numBonoComprobar.required' => 'El campo debe ser rellenado.',
                'numBonoComprobar.unique' =>'El bono ingresado esta siendo utilizado en otra reserva'
            ]);

        if ($validator->fails()) {

            $result = array(
                'registered' => false,
                'errors' => $validator->errors()
            );
            return response()->json($result);
        }

        $matchThese = [
            'idbono' => $request->input('numBonoComprobar'),
            'idhora' => $request->input('idHora')
        ];


        if (registroBonoHora::where('idbono', '=',$request->numBonoComprobar)->exists()) {
            $registroVal = registroBonoHora::find($request->idRegBonoHora);
            $registroVal->id_proceso_estado = 20;
            $flag = $registroVal->save();

        }elseif($request->conf == 1 and registroBonoHora::where('idRegBonoHora', '=',$request->idRegBonoHora)->exists()) {
                $registroVal = registroBonoHora::find($request->idRegBonoHora);
                $registroVal -> id_proceso_estado = 20;
                $registroVal -> observacion = 'Numero bono cambiado por funcionario, numero anterior: '.$registroVal->idbono;
                $registroVal -> idbono = $request->numBonoComprobar;
                $flag = $registroVal->save();

        }else{
            $mensaje = "El bono ingresado no coincide con el registrado por el paciente. <b>Si desea continuar de todas formas, haga click en Guardar, de lo contrario, presione Cerrar.</b>";
            array_push($errores, $mensaje);
            return response()->json(array('registered' => false, 'errors' => $errores, 'valor'=> 1)) ;
            }

        if($flag) {
            $result = array(
                'registered' => true
            );

            return response()->json($result);
        }


    }


    public static function horaReservadaFunc(){
        date_default_timezone_set("Chile/Continental");

        $dateTimeActual = date('Y-m-d H:i:s');
        $tomorrowDate = date('Y-m-d H:i:s', strtotime($dateTimeActual . " +1 days"));

        $horasReservadas = Hora::from('hora AS hr')
            ->join('prestador AS pre', 'hr.prestador', '=', 'pre.id')
            ->join('especialidad AS esp', 'hr.especialidad', '=', 'esp.id')
            ->join('paciente AS pac', 'hr.paciente', '=', 'pac.id')
            ->join('convenio as con', 'hr.idconvenio', '=', 'con.idConvenio')
            ->select(
                'hr.id as idHora',
                DB::raw('(DATE_FORMAT(hr.hora,"%d-%m-%Y")) AS fecha'),
                DB::raw('(TIME_FORMAT(hr.hora,"%H:%i %p")) AS hora'),
                'hr.hora as horaCompleta',
                'hr.idconvenio',
                'con.descripcion AS convenio',
                DB::raw('CONCAT(pre.nombres," ",pre.apellidoPaterno) AS prestador'),
                'esp.especialidad',
                DB::raw('(ADDTIME(hr.hora,"-00:30:00")) AS horaComienzo'),
                DB::raw('(ADDTIME(hr.hora,"1:30:00")) AS horaExpiracion'),
                DB::raw('CONCAT(pac.nombres," ",pac.apellidoPaterno," ",pac.apellidoMaterno) AS nombre'),
                DB::raw('CONCAT(pac.rut) AS rut'),
                DB::raw('(pac.celular) as celular')
            )

            ->where('hr.telecetep', '=', '1')
            ->where('hr.idTipoReserva','=','1')
            ->where(DB::raw('(ADDTIME(hr.hora,"1:30:00"))'), '>=', $dateTimeActual)
            ->orderby('hr.hora', 'ASC')
            ->get();


        return array('dateTimeActual' => $dateTimeActual,
            'horasReservadas' => $horasReservadas,
            'tomorrowDate' => $tomorrowDate);
    }



}
