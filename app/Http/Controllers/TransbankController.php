<?php

namespace App\Http\Controllers;

use App\Entities\Convenio;
use App\Entities\EstadoPagoHora;
use App\Entities\Hora;
use App\Entities\Prestacion;
use App\Entities\RegistroTransbank;
use App\Notifications\ConfirmacionHora;
use App\Notifications\ConfirmacionPago;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Transbank\Webpay\Configuration;
use Transbank\Webpay\Webpay;

class TransbankController extends Controller
{
    public function getReturnTransaction(Request $request,$idhora,$idConvenio,$idPrestacion)
    {
        $convenio = Convenio::find($idConvenio);
        $prestacion = Prestacion::find($idPrestacion);
        $paciente = Auth::guard('paciente')->user()->paciente;
        date_default_timezone_set("Chile/Continental");

        /*Actualizamos la hora con la información del paciente que reserva.*/
        $hora = Hora::find(Crypt::decrypt($idhora));
        $tokenWS = $request->token_ws;

        $entorno = 2; //testing 0; QA 1; PROD 2

        if($entorno == 0){
            //API aranceles UAC
            $resultAPI = 'https://cetep.cl/qa/uac/apiArancel.php?idIsapre='.$paciente->isapre.'&idPrestacion='.$prestacion->idprestacion.'&idConvenio='.$convenio->idConvenio.'';
            $data = json_decode(file_get_contents($resultAPI), true);
            $amount = $data["valorArancel"];

            //URL testing webpay
            $transaction = (new Webpay(Configuration::forTestingWebpayPlusNormal()))->getNormalTransaction();
        } elseif($entorno == 1){
            //API aranceles UAC
            $resultAPI = 'https://cetep.cl/qa/uac/apiArancel.php?idIsapre='.$paciente->isapre.'&idPrestacion='.$prestacion->idprestacion.'&idConvenio='.$convenio->idConvenio.'';
            $data = json_decode(file_get_contents($resultAPI), true);
            $amount = $data["valorArancel"];

            //URL QA webpay
            $transaction = (new Webpay(Configuration::forTestingWebpayPlusNormal()))->getNormalTransaction();
        }
        elseif($entorno == 2) {
            //API aranceles UAC
            $resultAPI = 'https://cetep.cl/uac/apiArancel.php?idIsapre=' . $paciente->isapre . '&idPrestacion=' . $prestacion->idprestacion . '&idConvenio=' . $convenio->idConvenio . '';
            $data = json_decode(file_get_contents($resultAPI), true);
            $amount = $data["valorArancel"];

            //Configuration Pro
            $configuration = new Configuration();
            $configuration->setEnvironment('PRODUCCION');
            $configuration->setCommerceCode(597035456341);
            $publiccert = 'https://www.cetep.cl/tbank/597035456341.crt';
            $privatekey = 'https://www.cetep.cl/tbank/597035456341.key';
            $configuration->setPublicCert(file_get_contents($publiccert));
            $configuration->setPrivateKey(preg_replace('~[\r]+~', '', file_get_contents($privatekey)));

            //URL produccion
            $transaction = (new WebPay($configuration))->getNormalTransaction();
        }

        $result = $transaction->getTransactionResult($request->input("token_ws"));
        $output = $result->detailOutput;
        if ($output->responseCode == 0) {
            $flag = 1;
            return view('confirmacionPago', compact( 'result','output','tokenWS','paciente','hora','flag'));
        }else{ //fallo
            $detalleReserva = null;
            $hora->paciente = $paciente->id;
            $hora->fecha_agendamiento = Date('Y-m-d H:i:s');
            $hora->prestacion = $prestacion->idprestacion;
            $hora->idconvenio = $convenio->idConvenio;
            $hora->telecetep = 1;
            $hora->idsede = 1;
            $hora->idmodalidad = 1;
//            $hora->confirmada = 'SI';
            $hora->observacion = 'Corresponde a una atención de teleconsulta';
            $hora->token_paciente = md5($hora->id . $paciente->id);
            $hora->token_prestador = md5($hora->id . $hora->prestador);
            $hora->save();

            $flag = 0;

            EstadoPagoHora::create([
                'id_hora' => $hora->id,
                'id_estado_pago' => 7,
                'id_medio_pago' => 1,
                'monto' => $output->amount,
                'fecha' => Date('Y-m-d H:i:s')
            ]);

            return view('resultadoPago', compact( 'paciente','output','detalleReserva','flag'));
        }
    }

    public function getResultTransaction(Request $request,$idhora,$idConvenio,$idPrestacion)
    {
        if(!$request->TBK_ORDEN_COMPRA){
            date_default_timezone_set("Chile/Continental");
            $convenio = Convenio::find($idConvenio);
            $prestacion = Prestacion::find($idPrestacion);
            $paciente = Auth::guard('paciente')->user()->paciente;
            
            /*Actualizamos la hora con la información del paciente que reserva.*/
            $hora = Hora::find(Crypt::decrypt($idhora));

            $horaNueva = false;
            if($hora->paciente != null && $paciente->id == $hora->paciente){
                $hora->observacion = $hora->observacion. ' // Pagó mediante webpay';
            } else {
                $hora->paciente = $paciente->id;
                $hora->fecha_agendamiento = Date('Y-m-d H:i:s');
                $hora->prestacion = $prestacion->idprestacion;
                $hora->idconvenio = $convenio->idConvenio;
                $hora->telecetep = 1;
                $hora->idsede = 1;
                $hora->idmodalidad = 1;
                $hora->confirmada = 'SI';
                $hora->observacion = 'Corresponde a una atención de teleconsulta // Pagó mediante webpay';
                $hora->token_paciente = md5($hora->id . $paciente->id);
                $hora->token_prestador = md5($hora->id . $hora->prestador);

                $horaNueva = true;
            }

            $flag = $hora->save();

            if ($flag && count($hora->getChanges()) > 0) {

                #Se crea query con el detalle de la hora agendada
                $detalleReserva = Hora::from('hora as hr')
                    ->Join('prestador as pre', 'hr.prestador', '=', 'pre.id')
                    ->Join('paciente as pa', 'hr.paciente', '=', 'pa.id')
                    ->Join('especialidad as esp', 'hr.especialidad', '=', 'esp.id')
                    ->select(
                        "hr.id as idHora",
                        DB::raw("DATE_FORMAT(hr.hora,'%d-%m-%Y') as fecha"),
                        DB::raw("TIME_FORMAT(hr.hora, '%H:%i %p') as hora"),
                        "pre.id as idPrestador",
                        DB::raw("CONCAT(pre.nombres, ' ', pre.apellidoPaterno, ' ', pre.apellidoMaterno) AS nombrePrestador"),
                        "esp.id as idEspecialidad",
                        DB::raw("CONCAT(pa.nombres, ' ', pa.apellidoPaterno, ' ', pa.apellidoMaterno) AS nombrePaciente"),
                        "esp.especialidad",
                        'pa.rut')
                    ->where('hr.id', '=', $hora->id)
                    ->first()
                    ->toArray();

                $detallePago = Hora::from('registroTransbank as rtb')
                    ->select('rtb.detailbuyOrder',
                             'rtb.amount',
                             'rtb.cardNumber',
                             'rtb.idhora')
                    ->where('rtb.idhora', '=', $hora->id)
                    ->orderBy('idregistrotransbank', 'DESC')
                    ->first()
                    ->toArray();


                # Envio de correos de confirmación de hora y de pago
                if($horaNueva){
                    $paciente->notify(new ConfirmacionHora($paciente->email_telecetep, $detalleReserva));
                }
                $paciente->notify(new ConfirmacionPago($paciente->email_telecetep, $detalleReserva, $detallePago));

                $flag = 1;
            } else {
                $flag = 0;
            }

            return view('resultadoPago', compact( 'paciente','detalleReserva','flag'));
        }else{
            $flag = 2; //cancelado
            $paciente = Auth::guard('paciente')->user()->paciente;
            return view('resultadoPago', compact( 'paciente','flag'));
        }

    }
    public static function store($output,$result,$hora)
    {
        date_default_timezone_set("Chile/Continental");

        $paciente = Auth::guard('paciente')->user()->paciente;
        $registrotransbank = new RegistroTransbank();

        $registrotransbank->accountingDate = $result->accountingDate;
        $registrotransbank->buyOrder = $result->buyOrder;
        $registrotransbank->idhora = $hora->id;
        $registrotransbank->idpaciente = $paciente->id;
        $registrotransbank->cardNumber = $result->cardDetail->cardNumber;
        $registrotransbank->cardExpirationDate = $result->cardDetail->cardExpirationDate;
        $registrotransbank->authorizationCode = $output->authorizationCode;
        $registrotransbank->paymentTypeCode = $output->paymentTypeCode;
        $registrotransbank->responseCode = $output->responseCode;
        $registrotransbank->sharesNumber = $output->sharesNumber;
        $registrotransbank->amount = $output->amount;
        $registrotransbank->commerceCode = $output->commerceCode;
        $registrotransbank->detailbuyOrder = $output->buyOrder;
        $registrotransbank->sessionId = $result->sessionId;
        $registrotransbank->transactionDate = $result->transactionDate;
        $registrotransbank->urlRedirection = $result->urlRedirection;
        $registrotransbank->vci = $result->VCI;

        $registrotransbank->save();

        if($output->responseCode == 0){
            $estadopago = 9;
        }else{
            $estadopago = 7;
        }

        EstadoPagoHora::create([
            'id_hora' => $hora->id,
            'id_registro_transbank' => $registrotransbank->idregistrotransbank,
            'id_estado_pago' => $estadopago,
            'id_medio_pago' => 1,
            'monto' => $output->amount,
            'fecha' => Date('Y-m-d H:i:s')
        ]);
    }

}
