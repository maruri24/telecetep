<?php

namespace App\Http\Controllers;

use App\Entities\Convenio;
use App\Entities\ConvenioPaciente;
use App\Entities\Paciente;
use App\Entities\Prestacion;
use App\Entities\Prestador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ConvenioController extends Controller
{
    public static function getConveniosPaciente(Paciente $paciente){
        $convenios = $paciente->convenios()->orderBy('idConvenio')->get();

        /* Si el paciente no tiene convenios o no tiene el convenio Libre Eleccion
         * se crea un nuevo registro en la base de datos asignando este convenio al
         * paciente.*/
        if($convenios === null || $convenios->find(0) === null){
            $convenioPaciente = new ConvenioPaciente;
            $convenioPaciente->idpaciente = $paciente->id;
            $convenioPaciente->idconvenio = 0;

            $convenioPaciente->save();
            $convenios = $paciente->convenios()->orderBy('idConvenio')->get();
        }

        return $convenios;
    }

    public static function getPrestacionesConvenio(Convenio $convenio){
        return $convenio->prestaciones()->where('prestacion_achs.telecetep', '=', 1)->orderBy('descripcionespecialidad')->get();
    }

    public static function getPrestadoresPrestacion(Prestacion $prestacion, Convenio $convenio, $location){

        if($location != 'all'){
            return Prestador::where('telecetep', '=', '1')
                ->whereHas('convenios', function($query) use ($convenio){
               $query->where('convenio.idConvenio','=',$convenio->idConvenio);
            })->whereHas('prestaciones', function($query) use ($prestacion){
                $query->where('prestacion_achs.idprestacion', '=', $prestacion->idprestacion);
            })->whereHas('sedes', function($query) use ($location){
                $query->where('prestador_especialidad.idsede', '=', $location);
            })->get();
        }

        return Prestador::where('telecetep', '=', '1')
            ->whereHas('convenios', function($query) use ($convenio){
            $query->where('convenio.idConvenio','=',$convenio->idConvenio);
        })->whereHas('prestaciones', function($query) use ($prestacion){
            $query->where('prestacion_achs.idprestacion', '=', $prestacion->idprestacion);
        })->get();

    }

    public function getPrestadoresPrestacion_ajax(Request $request){
        if($request->location == 'all'){
            $prestadores = Prestador::where('telecetep', '=', '1')
                ->whereHas('convenios', function($query) use ($request){
                $query->where('convenio.idConvenio','=',$request->idConvenio);
            })->whereHas('prestaciones', function($query) use ($request){
                $query->where('prestacion_achs.idprestacion', '=', $request->idPrestacion);
            })->get();
        } else {
            $prestadores = Prestador::where('telecetep', '=', '1')
                ->whereHas('convenios', function($query) use ($request){
                $query->where('convenio.idConvenio','=',$request->idConvenio);
            })->whereHas('prestaciones', function($query) use ($request){
                $query->where('prestacion_achs.idprestacion', '=', $request->idPrestacion);
            })->whereHas('sedes', function($query) use ($request){
                $query->where('prestador_especialidad.idsede', '=', $request->location);
            })->get();
        }

        return response()->json(array($prestadores));
    }

    public function getPrestacionesConvenio_ajax(Request $request){
        $prestaciones = Convenio::find($request->idConvenio)->prestaciones()->where('prestacion_achs.telecetep', '=', 1)->orderBy('descripcionespecialidad')->get();
        return response()->json(array($prestaciones));
    }

}
