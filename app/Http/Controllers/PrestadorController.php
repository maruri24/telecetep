<?php

namespace App\Http\Controllers;

use App\Entities\DetalleRegla;
use App\Entities\Hora;
use App\Entities\Paciente;
use App\Entities\Prestacion;
use App\Entities\Prestador;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PrestadorController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public static function getPrestadorPaciente(Paciente $paciente){
        $prestadorPaciente = DB::table('reserva_paciente')
            ->select('id_prestador')
            ->where('id_paciente', $paciente->id)
            ->orderby('id', 'DESC')
            ->first();
        return is_null($prestadorPaciente) ?  PrestadorController::getPrestadores()->first()->id : $prestadorPaciente->id_prestador;
    }

    public static function getPrestadores(){
        return Prestador::from('prestador as pre')
            ->join('hora as hr', 'pre.id', '=', 'hr.prestador')
            ->select('pre.id',
                      DB::raw("CONCAT(pre.nombres,' ', pre.apellidoPaterno,' ', pre.apellidoMaterno) as nombreCompleto"))
            ->where('hr.especialidad', '=', '2')
            ->where('pre.telecetep', '=', '1')
            ->groupby('pre.id')
            ->orderby(DB::raw('pre.nombres, pre.apellidoPaterno'), 'ASC')
            ->get();
    }

    public static function getFechasDisponiblesPrestacion($convenio, $prestacion, $prestadores){
        date_default_timezone_set("Chile/Continental");

        $especialidad = $prestacion->especialidad()->groupBy('id')->get()[0];

        if($prestadores instanceof Collection){
            $idPrestadores = collect($prestadores)->map(function ($prestador){
                return $prestador->id;
            })->reject( function ($prestador) {
                return empty($prestador);
            })->toArray();
        } else {
            $idPrestadores = array($prestadores->id);
        }

        $regla = DetalleRegla::where('idregla', '=', 5)->where('idconvenio', '=', $convenio->idConvenio)->first();
        $cantHorasAgendamiento = $regla->valor;

        $dateTimeDesde = date('Y-m-d H:i:s', strtotime($cantHorasAgendamiento .' hour'));

        $result = Prestador::from('hora as hr')
            ->select(DB::raw('TIME_FORMAT(hr.hora, "%H:%i %p") as hora'),
                DB::raw('DATE(hr.hora) as fecha'))
            ->whereNull('hr.paciente')
            ->whereIn('hr.prestador', $idPrestadores)
            ->where('hr.especialidad','=',$especialidad->id)
            ->whereIn('hr.idTipoReserva', [1,2])
            ->where('hr.idmodalidad','=','1')
            ->where(DB::raw('hr.hora'), '>=', DB::raw('"'.$dateTimeDesde.'"'))
            ->groupby( DB::raw('DATE(hr.hora)'))
            ->orderby(DB::raw('hr.hora'), 'ASC')
            ->get();

        return $result;
    }

    public static function getProxHoraDisponible($convenio, $prestacion, $prestadores, $paciente){
        date_default_timezone_set("Chile/Continental");

        $especialidad = $prestacion->especialidad()->groupBy('id')->get()[0];

        if($prestadores instanceof Collection){
            $idPrestadores = collect($prestadores)->map(function ($prestador){
                return $prestador->id;
            })->reject( function ($prestador) {
                return empty($prestador);
            })->toArray();
        } else {
            $idPrestadores = array($prestadores->id);
        }

        $regla = DetalleRegla::where('idregla', '=', 5)->where('idconvenio', '=', $convenio->idConvenio)->first();
        $cantHorasAgendamiento = $regla->valor;

        $dateTimeDesde = date('Y-m-d H:i:s', strtotime($cantHorasAgendamiento .' hour'));

        $flag = false;
        $contador = 0;

        while(!$flag){
            $result = Prestador::from('hora as hr')
                ->select(DB::raw('TIME_FORMAT(hr.hora, "%H:%i %p") as hora'),
                         DB::raw('DATE(hr.hora) as fecha'),
                         'hr.id as idHora')
                ->whereNull('hr.paciente')
                ->whereIn('hr.prestador', $idPrestadores)
                ->whereIn('hr.idTipoReserva', [1,2])
                ->where('hr.especialidad','=',$especialidad->id)
                ->where('hr.idmodalidad','=','1')
                ->where(DB::raw('hr.hora'), '>=', DB::raw('"'.$dateTimeDesde.'"'))
                ->orderby(DB::raw('hr.hora'), 'ASC')
                ->get();


            if(isset($result[$contador])){
                $proxHora = Hora::find($result[$contador]->idHora);

                $countMatchWithProxHora = Prestador::from('hora as hr')
                    ->select('hr.id')
                    ->where('hr.paciente', $paciente->id)
                    ->whereRaw("'".$proxHora->hora ."' >= SUBTIME(hr.hora, '01:30:00') AND '". $proxHora->hora ."' <= ADDTIME(hr.hora, '01:30:00')")
                    ->get();

                if($countMatchWithProxHora->count() > 0){
                    $contador += 1;
                }
                else{
                    $result = $result[$contador];
                    $flag = true;
                }
            } else {
                $result = null;
                $flag = true;
            }
        }

        return $result;
    }

    public static function getHorasDisponibles($convenio, $prestacion, $prestadores, $fecha){
        date_default_timezone_set("Chile/Continental");
        $especialidad = $prestacion->especialidad()->groupBy('id')->get()[0];

        if($prestadores instanceof Collection){
            $idPrestadores = collect($prestadores)->map(function ($prestador){
                return $prestador->id;
            })->reject( function ($prestador) {
                return empty($prestador);
            })->toArray();
        } else {
            $idPrestadores = array($prestadores->id);
        }

        $regla = DetalleRegla::where('idregla', '=', 5)->where('idconvenio', '=', $convenio->idConvenio)->first();
        $cantHorasAgendamiento = $regla->valor;
        $dateTimeDesde = date('Y-m-d H:i:s', strtotime($cantHorasAgendamiento .' hour'));

        $result = Hora::from('hora as hr')
            ->Join('prestador as pre', 'hr.prestador', '=', 'pre.id')
            ->Join('especialidad as esp', 'hr.especialidad', '=', 'esp.id')
            ->select(
                "hr.id as idHora",
                DB::raw("TIME_FORMAT(hr.hora, '%H:%i') as hora"),
                DB::raw("hr.hora as horaCompleta"),
                "pre.id",
                DB::raw("CONCAT(pre.nombres, ' ', pre.apellidoPaterno, ' ', pre.apellidoMaterno) AS nombrePrestador"),
                "esp.id",
                "esp.especialidad")
            ->whereNull('hr.paciente')
            ->whereIn('pre.id', $idPrestadores)
            ->whereIn('hr.idTipoReserva', [1,2])
            ->where('esp.id', $especialidad->id)
            ->where('hr.idmodalidad','=','1')
            ->whereRaw('if(DATE("'.$fecha.'") = DATE("'.$dateTimeDesde.'"), hr.hora >= "'.$dateTimeDesde.'" AND DATE(hr.hora) = "'.$fecha.'", DATE(hr.hora) = "'.$fecha.'")')
            ->orderby('hr.hora', 'ASC')
            ->get();

        return $result;
    }
}
