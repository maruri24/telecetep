<?php

namespace App\Http\Controllers;

use App\Entities\Prestacion;
use Illuminate\Http\Request;

class PrestacionController extends Controller
{

    public static function getPrestaciones(){
        return Prestacion::orderBy('descripcionespecialidad')->get();
    }
}
