<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Entities\Paciente;
use App\Entities\Prestador;
use App\Entities\User;
use App\Entities\PasswordReset;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Freshwork\ChileanBundle\Rut;
use Illuminate\Support\Facades\Crypt;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:paciente')->except('logout');
        $this->middleware('guest:prestador')->except('logout');
    }

    /**
     * Funcion encargada de retornar la vista del formulario
     * de inicio de sesión
     *
     * @return Vista /resources/views/auth/Login.blade.php
     */
    public function showPrestadorLoginForm()
    {
        return view('auth.login', ['url' => 'prestador']);
    }

    /**
     * Funcion que procesa el incio de sesión de un prestador.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function prestadorLogin(Request $request)
    {
        $asignaClave = false;
        $validator = $this->validateLoginForm($request);

        if($validator->fails()){
            return redirect()->route('showLoginPrestador')
                ->withErrors($validator)
                ->withInput();
        }

        $matchThese = [
            'rut' => $request->input('username'),
            'telecetep' => '1'
        ];


        if(Prestador::where($matchThese)->exists()){
            $usuario = Prestador::where('rut', $request->input('username'))->first();
            $asignaClave = $this->setFirstPassword($usuario, $request->input('password'));
        }

        if (Auth::guard('prestador')->attempt(['rut' => $request->username, 'password' => $request->password], $request->get('remember'))) {

            return $asignaClave == true ?
                redirect()->route('showCreatePasswordPrestador') :
                redirect()->intended('/prestador');
        }
        $validator->errors()->add('login', 'Las credenciales ingresadas son incorrectas');
        return redirect()->route('showLoginPrestador')
            ->withErrors($validator)
            ->withInput();
    }

    /**
     * Funcion encargada de retornar la vista del formulario
     * de inicio de sesión
     *
     * @return Vista /resources/views/auth/Login.blade.php
     */
    public function showPacienteLoginForm()
    {
        return view('auth.login', ['url' => 'paciente']);
    }

    /**
     * Funcion que procesa el incio de sesión de un paciente.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
 public function pacienteLogin(Request $request)
    {

        $asignaClave = false;
        $passwordReset = null;
        $validator = $this->validateLoginForm($request);

        if($validator->fails()){
            return redirect()->route('showLoginPaciente')
                ->withErrors($validator)
                ->withInput();
        }


        $matchThese = [
            'rut' => $request->input('username'),
        ];
        $paciente = Paciente::where('rut', $request->input('username'))->first();
        $usuario = User::where('rut', $request->input('username'))->first();
        if($usuario)
        {
            $passwordReset = PasswordReset::where('email',$usuario->email)->first();
        }

        

        if(!$paciente){
            return redirect()->route('showLoginPaciente')
                ->with('register', 'No se encuentra en el sistema. Si desea registrarse, haga click <a href="/register">Aquí</a>.');
        }
        else if(!$usuario){
            $paciente_registro = [
                'nombres'           => $paciente->nombres,
                'apellidoPaterno'   => $paciente->apellidoPaterno,
                'apellidoMaterno'   => $paciente->apellidoMaterno,
                'rut'               => Rut::set($paciente->rut)->fix()->format(Rut::FORMAT_WITH_DASH)
            ];
            
             $validator->errors()->add('info', 'No se encuentra en el sistema. Si desea registrarse, complete el siguiente formulario:');
             $encid = url('register_paciente/'.Crypt::encrypt($paciente->id));

             return redirect($encid)
                ->withErrors($validator);  
        }
        else if($passwordReset){
            return redirect()->route('showLoginPaciente')
            ->with('danger','La cuenta no se encuentra activa.');     
        }        
        else{
            $asignaClave = $this->setFirstPassword($usuario, $request->input('password'));

            if (Auth::guard('paciente')->attempt(['rut' => $request->username, 'password' => $request->password], $request->get('remember'))) {
//                dd(Auth::guard('paciente')->check());
                return $asignaClave == true ?
                    redirect()->route('showCreatePasswordPaciente') :
                    redirect('/paciente');
            }
            $validator->errors()->add('login', 'Las credenciales ingresadas son incorrectas');
            return redirect()->route('showLoginPaciente')
                ->withErrors($validator);
        }
    }


    /**
     * Funcion que crea la primera password de un usuario
     * en caso de que no la tenga.
     *
     * @param $usuario
     * @param $inputPassword
     * @return int 1 = true, -1 y 0 False
     */
    function setFirstPassword($usuario, $inputPassword){
        if($usuario->password === null){
            $newPassword = substr($usuario->rut, 0, 5);
            $data['password'] = bcrypt($newPassword);

            if($newPassword == $inputPassword){
                $usuario->update($data);
                return 1;
            }else{
                return -1;
            }
        }
        return 0;
    }

    /**
     * Funcion que se encarga de validar que los campos
     * username y email cumplan con las condiciones establecidas.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    function validateLoginForm(Request $request){
        return Validator::make($request->all(),[
            'username' => 'required',
            'password' => 'required|min:5',
        ],[
            'username.required' => 'El campo username es obligatorio',
            'password.required' => 'El campo password es obligatorio',
            'password.min' => 'El campo password debe contener al menos 5 caracteres',
        ]);
    }

//    function validateAuthPaciente($paciente, Request $request){
//        return Auth::guard('paciente')->check() && $request->user('web-paciente')->id === $paciente->id
//    }


    /**
     * Función que finaliza la sesión y redirecciona al formulario de inicio de sesión
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    function logout(){
        $pathToRedirect = Auth::guard('prestador')->check() ? '/prestador' : (Auth::guard('paciente')->check() ? '/paciente': '/');
        Auth::guard('prestador')->logout();
        Auth::guard('paciente')->logout();
        Session::flush();
        return redirect($pathToRedirect);
    }

}
