<?php

namespace App\Http\Controllers\Auth;

use App\Entities\User;
use App\Entities\Paciente;
use App\Entities\Isapre;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Freshwork\ChileanBundle\Rut;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Auth;
use App\Notifications\RegisterToken;
use App\Http\Controllers\Auth\ActivarCuentaController;
use Redirect;
use Illuminate\Support\Facades\Input;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */


    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */


    protected function guard()
    {
        //return Auth::guard('guest');
    }

    public function register(Request $request)
    {
        if(strpos($request->rut, '-'))
        {
            
        }
        else
        {
            $dv = Rut::set($request->rut)->calculateVerificationNumber();
            $request->rut = Rut::parse($request->rut.$dv)->format(Rut::FORMAT_WITH_DASH);
        }
    
        $data = $this->validator($request->all())->validate();

        $rut_sin_dv = substr($request->rut, 0, -2);
        $paciente = Paciente::where('rut', $rut_sin_dv)->first();
        $password = Hash::make($data['password']);
        $user = User::where('rut', $rut_sin_dv)->first();

        if($user)
        {
            return Redirect::back()->withErrors(['El rut ya se encuentra registrado en nuestro sistema.'])->withInput(Input::all());             
        }

        
        if(!$paciente)
        {
            $paciente = Paciente::create([
                'nombres'               => $data['nombres'],
                'apellidoPaterno'       => $data['apellidoPaterno'],
                'apellidoMaterno'       => $data['apellidoMaterno'],
                'celular'               => $data['celular'],
                'rut'                   => $rut_sin_dv,
                'email_telecetep'       => $data['email'],
                'password'              => $password,
                'isapre'                => $data['isapre'],
                'contactoEmergencia_1'  => $request->contacto_emergencia

            ]);
        }
        else
        {
            $paciente->nombres              = $data['nombres'];
            $paciente->apellidoPaterno      = $data['apellidoPaterno'];
            $paciente->apellidoMaterno      = $data['apellidoMaterno'];
            $paciente->celular              = $data['celular'];
            $paciente->password             = $password;
            $paciente->email_telecetep      = $data['email'];
            $paciente->isapre               = $data['isapre'];
            $paciente->contactoEmergencia_1 = $request->contacto_emergencia;

            $paciente->save();
        }       


        $usuario = User::create([
            'idpaciente' => $paciente->id,
            'name'       => $paciente->nombres.' '.$paciente->apellidoPaterno.' '.$paciente->apellidoMaterno,
            'rut'        => $rut_sin_dv,
            'email'      => $data['email'],
            'password'   => Hash::make($data['password']),
        ]);
        
        ActivarCuentaController::create($usuario->id);
        return redirect()->route('showLoginPaciente')
            ->with('success','El usuario se registró correctamente, se envió correo de validación para activar tu cuenta.');
    }

    public function showRegistrationForm()
    {
        $isapres = Isapre::orderBy('isapre')->get();
        return view('auth.register', compact('isapres'));
    }



    protected function validator(array $data)
    {

        return Validator::make($data, [
            'nombres'          => 'required|string|max:255',
            'apellidoPaterno'  => 'required|string|max:255',
            'apellidoMaterno'  => 'required|string|max:255',
            'rut'              => 'required|string|min:9|max:10|unique:users|cl_rut',
            'email'            => 'required|string|email|max:255|unique:users',
            'celular'          => 'required|numeric',
            'password'         => 'required|string|min:6|confirmed',
            'isapre'           => 'required',
        ],
        [
            'email.unique'       => 'El correo electrónico ingresado ya se encuentra en uso por ' . $this->getUsuarioEmailEnUso($data['email']),
            'rut.unique'         => 'El rut ingresado ya se encuentra en uso',
            'rut.cl_rut'         => 'El campo RUT debe contener guion y digito verificador, ejemplo : 1234567-8',
            'password.min'       => 'La contraseña debe contener al menos 6 caracteres.',
            'password.confirmed' => 'La confirmación de contraseña no coincide.'
        ]);   
            
    }

    protected function getUsuarioEmailEnUso($email){
        $usuario = User::where('email', '=', $email)->first();

        if($usuario){
            $arrayName = explode(" ", $usuario->name);

            $nombreOculto = '';
            foreach ($arrayName as $palabra){
                $chars = str_split($palabra);
                $contador = 0;
                foreach ($chars as $letra){
                    $contador += 1;
                    if($contador < 3){
                        $nombreOculto .= $letra;
                    }else{
                        $nombreOculto .= '*';
                    }
                }
                $nombreOculto .= ' ';
            }
            return $nombreOculto;
        }

        return "Otro usuario";

    }

}
