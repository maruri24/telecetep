<?php
namespace App\Http\Controllers\Auth;
use App\Entities\User;
use Carbon\Carbon;
use App\Entities\PasswordReset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Notifications\RegisterToken;
use Illuminate\Support\Facades\DB;


class ActivarCuentaController extends Controller
{
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public static function create($id)
    {
        $user = User::find($id);
        
        $passwordReset = PasswordReset::where('email',$user->email)->first();
        $token = str_random(60);
        DB::table('password_resets')->where('email',$user->email)->delete();
        $passwordReset = DB::table('password_resets')->insert([
            'email' => $user->email, 
            'token' => $token,
            'created_at' => now(),
        ]);
        if ($user && $passwordReset)
            $user->notify(
                new RegisterToken($token,$user)
            );
        return true;
    }
    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)
            ->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        }
        return response()->json($passwordReset);
    }
     /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public static function reset($token)
    {
     
        $passwordReset = PasswordReset::where('token', $token)->first();
        
        if (!$passwordReset)
        {
            return redirect()->route('showLoginPaciente')
            ->with('danger','El link de activación no es valido.');            
        }

        $passwordReset = PasswordReset::where('email',$passwordReset->email)->delete();
        //$user->notify(new PasswordResetSuccess($passwordReset));
        return redirect()->route('showLoginPaciente')
            ->with('success','Tu cuenta se activó exitosamente.');
    }
}