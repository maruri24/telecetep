<?php

namespace App\Http\Controllers\Auth;

use App\Entities\User;
use App\Entities\Paciente;
use App\Entities\Isapre;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Freshwork\ChileanBundle\Rut;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Auth;
use App\Notifications\RegisterToken;
use Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;

class RegisterPacienteController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */


    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */


    protected function guard()
    {
        //return Auth::guard('guest');
    }

    public function register(Request $request)
    {

        if(strpos($request->rut, '-'))
        {
            
        }
        else
        {
           $dv = Rut::set($request->rut)->calculateVerificationNumber();
           $request->rut = Rut::parse($request->rut.$dv)->format(Rut::FORMAT_WITH_DASH);
        }

        $data = $this->validator($request->all())->validate();

        $rut_sin_dv = substr($request->rut, 0, -2);
        $paciente = Paciente::where('rut', $rut_sin_dv)->first();

        $user = User::where('rut', $rut_sin_dv)->first();
       
        if($user)
        {
            return Redirect::back()->withErrors(['El rut ya se encuentra registrado en nuestro sistema.'])->withInput(Input::all());         
        }        
        $password = Hash::make($data['password']);
        if(!$paciente)
        {
            $paciente = Paciente::create([
                'nombres'               => $data['nombres'],
                'apellidoPaterno'       => $data['apellidoPaterno'],
                'apellidoMaterno'       => $data['apellidoMaterno'],
                'celular'               => $data['celular'],
                'rut'                   => $rut_sin_dv,
                'email_telecetep'       => $data['email'],
                'password'              => $password,
                'contactoEmergencia_1'  => $request->contacto_emergencia
            ]);
        }
        else
        {
            $paciente->celular              = $data['celular'];
            $paciente->email_telecetep      = $data['email'];
            $paciente->isapre               = $data['isapre'];
            $paciente->contactoEmergencia_1 = $request->contacto_emergencia;

            $paciente->save();
        }

        
        $usuario = User::create([
            'idpaciente' => $paciente->id,
            'name'       => $paciente->nombres.' '.$paciente->apellidoPaterno.' '.$paciente->apellidoMaterno,
            'rut'        => $rut_sin_dv,
            'email'      => $data['email'],
            'password'   => Hash::make($data['password']),
        ]);

        ActivarCuentaController::create($usuario->id);
        return redirect()->route('showLoginPaciente')
            ->with('success','El usuario se registro correctamente, se envió correo de validación para activar tu cuenta.');
    }

    public function showRegistrationForm($id)
    {
       
        $id = Crypt::decrypt($id);

        $paciente = Paciente::where('id', $id)->first();
        $isapres = Isapre::orderBy('isapre')->get();
        
        $dv = Rut::set($paciente->rut)->calculateVerificationNumber();
        $rut = Rut::parse($paciente->rut.$dv)->format(Rut::FORMAT_WITH_DASH);     

        return view('auth.register_paciente', compact('isapres', 'paciente', 'rut'));
    }



    protected function validator(array $data)
    {
        return Validator::make($data, [

            'email'            => 'required|string|email|max:255|unique:users',
            'celular'         => 'required|numeric',
            'password'         => 'required|string|min:6|confirmed',
            'isapre'           => 'required',
        ],
        [
            'email.unique'       => 'El correo electrónico ingresado ya se encuentra en uso por ' . $this->getUsuarioEmailEnUso($data['email']),
            'password.min'       => 'La contraseña debe contener al menos 6 caracteres.',
            'password.confirmed' => 'La confirmación de contraseña no coincide.'           
        ]);     
            
    }

    protected function getUsuarioEmailEnUso($email){
        $usuario = User::where('email', '=', $email)->first();

        if($usuario){
            $arrayName = explode(" ", $usuario->name);

            $nombreOculto = '';
            foreach ($arrayName as $palabra){
                $chars = str_split($palabra);
                $contador = 0;
                foreach ($chars as $letra){
                    $contador += 1;
                    if($contador < 3){
                        $nombreOculto .= $letra;
                    }else{
                        $nombreOculto .= '*';
                    }
                }
                $nombreOculto .= ' ';
            }
            return $nombreOculto;
        }
        return "Otro usuario";
    }

}
