<?php

namespace App\Http\Controllers;

use App\Entities\Paciente;
use App\Entities\Prestador;
use App\Entities\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WelcomeController extends Controller
{
    /**
     * Recupera los datos relacionados al paciente para
     * poder cargar la vista de bienvenida al paciente
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function welcome(){
        $paciente = Auth::guard('paciente')->user()->paciente;
        $dateTimeActual = HoraController::agendaDelPaciente($paciente)['dateTimeActual'];
        $horasReservadas = HoraController::agendaDelPaciente($paciente)['horasReservadas'];
        $tomorrowDate = HoraController::agendaDelPaciente($paciente)['tomorrowDate'];

        return view('welcome', compact('paciente',  'dateTimeActual', 'horasReservadas', 'tomorrowDate'));
    }

    function bono(){
        $paciente = Auth::guard('paciente')->user()->paciente;
        $dateTimeActual = HoraController::agendadosBono()['dateTimeActual'];
        $horasReservadas = HoraController::agendadosBono()['horasReservadas'];
        $tomorrowDate = HoraController::agendadosBono()['tomorrowDate'];

        return view('prueba', compact('paciente',  'dateTimeActual', 'horasReservadas', 'tomorrowDate'));
    }

    function horaReservada(){
        $paciente = Auth::guard('paciente')->user()->paciente;
        $dateTimeActual = HoraController::horaReservadaFunc()['dateTimeActual'];
        $horasReservadas = HoraController::horaReservadaFunc()['horasReservadas'];
        $tomorrowDate = HoraController::horaReservadaFunc()['tomorrowDate'];

        return view('horaReservada', compact('paciente',  'dateTimeActual', 'horasReservadas', 'tomorrowDate'));
    }

    function welcomePrestador(Prestador $prestador){
        $prestador = Auth::guard('prestador')->user()->id === $prestador->id ? $prestador : Auth::guard('prestador')->user();

        return view('prestador.welcome', compact('prestador'));
    }

    function pruebas(){
        $pares = [
            "grupo_1" => [
                "paciente" => 'Francisco',
                "prestador" => 'Javier',
            ],
            "grupo_2" => [
                "paciente" => 'Rodrigo O.',
                "prestador" => 'Mauricio',
            ],
            "grupo_3" => [
                "paciente" => 'Oscar',
                "prestador" => 'Rodrigo T.',
            ],
            "grupo_4" => [
                "paciente" => 'Luis',
                "prestador" => 'Roberto',
            ],
            "grupo_5" => [
                "paciente" => 'Jonattan',
                "prestador" => 'Eduardo',
            ],
            "grupo_6" => [
                "paciente" => 'Lucas',
                "prestador" => 'Marcelo',
            ]
        ];

        foreach ($pares as $key => $par){
            $token_paciente = md5($par['paciente']);
            $token_prestador =  md5($par['prestador']);

            echo "<h1 style='text-transform:uppercase;'>$key</h1>";
            echo "<strong>".$par['paciente']." (Paciente): </strong> https://app.telecetep.cl/index.html?actor=paciente&token_paciente=$token_paciente&token_prestador=$token_prestador";
            echo "<br><strong>".$par['prestador']." (Prestador): </strong> https://app.telecetep.cl/index.html?actor=prestador&token_paciente=$token_paciente&token_prestador=$token_prestador";
            echo "<br><br>";
        }
    }
}