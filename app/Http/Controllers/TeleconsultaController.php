<?php

namespace App\Http\Controllers;

use App\Entities\Hora;
use App\Entities\Paciente;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class TeleconsultaController extends Controller
{
    //
    function viewTeleconsulta(Hora $hora){
        $paciente = Auth::guard('paciente')->user()->paciente;
        date_default_timezone_set("Chile/Continental");

        $token_paciente = md5($hora->id  .  $paciente->id);
        $token_prestador = md5($hora->id .  $hora->prestador);
        $token_videollamada = $token_paciente.$token_prestador;

        $hora_teleconsulta = date('Y-m-d H:i:s', strtotime($hora->hora));
        $hora_comienzo = date('Y-m-d H:i:s',strtotime('-30 minutes',strtotime($hora_teleconsulta)));
        $hora_fin = date('Y-m-d H:i:s',strtotime('+1 hour +30 minutes',strtotime($hora_teleconsulta)));

        if($hora_teleconsulta >= $hora_comienzo && $hora_teleconsulta <= $hora_fin){
            if(md5($hora->id  .  $paciente->id) == $token_paciente && md5($hora->id .  $hora->prestador) == $token_prestador){
                /*Indicamos que el paciente asiste a la hora.*/
                if($hora->asiste != 'SI'){
                    $hora->horaasiste = date('Y-m-d H:i:s');
//                    $hora->asiste = 'SI';
                    $flag = $hora->save();
                }

                return view('teleconsulta', compact('paciente', 'hora', 'token_videollamada'));
            }
        }
    }

    function atencionExterna($idHora){
        date_default_timezone_set("Chile/Continental");

        $hora = Hora::find($idHora);
        $flag = false;

        if($hora->paciente){

            $paciente = Paciente::find($hora->paciente);
            $token_paciente = md5($hora->id  .  $paciente->id);
            $token_prestador = md5($hora->id .  $hora->prestador);
            $token_videollamada = $token_paciente.$token_prestador;

            $hora_teleconsulta = date('Y-m-d H:i:s', strtotime($hora->hora));
            $hora_comienzo = date('Y-m-d H:i:s',strtotime('-30 minutes',strtotime($hora_teleconsulta)));
            $hora_fin = date('Y-m-d H:i:s',strtotime('+1 hour +30 minutes',strtotime($hora_teleconsulta)));
            $hora_actual = date('Y-m-d H:i:s');

            if($hora_actual >= $hora_comienzo && $hora_actual <= $hora_fin){
                $flag = true;
                return view('atencionExterna', compact('flag','mensaje', 'paciente', 'hora', 'token_videollamada'));
            }
            elseif ($hora_actual < $hora_comienzo){
                Carbon::setLocale('es');
                $fecha_ES = Carbon::parse($hora_teleconsulta);
                $mensaje = 'Esta atención comenzará ' . $fecha_ES->diffForHumans();
            }
            elseif ($hora_actual > $hora_fin){
                Carbon::setLocale('es');
                $fecha_ES = Carbon::parse($hora_teleconsulta);
                $mensaje = 'Esta atención finalizó ' . $fecha_ES->diffForHumans();
            }
            return view('atencionExterna', compact('flag','mensaje', 'paciente', 'hora', 'token_videollamada'));
        }
        $mensaje = 'Ha ocurrido un error. Por favor contactate con soporte';
        return view('atencionExterna', compact('flag','mensaje', 'paciente', 'hora', 'token_videollamada'));
    }
}
