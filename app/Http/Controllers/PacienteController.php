<?php

namespace App\Http\Controllers;

use App\Entities\Isapre;
use App\Entities\Paciente;
use App\Entities\Hora;
use App\Entities\Prestador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PacienteController extends Controller
{

    protected $redirectTo = '/';

    public function __construct()
    {
//        $this->middleware('guest:paciente');
    }

    public function mis_datos()
    {
        $paciente = Auth::guard('paciente')->user()->paciente;
        $isapres = Isapre::orderBy('isapre')->get();
        return view('mis_datos', compact('paciente', 'isapres'));
    }

    public function actualizar_mis_datos(Request $request){
        $paciente = Auth::guard('paciente')->user()->paciente;
        $user = Auth::guard('paciente')->user();
        $isapres = Isapre::orderBy('isapre')->get();

        $validator = Validator::make($request->all(),[
            'nombres' => 'required',
            'apellidoPaterno' => 'required',
            'apellidoMaterno' => 'required',
            'email' => [
                'required',
                'email',
                Rule::unique('paciente', 'email_telecetep')->ignore($paciente->id),
                Rule::unique('users')->ignore($user->id),
            ],
            'celular' => 'required|numeric',
            'isapre' => 'required|not_in:x'
        ],[
            'username.required' => 'El campo RUT es obligatorio',
            'nombres.required' => 'El campo nombres es obligatorio',
            'apellidoPaterno.required' => 'El campo Apellido Paterno es obligatorio',
            'apellidoMaterno.required' => 'El campo Apellido Materno es obligatorio',
            'email.required' => 'El campo Correo electrónico es obligatorio',
            'email.email' => 'El campo Correo electrónico debe contener un formato válido',
            'email.unique' => 'El correo electrónico ingresado ya se encuentra en uso',
            'celular.required' => 'El campo Teléfono celular es obligatorio',
            'celular.numeric' => 'El campo Teléfono celular solo acepta números',
            'isapre.required' => 'El campo Seguro de salud es obligatorio',
            'isapre.not_in' => 'Debe seleccionar un Seguro de salud'
        ]);

        if($validator->fails()){
            return redirect()->route('mis_datos')
                ->withErrors($validator)
                ->withInput();
        }


        $paciente->nombres = $request->nombres;
        $paciente->apellidoPaterno = $request->apellidoPaterno;
        $paciente->apellidoMaterno = $request->apellidoMaterno;
        $paciente->email_telecetep = $request->email;
        $paciente->celular = $request->celular;
        $paciente->isapre = $request->isapre;
        $paciente->contactoEmergencia_1 = $request->contacto_emergencia;
        $user->name = $request->nombres.' '.$request->apellidoPaterno.' '.$request->apellidoMaterno;
        $user->email = $request->email;

        $flagPaciente = $paciente->save();
        $flagUser = $user->save();

        if($flagPaciente && count($paciente->getChanges()) > 0){
            $mensaje = "Los datos se han actualizado exitosamente.";
            $paciente = Paciente::find($paciente->id);
        } elseif($flagPaciente){
            $mensaje = null;
        } else{
            $mensaje = "No se han podido actualizar los datos";
        }

        return view('mis_datos', compact('paciente','isapres', 'mensaje', 'flagPaciente'));
    }

    public function historialDelPaciente(){
        $paciente = Auth::guard('paciente')->user()->paciente;
        return view('mi_historial', compact('paciente'));
    }


    public function historialDelPaciente_ajax(Request $request){
        $result = array(
            "data" => HoraController::historialDelPaciente($request->id_paciente)
        );
        return response()->json($result);
    }

    public function buscarPacienteAjax(Request $request){
        $paciente = Paciente::where('rut', $request->rut)->first();
        $result = array(
            'pacienteExists' => isset($paciente),
            'paciente' => $paciente
        );
        return response()->json($result);
    }

    public function registrarPacienteAjax(Request $request){
        $validator = Validator::make($request->all(),[
            'rut' => 'required|numeric',
            'nombres' => 'required',
            'apellidoPaterno' => 'required',
            'apellidoMaterno' => 'required',
            'email' => 'required|email|unique:users',
            'telefono' => 'required|numeric',
            'celular' => 'required|numeric',
        ],[
            'rut.required' => 'El campo rut es obligatorio',
            'nombres.required' => 'El campo nombres es obligatorio',
            'apellidoPaterno.required' => 'El campo Apellido Paterno es obligatorio',
            'apellidoMaterno.required' => 'El campo Apellido Materno es obligatorio',
            'email.required' => 'El campo Correo electrónico es obligatorio',
            'email.email' => 'El campo Correo electrónico debe contener un formato válido',
            'email.unique' => 'El correo electrónico ingresado ya se encuentra en uso',
            'telefono.required' => 'El campo Teléfono fijo es obligatorio',
            'telefono.numeric' => 'El campo Teléfono fijo solo acepta numeros',
            'celular.required' => 'El campo Teléfono celular es obligatorio',
            'celular.numeric' => 'El campo Teléfono celular solo acepta numeros',
        ]);

        if($validator->fails()){
            $result = array(
                'registered' => false,
                'errors' => $validator->errors()
            );
            return response()->json($result);
        }

        if(Paciente::where('rut', $request->rut)->first()){
            $validator->errors()->add('exists', 'El rut ingresado ya se encuentra registrado.');
            $result = array(
                'registered' => false,
                'errors' => $validator->errors()
            );
            return response()->json($result);
        }

        $paciente = new Paciente();
        $paciente->rut = $request->rut;
        $paciente->nombres = $request->nombres;
        $paciente->apellidoPaterno = $request->apellidoPaterno;
        $paciente->apellidoMaterno = $request->apellidoMaterno;
        $paciente->email = $request->email;
        $paciente->telefono = $request->telefono;
        $paciente->celular = $request->celular;
        $paciente->telecetep = 1;

        $flag = $paciente->save();
        if($flag){
            $result = array(
                'registered' => true,
                'paciente' => $paciente
            );
            return response()->json($result);
        }
    }

    public function showPerfilPaciente(Prestador $prestador, Paciente $paciente){
        date_default_timezone_set("Chile/Continental");

        $dateTimeActual = date('Y-m-d H:i:s');
        $tomorrowDate = date('Y-m-d H:i:s', strtotime($dateTimeActual . " +1 days"));

        /*En caso de que en un futuro se puedan realizar más de 2 reservas,
        modificar la siguiente variables*/
        $limiteDeReservas = 2;

        $horasReservadas = Hora::from('hora AS hr')
            ->join('prestador AS pre', 'hr.prestador', '=', 'pre.id')
            ->join('especialidad AS esp', 'hr.especialidad', '=', 'esp.id')
            ->join('paciente AS pac', 'hr.paciente', '=', 'pac.id')
            ->select(
                'hr.id as idHora',
                DB::raw('(DATE_FORMAT(hr.hora,"%d-%m-%Y")) AS fecha'),
                DB::raw('(TIME_FORMAT(hr.hora,"%H:%i %p")) AS hora'),
                'hr.hora as horaCompleta',
                DB::raw('CONCAT(pre.nombres," ",pre.apellidoPaterno) AS prestador'),
                'esp.especialidad',
                DB::raw('(ADDTIME(hr.hora,"-00:30:00")) AS horaComienzo'),
                DB::raw('(ADDTIME(hr.hora,"1:30:00")) AS horaExpiracion')
            )
            ->where('esp.id', 2)
            ->where('hr.paciente', $paciente->id)
            ->where(DB::raw('(ADDTIME(hr.hora,"1:30:00"))'), '>=', $dateTimeActual)
            ->orderby('hr.hora', 'ASC')
            ->get();

        $countReservas = $horasReservadas->count();
        return view('prestador.perfilPaciente', compact('prestador', 'paciente', 'horasReservadas', 'dateTimeActual',  'limiteDeReservas', 'countReservas', 'tomorrowDate'));
    }


    /**--------------------------------------------------------
     * - Funciones estaticas
     * --------------------------------------------------------
     */
    public static function formatoRut($rut){
        return self::setRutFormat($rut) .'-'. self::dv($rut);
    }

    public static function dv($_rol){
        $s=1;

        for($m=0;$_rol!=0;$_rol/=10){
            $s=($s+$_rol%10*(9-$m++%6))%11;
        }

        return chr($s?$s+47:75);
    }

    public static function setRutFormat($numero){
        if($numero == NULL){ return 0; }
        else {
            //Si es float
            if(substr_count($numero, '.') != 0) {
                $numero=number_format($numero,2,",",".");
            }
            else {
                $numero=number_format($numero,0,",",".");
            }
            return $numero;
        }
    }

    public static function getEstadoCivil($idEstadoCivil){
        return DB::table('estadocivil')
                    ->select('descripcion')
                    ->where('idestadocivil', $idEstadoCivil)
                    ->first();
    }

    public static function getIsapre($idIsapre){
        return DB::table('isapre')
            ->select('isapre')
            ->where('id', $idIsapre)
            ->first();
    }

    public static function getGenero($idGenero){
        return DB::table('sexo')
            ->select('sexo')
            ->where('idsexo', $idGenero)
            ->first();
    }

    public static function getComuna($idComuna){
        return DB::table('comuna')
            ->select('NombreComuna')
            ->where('codcomuna', $idComuna)
            ->first();
    }

}
