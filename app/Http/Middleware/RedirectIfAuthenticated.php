<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if ($guard == "prestador" && Auth::guard($guard)->check()) {
            $prestador = Auth::guard('prestador')->user();
            return redirect()->route('welcomePrestador',compact('prestador'));
        }
        if ($guard == "paciente" && Auth::guard($guard)->check()) {
            return redirect()->route('welcome');
        }

        if (Auth::guard($guard)->check()) {
            return redirect()->route('welcome');
        }
//        dd($request);
        return $next($request);
    }
}
