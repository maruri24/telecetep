<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		    <td colspan='4' height=''></td>
	    </tr>
	    <tr>
	        <td align="center" style="padding: 50px 0px 50px 0px" bgcolor="#E2EFD9">
	            <table width='550' bgcolor="white">
					<tr>
						<td colspan='4' height='40px' bgcolor='#0E8333' style='text-align: center; color: white; font-size: 22px' >
							<strong>Cambio de Contraseña</strong>
						</td>
					</tr>
					<tr>
						<td colspan='4' style='text-align: center; color: #0E8333;'>
							<h2 style='font-size: 18px; margin: 20px 0px 10px 10px;'><strong>Estimado(a): {{$user->nombres}} {{$user->apellidoPaterno}} {{$user->apellidoMaterno}}</strong></h2>
						</td>
					</tr>
					<tr>
					    <td colspan='4' style='text-align: center; color: #727371;'>
							<p style='font-size: 16px; margin: 0px 0px 20px 20px;'>Has cambiado tu contraseña correctamente. <br>
							Si cambió la contraseña, no se requiere ninguna otra acción.
							</p>
						</td>
			        </tr>					
					<tr>
						<td width="50px"></td>
						<td colspan="2" style="text-align: center; color: #727371; padding: 20px 0px;">
							<small>Si no cambió la contraseña, proteja su cuenta.</small>

						</td>
						<td width="50px"></td>
					</tr>
					<tr>
						<td colspan="4" style="text-align: center;" width='150'>
							<img src="http://cetep.cl/imgMailing/GC_CETEP_150px.png" style="width: 150px" alt="">
						</td>
					</tr>
					<tr>
					    <td height='50' colspan='4'></td>
                    </tr>
				</table>
	        </td>
	    </tr>
	</table>
</body>