<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan='4' height=''></td>
        </tr>
        <tr>
            <td align="center" style="padding: 50px 0px 50px 0px" bgcolor="#E2EFD9">
                <table width='550' bgcolor="white">
                    <tr>
                        <td colspan='4' height='40px' bgcolor='#0E8333' style='text-align: center; color: white; font-size: 22px' >
                            <strong>Detalle hora de atención</strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='4' style='text-align: center; color: #0E8333;'>
                            <h2 style='font-size: 18px; margin: 20px 0px 10px 10px;'><strong>Estimado(a): <span style="text-transform:uppercase;">{{ $detalleReserva->nombrePaciente }}</span></strong></h2>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='4' style='text-align: center; color: #727371;'>
                            <p style='font-size: 16px; margin: 0px 0px 20px 20px;'>Hemos recibido la siguiente reserva:</p>
                        </td>
                    </tr>
                    <!--
                    <tr bgcolor="#E2EFD9">
                        <td width="50" bgcolor="white"></td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">ID Reserva</td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">46285045</td>
                        <td width="50" bgcolor="white"></td>
                    </tr>
                    -->
                    <tr bgcolor="#E2EFD9">
                        <td width="50" bgcolor="white"></td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">Rut del paciente</td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">{{ formatoRut($detalleReserva->rut) }}</td>
                        <td width="50" bgcolor="white"></td>
                    </tr>
                    <tr bgcolor="">
                        <td width="50" bgcolor="white"></td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">Especialidad</td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">{{ $detalleReserva->especialidad }}</td>
                        <td width="50" bgcolor="white"></td>
                    </tr>
                    <tr bgcolor="#E2EFD9">
                        <td width="50" bgcolor="white"></td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">Profesional</td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">{{$detalleReserva->nombrePrestador}}</td>
                        <td width="50" bgcolor="white"></td>
                    </tr>
                    <tr bgcolor="">
                        <td width="50" bgcolor="white"></td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">Fecha</td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">{{ $detalleReserva->fecha }}</td>
                        <td width="50" bgcolor="white"></td>
                    </tr>
                    <tr bgcolor="#E2EFD9">
                        <td width="50" bgcolor="white"></td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">Hora</td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">{{ $detalleReserva->hora }}</td>
                        <td width="50" bgcolor="white"></td>
                    </tr>
                    <!--
                    <tr bgcolor="">
                        <td width="50" bgcolor="white"></td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">Ubicación</td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">San Pío X 2460 Of.1506, Providencia</td>
                        <td width="50" bgcolor="white"></td>
                    </tr>
                    <tr bgcolor="#E2EFD9">
                        <td width="50" bgcolor="white"></td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;">Mapa</td>
                        <td style="color: #727371; padding: 5px 0px 5px 40px;"><a href="maps.google.com">Ver mapa</a></td>
                        <td width="50" bgcolor="white"></td>
                    </tr>
                    -->
                    <tr>
                        <td width="50px"></td>
                        <td colspan="2" style="text-align: center; color: #727371; padding: 20px 0px;">
                            Recomendamos ingresar a la Plataforma TeleCetep con <strong>15 minutos</strong> de anticipación a su consulta.<br>
                                    Para tomar una nueva hora comuníquese al <strong>2 2604 4000.</strong>
                        </td>
                        <td width="50px"></td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: center;" width='150'>
                            <img src="http://cetep.cl/imgMailing/GC_CETEP_150px.png" style="width: 150px" alt="">
                        </td>
                    </tr>
                    <tr>
                        <td height='50' colspan='4'></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>