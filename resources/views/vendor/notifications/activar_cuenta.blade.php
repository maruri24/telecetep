
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		    <td colspan='4' height=''></td>
	    </tr>
	    <tr>
	        <td align="center" style="padding: 50px 0px 50px 0px" bgcolor="#E2EFD9">
	            <table width='550' bgcolor="white">
					<tr>
						<td colspan='4' height='40px' bgcolor='#0E8333' style='text-align: center; color: white; font-size: 22px' >
							<strong>Activar cuenta</strong>
						</td>
					</tr>
					<tr>
						<td colspan='4' style='text-align: center; color: #0E8333;'>
							<h2 style='font-size: 18px; margin: 20px 0px 10px 10px;'><strong>Estimado(a): {{$user->name}}</strong></h2>
						</td>
					</tr>
					<tr>
					    <td colspan='4' style='text-align: center; color: #727371;'>
							<p style='font-size: 16px; margin: 0px 0px 20px 20px;'>Te registraste recientemente en Telecetep. Para completar tu registro confirma tu cuenta en el siguiente enlace:</p>
						</td>
			        </tr>
					<tr>
					    <td colspan='4' style='text-align: center; color: #727371;'>
							<p style='font-size: 40px; margin: 0px 0px 20px 0px;'>
								<strong>
								<center><a style=" background-color: #4CAF50; border: none;  color: white;  padding: 15px 32px;  text-align: center;  text-decoration: none;  display: inline-block;  font-size: 16px;  margin: 4px 2px;  cursor: pointer;  -webkit-transition-duration: 0.4s; transition-duration: 0.4s;" href="{{$url}}">Activar cuenta</a></center>
								</strong>
							</p>
						</td>
			        </tr>			        
					<tr>
						<td width="50px"></td>
						<td colspan="2" style="text-align: center; color: #727371; padding: 20px 0px;">
							<small>
							Si tiene problemas para hacer clic en el botón "Activar Cuenta", copie y pegue la URL en su navegador web: {{$url}}
							</small>
						</td>
						<td width="50px"></td>
					</tr>
					<tr>
						<td colspan="4" style="text-align: center;" width='150'>
							<img src="http://cetep.cl/imgMailing/GC_CETEP_150px.png" style="width: 150px" alt="">
						</td>
					</tr>
					<tr>
					    <td height='50' colspan='4'></td>
                    </tr>
				</table>
	        </td>
	    </tr>
	</table>
</body>