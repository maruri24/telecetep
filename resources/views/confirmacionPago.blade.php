@if ($output->responseCode == 0)
<script>
    window.localStorage.clear();
    window.localStorage.setItem('buyOrder','{{$output->buyOrder}}');
    window.localStorage.setItem('cardNumber','{{$result->cardDetail->cardNumber}}');
    window.localStorage.setItem('transactionDate','{{\Carbon\Carbon::parse($result->transactionDate)->format('d/m/Y H:i:s')}}');
    window.localStorage.setItem('authorizationCode','{{$output->authorizationCode}}');
    window.localStorage.setItem('paymentTypeCode','{{$output->paymentTypeCode}}');
    window.localStorage.setItem('amount','{{number_format($output->amount,0, ',', '.')}}');
    window.localStorage.setItem('responseCode','{{$output->responseCode}}');
</script>
@endif

{{ App\Http\Controllers\TransbankController::store($output,$result,$hora) }}

@if ($output->responseCode == 0)
<form action="{{$result->urlRedirection}}" method="POST" id="return-form">
    {{ csrf_field() }}
    <input type="hidden" name="token_ws" value="{{$tokenWS}}">
</form>

<script>
   document.getElementById('return-form').submit();
</script>
@endif