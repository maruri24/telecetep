@extends('layout')

@section('customCss')
    <style>
        h3{
            margin-bottom: 20px;
            font-family: 'Roboto', sans-serif;
            font-size: 1em;
            text-align: center;
        }
        a#btnVolver{
            text-transform: uppercase;
            width: 200px;
        }
        figure{
            margin-top: 50px;
            width: 200px;
            margin: auto;
            padding: 0;
        }
        figure img{
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 id="pageTitle">Hora reservada</h2>
        </div>
    </div>

    <div class="row d-flex justify-content-center">
        <div class="col-md-6">
            <figure>
                @if($flag)
                    <img src="{{ asset('check.png') }}" alt="imagen check">
                @else
                    <img src="{{ asset('warning.png') }}" alt="imagen check">
                @endif
            </figure>
            <h3>
                @if($flag)
                    El proceso de agendamiento ha finalizado con éxito. En los próximos minutos, usted recibirá un correo con toda la información
                    relacionada a la reserva.
                @else
                    El proceso de agendamiento ha sido interrumpido. Lamentablemente no hemos podido generar la reserva. Inténtelo nuevamente más tarde.
                @endif
            </h3>
            <div class="btnContentReserva d-flex justify-content-center">
                <a href="{{ route('welcome') }}" id="btnVolver" class="btn btn-primary">Volver a inicio</a>
            </div>
        </div>
    </div>
@endsection


@section('jsScripts')
    <script>
        $("nav ul li a").removeClass('active');
        $(".menuMis_consultas").addClass('active');
    </script>
@endsection
