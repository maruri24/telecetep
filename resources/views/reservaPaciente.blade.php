@extends('layout')

@section('customCss')
    @parent
    <link rel="stylesheet" href="{{ asset('css/agendarHora.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nigran.datepicker.css') }}">
@append

@section('content')
    <div class="loadingGif">
        <img src="{{asset('loading.gif')}}" alt="">
    </div>
    <div class="bluredBackground"></div>
    <div class="row">
        <div class="col-md-8">
            <h2 id="pageTitle">Agendar Hora</h2>
        </div>
        <div class="col-md-4 location-container">
            <div class="form-group">
                <label for="location">Ubicación:</label>
                <select name="location" id="location" class="form-control" disabled>
                    <option value="all" {{ $location == 'all' ? 'selected' : '' }}>Todas</option>
                    <option value="4" {{ $location == '4' ? 'selected' : '' }}>Concepción</option>
                    <option value="1" {{ $location == '1' ? 'selected' : '' }}>Las Condes, Santiago</option>
                    <option value="2" {{ $location == '2' ? 'selected' : '' }}>Providencia, Santiago</option>
                    <option value="3" {{ $location == '3' ? 'selected' : '' }}>Rancagua</option>
                </select>
                <a class="location-info" data-toggle="popover" tabindex="0"><i class="fa fa-info-circle" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="steps col-md-12">
            <div class="row">
                @if(count($convenios) > 1)
                <div class="col-md-4">
                    <span class="stepNumber"><em class="">1</em> Convenio</span>
                    <div class="form-group">
                        <label for="convenio">Seleccione el convenio asociado a la reserva</label>
                        <select name="convenio" id="convenio" class="form-control">
                            @foreach($convenios as $convenio)
                                <option value="{{$convenio->idConvenio}}">{{$convenio->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @endif
                <div class="col-md-4">
                    <span class="stepNumber"><em class="">2</em> Especialidad</span>
                    <div class="form-group">
                        <label for="prestacion">Seleccione la especialidad</label>
                        <select name="prestacion" id="prestacion" class="form-control">
                            @foreach($prestaciones as $prestacion)
                                <option value="{{$prestacion->idprestacion}}">{{$prestacion->descripcionespecialidad}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <span class="stepNumber"><em class="">3</em> Profesional</span>
                    <div class="form-group">
                        <label for="prestador">Seleccione un profesional</label>
                        <select name="prestador" id="prestador" class="form-control">
                            <option value="x">TODOS LOS PROFESIONALES</option>
                            @foreach($prestadores as $prestador)
                                <option value="{{ $prestador->id }}">{{$prestador->nombres}} {{$prestador->apellidoPaterno}} {{$prestador->apellidoMaterno}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-6">
                    <span class="stepNumber"><em>4</em> Fecha</span>
                    <div class="datepicker-container">
                        <label for="date">Seleccione una fecha</label>
                        <div id="datepicker" class="ll-skin-latoja">
                        </div>
                        <input type="hidden" value="" name="date" id="date" />
                        <div id="proxHoraDisponible" class="alert alert-primary" role="alert">
                            <em>Próxima hora disponible:</em> <span></span>
                        </div>
                    </div>
                </div>
                <div id="stepHoras" class="steps col-md-6">
                    <span class="stepNumber"><em>5</em> Hora</span>
                    <div class="horasDisponibles">
                        <div id="alterInfoHora" class="alert alert-info" style="display: none">
                            Seleccione una hora para continuar
                        </div>
                        <div id="alertaPrevia" class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">¡Atención!</h4>
                            <hr>
                            <p>Para reservar una hora, debe indicar el profesional y fecha relacionada a la consulta.</p>
                        </div>
                        <div class="horasContent">
                        </div>
                    </div>
                    <div class="btnContentAgendar">
                        <button type="button" id="btnContinuar" class="btn btn-primary" disabled>Continuar con la reserva</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('jsScripts')
    @parent
    @include('js.jsReservaPaciente')
@append
