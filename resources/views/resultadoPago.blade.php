@extends('layout')

@section('customCss')
    <link rel="stylesheet" href="{{ asset('css/detallePago.css') }}">
    <style>
        figure{
            margin-top: 50px;
            width: 200px;
            margin: auto;
            padding: 0;
        }
        figure img{
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($flag == 1)
            <h2 id="pageTitle">Pago realizado</h2>
            @elseif($flag == 2)
            <h2 id="pageTitle">Pago cancelado</h2>
            @else
            <h2 id="pageTitle">Pago Rechazado</h2>
            @endif
        </div>
    </div>
    @if($flag == 1)
    <div class="row d-flex justify-content-center">
        <div class="col-md-6">
            <figure>
                <img src="{{ asset('check.png') }}" alt="imagen check">
            </figure>
            <h3>
                Su pago se ha realizado con éxito.
            </h3>
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                    <table>
                        <tr>
                            <td>ID pago</td>
                            <td id="buyOrder"></td>
                        </tr>
                        <tr>
                            <td>Fecha pago</td>
                            <td id="transactionDate"></td>
                        </tr>
                        <tr>
                            <td>Tarjeta</td>
                            <td id="cardNumber"></td>
                        </tr>
                        <tr>
                            <td>Monto</td>
                            <td id="amount"></td>
                        </tr>
                    </table>
                    <div class="btnContentReserva d-flex justify-content-center">
                        <a href="{{ route('welcome') }}" id="btnVolver" class="btn btn-primary">Volver a inicio</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif($flag == 2)
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <figure>
                    <img src="{{ asset('warning.png') }}" alt="imagen check">
                </figure>
                <h3>
                    Pago cancelado.
                </h3>
                <div class="row d-flex justify-content-center">
                    <div class="col-md-12">
                        <table>
                            <tr>
                                <td>Fecha Transacción</td>
                                <td>{{date('d/m/Y H:i:s')}}</td>
                            </tr>
                        </table>
                        <div class="btnContentReserva d-flex justify-content-center">
                            <a href="{{ route('welcome') }}" id="btnVolver" class="btn btn-primary">Volver a inicio</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
    <div class="row d-flex justify-content-center">
        <div class="col-md-6">
            <figure>
                <img src="{{ asset('warning.png') }}" alt="imagen check">
            </figure>
            <h3>
                Su pago ha sido rechazado.
            </h3>
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                    <table>
                        <tr>
                            <td>ID pago</td>
                            <td>{{$output->buyOrder}}</td>
                        </tr>
                        <tr>
                            <td>Fecha pago</td>
                            <td>{{date('d/m/Y H:i:s')}}</td>
                        </tr>
                        <tr>
                            <td>Monto</td>
                            <td>$ {{number_format($output->amount,0, ',', '.')}}</td>
                        </tr>
                    </table>
                    <div class="btnContentReserva d-flex justify-content-center">
                        <a href="{{ route('welcome') }}" id="btnVolver" class="btn btn-primary">Volver a inicio</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection


@section('jsScripts')
    <script>
        $("nav ul li a").removeClass('active');
        $(".menuMis_consultas").addClass('active');
        $('#buyOrder').html(window.localStorage.getItem('buyOrder'));
        $('#transactionDate').html(window.localStorage.getItem('transactionDate'));
        $('#cardNumber').html("**** **** **** "+window.localStorage.getItem('cardNumber'));
        $('#amount').html("$ "+window.localStorage.getItem('amount'));
        $('#paymentTypeCode').html(window.localStorage.getItem('paymentTypeCode'));
        $('#authorizationCode').html(window.localStorage.getItem('authorizationCode'));
        $('#responseCode').html(window.localStorage.getItem('responseCode'));
    </script>
@endsection
