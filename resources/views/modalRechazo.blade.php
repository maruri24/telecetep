<div class="modal fade" id="rechazo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalReqPBLabel">Rechazar reserva</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>×</span>
                </button>
            </div>
            <form id="rechbono" name="rechbono"  method="POST" action="{{'/rechBono'}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">

                    <div class="col-md-9">
                        <h4>Ingrese razón de rechazo </h4>
                        <input type="text" name="motivo" id="motivo" class="form-control" >
                    </div>

                    <div class="col-md-9">
                        <input type="text" name="idRegBonoHorasRe" id="idRegBonoHorasRe"class="form-control" value="" hidden="hidden" >
                        <input type="text" name="idHoraRe" id="idHoraRe"class="form-control" value="" hidden="hidden">
                    </div>

                    </div>
                    <br>
                    <div class="col"><b>El rechazo de una hora no libera esta, solo permite al paciente volver a ejecutar el pago*</b></div>

                    <br>
                <div class="col-md-10">
                    <div id="error" class="alert alert-danger" role="alert" style="display:none;">

                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger" value="Liberar">Rechazar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>