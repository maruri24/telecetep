@extends('layout')
@section('customCss')
    <style>
        h3{
            margin-top: 20px;
            margin-bottom: 10px;
            text-transform: uppercase;
            font-family: 'Roboto', sans-serif;
            font-size: 1.2em;
        }
        .alert{ margin-top: 20px; }
        .embed-responsive{ height: calc(100vh - 70px); }
        .content{ padding: 0 !important; }
    </style>
@endsection


@section('content')
    <div class="embed-responsive embed-responsive-4by3">
        {{--                <iframe class="embed-responsive-item" src="https://app.telecetep.cl/index.html?actor=paciente&token_paciente={{ $token_paciente }}&token_prestador={{ $token_prestador }}" allow="geolocation; microphone; camera"></iframe>--}}
        <iframe class="embed-responsive-item" src="https://jitsi.telecetep.cl/{{ $token_videollamada }}" allow="geolocation; microphone; camera; fullscreen"></iframe>
    </div>
@endsection

@section('jsScripts')
    <script>

        $(function () {
            var windowHeight =  $( window ).height();
            var headerHeight = $("header").height();

            $(".embed-responsive").css('height', windowHeight - headerHeight + 'px');

            $("nav ul li a").removeClass('active');
            $(".menuMis_consultas").addClass('active');

        });
    </script>
@endsection