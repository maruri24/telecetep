
<div class="modal fade" id="ModalB" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="modalBonoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalBonoLabel">Pago con bono</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Estimad@ {{$paciente->nombres}} {{$paciente->apellidoPaterno}} {{$paciente->apellidoMaterno}},
                    en esta sección podrá pagar con bono, indicando el número de bono y adjuntando una imagen completa de este.
                    El bono será validado con posterioridad por un operario de TELECETEP.</p>

                <form name="ingBono" id="ingBono" action="{{'/ingresoBono'}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="idBono">Número Bono</label>
                        <input id="idBono" name="idBono" type="text" class="form-control" pattern="[0-9]*" placeholder="Ingrese numero de bono" value="" required >
                    </div>

                    <div class="form-group">
                        <label for="fechab">Fecha emisión Bono</label>
                        <input type="date" name="fechab" id="fechab" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="bonoFile"> Adjuntar Bono</label>
                        <em>(Solo formatos: pdf, png, jpeg, jpg)</em>
                        <input type="file" class="form-control" name="bonoFile" id="bonoFile" required>
                    </div>

                    <input type="text" id="idHora" name="idHora" hidden="hidden" value="{{$detalleReserva->idHora}}" required>
                    <input type="text" id="monto" name="monto" hidden="hidden" value="{{$parametros->monto}}" required>
                    <input type="text" id="convenio" name="convenio" hidden="hidden" value="{{$convenio->idConvenio}}">
                    <input type="text" id="prestacion" name="prestacion" hidden="hidden" value="{{$prestacion->idPrestacion}}">

                    <div id="errores" class="alert alert-danger" role="alert" style="display:none;">
                    </div>

                    <input type="submit" class="btn btn-primary" value="Guardar">
                </form>
            </div>
        </div>
    </div>
</div>


@section('jsScripts')
    @parent
<script>
    $(function(){
        $("form#ingBono").submit(function(e){
            e.preventDefault();

            var form_data = new FormData($("#ingBono")[0]);

            $.ajax({
                url: "{{ route('ingresoBono') }}",
                method: "POST",
                data: form_data,
                dataType: 'JSON',
                contentType: false,
                cache:false,
                processData: false,
                success: function (data) {
                    if(data.registered){
                        window.location.href ='/confirmarReserva'+'/'
                            + {!!json_encode($idhoraEncrypted)!!} +
                            '/' + {!! json_encode($convenio->idConvenio)!!} +
                            '/' +{!! json_encode($prestacion->idprestacion)!!}
                    } else{
                        $("#errores").css('display', 'inherit');
                        $("#errores").html('');
                        $.each(data.errors, function(i, error){
                            $("#errores").append('<p>' + error + '</p>');
                        });
                        console.log(data.errors);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        })
    });
</script>
@append