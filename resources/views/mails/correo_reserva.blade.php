<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Llamado de emergencia</title>
</head>
<body>
<p>Hola! Se ha reportado un nuevo caso de emergencia a las 15:00.</p>
<p>Estos son los datos del usuario que ha realizado la denuncia:</p>
<ul>
    <li>Nombre: Lucas</li>
    <li>Teléfono: 995544778</li>
    <li>DNI: 1111111-1</li>
</ul>
<p>Y esta es la posición reportada:</p>
<ul>
    <li>Latitud: 78.542321</li>
    <li>Longitud: -18.5152</li>
    <li>
        <a href="https://google.com">
            Ver en Google Maps
        </a>
    </li>
</ul>
</body>
</html>