@extends('layout')

@section('customCss')
    <link rel="stylesheet" href="{{ asset('css/detalleReserva.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 id="pageTitle">Detalle reserva de hora</h2>
        </div>
    </div>

    <div class="row d-flex justify-content-center">
        <div class="col-md-6">
            <h3>
                Usted está reservando la siguiente hora de teleconsulta:
            </h3>
            <table>

                {{--                <tr>--}}
                {{--                    <td>ID HORA</td>--}}
                {{--                    <td>{{  $parametros->ordencompra }}</td>--}}
                {{--                </tr>--}}
                <tr>
                    <td>Hora</td>
                    <td>{{ $detalleReserva->hora }}</td>
                </tr>

                <tr>
                    <td>Fecha</td>
                    <td>{{ $detalleReserva->fecha }}</td>
                </tr>
                <tr>
                    <td>Especialidad</td>
                    <td>{{ $detalleReserva->especialidad }}</td>
                </tr>
                {{--                <tr>--}}
                {{--                    <td>Prestación</td>--}}
                {{--                    <td>{{ $prestacion->descripcionespecialidad }}</td>--}}
                {{--                </tr>--}}
                <tr>
                    <td>Profesional</td>
                    <td>{{ $detalleReserva->nombrePrestador }}</td>
                </tr>
                <tr>
                    <td>Convenio</td>
                    <td>{{ $convenio->descripcion }}</td>
                </tr>
                {{--                <tr>--}}
                {{--                    <td>Valor</td>--}}
                {{--                    <td>${{ number_format($parametros->monto,0, ',', '.') }}</td>--}}
                {{--                </tr>--}}
            </table>
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="col-md-12 col-lg-8">
            <div class="btnContentReserva d-flex justify-content-center">
                <form action="{{$initResult->url}}" method="POST" role="form" class="form-inline">
                    {{ csrf_field() }}
                    <input type="hidden" name="token_ws" value="{{$initResult->token}}">
                    {{--                    <button type="submit">Pagar reserva</button>--}}
                    <button type="button" data-toggle="modal" data-target ="#ModalB"> Pago Bono</button>
                    <button type="button" id="btnConfirmar">Confirmar reserva</button>
                    <button type="button" id="btnCancelar">Cancelar</button>
                </form>
            </div>
        </div>
    </div>
    @include("modalBono")
@endsection


@section('jsScripts')
    <script>
        $(function () {
            $("nav ul li a").removeClass('active');
            $(".menuMis_consultas").addClass('active');

            $("#btnCancelar").click(function(){
                window.location.href = '/';
            });
            $("#btnConfirmar").click(function(){

                window.location.href = '/confirmarReserva/' + {!!json_encode($idhoraEncrypted) !!} +
                    '/' + {!! json_encode($convenio->idConvenio) !!} +
                    '/' + {!! json_encode($prestacion->idprestacion) !!};
                validarReservaConReglasAjax();

            });

            function validarReservaConReglasAjax(){
                $.ajax({
                    type: "GET",
                    async: false,
                    url: '/validarStillAvailableAjax/',
                    data: {
                        'idHora': '{!! $detalleReserva->idHora !!}'
                    },
                    success: function (data) {
                        console.log(data);
                        if(data[0] == 1){
                            /*GENERAR RESERVA*/
                            window.location.href = '/confirmarReserva/' + {!! json_encode($idhoraEncrypted) !!} +
                                '/' + {!! json_encode($convenio->idConvenio) !!} +
                                '/' + {!! json_encode($prestacion->idprestacion) !!};
                        } else if(data[0] == 0){
                            /*MOSTRAR MENSAJE INDICANDO QUE LA HORA YA FUE TOMADA*/
                            alertify.alert('Atención', 'Esta hora ya no se encuentra disponible. Puede continuar con su reserva' +
                                ' seleccionando otro horario.');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
        })
    </script>
@endsection
