@extends('layout')

@section('customCss')
    <link rel="stylesheet" href="{{ asset('css/paciente/pacienteWelcome.css') }}">
@endsection

@section('content')
    <h1 id="pageTitle">
        @php $saludo = $paciente->idsexo == 3 || is_null($paciente->idsexo) ? "Bienvenid@s" : ($paciente->idsexo == 1 ? "Bienvenida" : "Bienvenido") @endphp
        ¡{{ $saludo }} a Telecetep!
    </h1>

    <div class="row d-flex justify-content-center mb-1">
        <div class="col-md-10">
            <div class="saludo">
                <div class="logos">
                    <figure class="logo">
                        <img src="{{asset('grupo_cetep.png')}}" alt="Logo de GrupoCetep">
                    </figure>
                </div>
                <p>
                    {{$saludo}} a nuestra Plataforma en Línea de Teleconsultas de Salud Mental.
                </p>
                <p>
                    Para asegurar una gran experiencia con nuestra plataforma, favor revisar nuestros <a href="#"  data-toggle="modal" data-target="#modalReqTecnicos">requerimientos técnicos.</a> </br>
                    En caso de problemas de señal o pérdida de conexión, nuestros profesionales lo contactarán telefónicamente al fono <strong><em>{{$paciente->celular}}</em></strong>.
                    <a href="{{ route('mis_datos') }}">Clic aquí</a> si requiere actualizar sus datos.
                </p>
            </div>
        </div>
    </div>

    <div class="row d-flex justify-content-center">
        <div class="col-xs-12 col-md-10">
            <div class="mis_consultas_content">
                <hr>
                @include('agendaPaciente')
                @yield('agendaPaciente')
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalReqTecnicos" tabindex="-1" role="dialog" aria-labelledby="modalReqTecnicosLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalReqTecnicosLabel">Requerimientos Técnicos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Para una buena experiencia con nuestra Plataforma Telecetep, los requerimientos técnicos son:
                        <br><br>

                        <strong>1.- Equipo:</strong>
                    <ul>
                        <li><strong>PC de escritorio o notebook: </strong><br>
                            <span class="reqTec-subtitle">Equipo y procesador:</span> Intel/AMD 1,6 (GHz) con 2 núcleos
                            o superior.
                            <br>
                            <span class="reqTec-subtitle">Memoria:</span> 4 GB de RAM mínimo. Se recomienda 8 GB de RAM.
                            <br>
                            <span class="reqTec-subtitle">Pantalla:</span> Resolución de pantalla de 1280 x 768 o
                            superior.
                            <br>
                            <span class="reqTec-subtitle">Gráficos:</span> DirectX 9 o versiones posteriores, con WDDM
                            2.0 o posteriores para Windows 10 (o bien WDDM 1.3 o posteriores).
                            <br>
                            <span class="reqTec-subtitle">Sistema operativo:</span> Windows 10, Windows 8.1, Windows 7
                            SP1.
                            <br>
                            <span class="reqTec-subtitle">Navegador:</span> Microsoft Edge, Internet Explorer 11,
                            Safari, Chrome o Firefox. (En su versión actualizada)
                            <br>
                            <span class="reqTec-subtitle">Cámara Web:</span> Compatible sistema operativo. <br>
                            <span class="reqTec-subtitle">Parlantes y micrófono:</span> Compatible sistema operativo. Se
                            recomienda micrófono solapa o headset.
                            <br>
                        </li>
                        <li><strong>Smarphone y/o tablet:</strong><br>
                            <span class="reqTec-subtitle">Sistema operativo:</span> Android <br>
                            <span class="reqTec-subtitle">Cámara Web:</span> Compatible sistema operativo. <br>
                            <span class="reqTec-subtitle">Parlantes y micrófono:</span> Compatible sistema operativo. Se
                            recomienda micrófono solapa o headset.
                            <br>
                            <span class="reqTec-subtitle">Navegador:</span> Chrome o Firefox. (En su versión actualizada)<br>
                            <span class="reqTec-subtitle">Aplicación:</span> Debe descargar la aplicación Jitsi Meet. <br>
                        </li>
                    </ul>
                    <strong>2.- Conectividad:</strong> <br>
                    Enlace Internet de 2 Mbps o superior. <br> <br>

                    <strong>3.- Condiciones entorno:</strong> <br>
                    <span class="reqTec-subtitle">Iluminación:</span> Se recomienda suficiente iluminación natural o
                    artificial, de frente o desde arriba. Evitar luz posterior.
                    <br>
                    <span class="reqTec-subtitle">Sonido:</span> Evitar ruido ambiental. Buscar entorno tranquilo y en
                    silencio. Hablar fuerte frente a micrófono.
                    <br>

                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Entendido</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('jsScripts')
    @parent
    @include('js.jsWelcomePaciente')
@endsection