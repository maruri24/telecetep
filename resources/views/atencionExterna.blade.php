<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Telecetep | Grupo Cetep</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Roboto&display=swap" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Fontawesome CSS-->
    {{--    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">--}}
    <script src="https://kit.fontawesome.com/f2a4983c57.js" crossorigin="anonymous"></script>

    <!-- jQuery UI -->
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.structure.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.theme.min.css') }}">

    <!-- Alertify -->
    <!-- include the style -->
    <link rel="stylesheet" href="{{ asset('css/alertify.min.css')}}" />
    <!-- include a theme -->
    <link rel="stylesheet" href="{{ asset('css/themes/default.min.css')}}" />

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/atencionExterna.css') }}" rel="stylesheet">

    <!-- DataTable CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>

</head>
<body>

<header>
    <div class="d-flex justify-content-center align-items-center mobileHeader">
        <figure class="navLogo">
            <img src="{{ asset('grupo_cetep_no_name.png') }}" alt="Logo Cetep">
        </figure>
        <h1>Telecetep</h1>

        {{--<div class="headerContent">
            <h2>¡Hola, {{$paciente->nombres}} {{$paciente->apellidoPaterno}} {{$paciente->apellidoMaterno}}!</h2>
        </div>--}}
    </div>

</header>

<div class="content">
    @if($flag)
        <div class="teleconsultaContent embed-responsive embed-responsive-4by3">
            <iframe class="embed-responsive-item" src="https://jitsi.telecetep.cl/{{ $token_videollamada }}" allow="geolocation; microphone; camera; fullscreen"></iframe>
        </div>
    @elseif($hora->paciente)
        <div id="fuera_de_rango" class="d-flex align-items-center flex-column justify-content-center">
            <span class="mt-3 mb-3">{{$mensaje}}</span>
            <span>Hora: {{date('H:i a', strtotime($hora->hora))}}</span>
            <span>Fecha: {{date('d/m/Y', strtotime($hora->hora))}}</span>
        </div>
    @else
        <div id="fuera_de_rango" class="d-flex align-items-center flex-column justify-content-center">
            <span class="mt-3 mb-3">{{$mensaje}}</span>
        </div>
    @endif
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- DataTable JS -->
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.19/sorting/date-uk.js"></script>

<!-- jQuery UI JS -->
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<!-- Alertify JS -->
<script src="{{ asset(('js/alertify.min.js')) }}"></script>
{{-- Custom JS --}}
<script src="{{ asset('js/menu.js') }}"></script>
</body>
</html>