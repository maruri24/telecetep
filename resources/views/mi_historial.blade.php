@extends('layout')
@section('customCss')
    @parent
    <link rel="stylesheet" href="{{ asset('css/paciente/pacienteHistorial.css') }}">
@endsection

@section('content')
    <h1 id="pageTitle">Mi Historial</h1>
    <div class="row d-flex justify-content-center">
        <div class="col-md-12">
            <p class="text-justify">
                Mi histórico de reservas de horas y de atenciones de Salud Mental en Centro Médico Cetep.
            </p>
        </div>
    </div>
    <hr>
    <div class="row d-flex justify-content-center mt-5">
        <div class="dataContainer col-md-10">
            <h3>Historial de reservas</h3>
            <table id="tablaHistorialDelPaciente" name="tablaHistorialDelPaciente" class="display compact" style="width: 100%">
                <thead class="thead-light">
                <tr>
                    <td>Hora</td>
                    <td>Fecha</td>
                    <td>Profesional</td>
                    <td>Especialidad</td>
                    <td>Prestación</td>
                    <td>Asiste</td>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('jsScripts')
    @parent
    @include('js.jsHistorialDelPaciente')
@endsection