<script>
    $(document).ready(function(){
        $('.delbtn').on('click',function(){
            $('#ModalD').modal('show');

            $tr = $(this).closest('tr');

            var data = $tr.children('td').map(function(){
                return $(this).text();
            }).get();

            console.log(data);

            $('#idRegBonoHorasLi').val(data[11]);
            $('#idHoraLi').val(data[12]);

        });
    });
</script>

<script>
    $(document).ready(function(){
        $('.editbtn').on('click',function(){
            $('#ModalV').modal('show');

            $tr = $(this).closest('tr');

            var data = $tr.children('td').map(function(){
                return $(this).text();
            }).get();

            console.log(data);
            $('#idHora').val(data[12]);
            $('#numbono').val(data[10]);
            $('#idRegBonoHora').val(data[11]);

        });
    });
</script>


<script>
    $(document).ready(function(){
        $('.rebtn').on('click',function(){
            $('#rechazo').modal('show');

            $tr = $(this).closest('tr');

            var data = $tr.children('td').map(function(){
                return $(this).text();
            }).get();

            console.log(data);

            $('#idRegBonoHorasRe').val(data[11]);
            $('#idHoraRe').val(data[12]);

        });
    });
</script>

<script>
    $(function(){
        $("form#validaBono").submit(function(e){
            e.preventDefault();

            var form_data = new FormData($("#validaBono")[0]);

            $.ajax({
                url: "{{ route('confirmarBonos') }}",
                method: "POST",
                data: form_data,
                dataType: 'JSON',
                contentType: false,
                cache:false,
                processData: false,
                success: function (data) {
                    if(data.registered){
                        $("#ModalB").modal('hide');
                        alertify
                            .alert("Validacion bono",
                                "El bono ha sido validado con exito.",
                                function(){
                                    location.reload() ;
                                });
                    } else{
                        $("#errores").css('display', 'inherit');
                        $("#errores").html('');
                        $.each(data.errors, function(i, error){
                            $("#errores").append('<p>' + error + '</p>');
                        });
                        console.log(data.errors);
                        $("#conf").val(data.valor);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        })
    });
</script>


<script>
    $(function(){
        $("form#delBono").submit(function(e){
            e.preventDefault();

            var form_data = new FormData($("#delBono")[0]);

            $.ajax({
                url: "{{ route('liberarBono') }}",
                method: "POST",
                data: form_data,
                dataType: 'JSON',
                contentType: false,
                cache:false,
                processData: false,
                success: function (data) {
                    if(data.registered){
                        $("#delBono").modal('hide');
                        alertify
                            .alert("Liberar Bono",
                                "El bono se ha liberado con exito.",
                                function(){
                                    location.reload() ;
                                });

                    } else {

                        $("#error").css('display', 'inherit');
                        $("#error").html('');
                        $.each(data.errors, function (i, error) {
                        $("#error").append('<p>' + error + '</p>');
                        });
                        console.log(data.errors);
                    }
                },
                error: function () {alertify.alert(
                    'Liberar reserva de hora',
                    'No es posible liberar esta reserva.',
                    function(){  });
                    console.log(data.errors);
                }
            })
        })
    });
</script>

<script>
    $(function(){
        $("form#rechbono").submit(function(e){
            e.preventDefault();

            var form_data = new FormData($("#rechbono")[0]);

            $.ajax({
                url: "{{ route('rechazarBono') }}",
                method: "POST",
                data: form_data,
                dataType: 'JSON',
                contentType: false,
                cache:false,
                processData: false,
                success: function (data) {
                    if(data.registered){
                        $("#rechbono").modal('hide');
                        alertify
                            .alert("Rechazar Bono",
                                "El bono se ha sido rechazado con exito.",
                                function(){
                                    location.reload() ;
                                });

                    } else {

                        $("#error").css('display', 'inherit');
                        $("#error").html('');
                        $.each(data.errors, function (i, error) {
                            $("#error").append('<p>' + error + '</p>');
                        });
                        console.log(data.errors);
                    }
                },
                error: function () {alertify.alert(
                    'Rechazar hora',
                    'No es posible rechazar esta reserva.',
                    function(){  });
                    console.log(data.errors);
                }
            })
        })
    });
</script>


<script>
    $('#ModalV').on('hidden.bs.modal', function () {
        $("#errores").hide();
        $(this).find("#numBonoComprobar,#conf",).val('').end();

    });
</script>

<script>
    $('#ModalD').on('hidden.bs.modal', function () {
        $("#error").hide();
        $(this).find("textarea,div").val('').end();

    });
</script>

