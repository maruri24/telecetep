<script>
    $(function () {
        setLoadingGif();
        $(document)
            .ajaxStart(function () {
                $(".bluredBackground").show();
                $('.loadingGif').show();
            })
            .ajaxStop(function () {
                $(".bluredBackground").hide();
                $('.loadingGif').hide();
            });

        $('.location-info').popover({
            trigger: "hover",
            placement: "bottom",
            title: "Zona de atención",
            content: "Es imporante saber donde atienden fisicamente los profesionales para así facilitar el " +
                "retiro de documentos en caso de ser necesario. Seleccione una ubicación para filtrar " +
                "profesionales según area de atención."
        });
        $('.disabled-hour').popover({
            trigger: "hover",
            placement: "bottom",
            title: "Hora bloqueada",
            content: "Esta hora coincide con una hora ya reservada."
        });

        setSteps();
    });

    $(document).ready(function() {
        $("nav ul li a").removeClass('active');
        $(".menuMis_consultas").addClass('active');

        $("select#location").change(function(event){
            getPrestadoresPrestacionAjax($("select#convenio").val(), $("select#prestacion").val(), $("select#location").val());
        });
        $("select#convenio").change(function(event){
            getPrestacionesConvenioAjax($("select#convenio").val());
        });
        $("select#prestacion").change(function(event){
            getPrestadoresPrestacionAjax($("select#convenio").val(), $("select#prestacion").val(), $("select#location").val());
        });
        $("select#prestador").change(function(event) {
            getAgendaPrestadorAjax($("#convenio").val(), $("#prestacion").val(), $("#prestador").val(), $("#location").val());
        });
        $("#btnContinuar").click(function(){
            var horaSeleccionada = $("input[name='horaSeleccionada']:checked").val();
            var idConvenio = $("#convenio").val();
            var idPrestacion = $("#prestacion").val();
            var idPaciente = {!! json_encode($paciente->id) !!};

            validarReservaConReglasAjax(idConvenio, idPrestacion, idPaciente, horaSeleccionada);
        });
    });

    function setLoadingGif(){
        var windowHeight = $(window).height();
        var loadingGifHeight = $(".loadingGif").outerHeight();

        $(".loadingGif").css('top', (windowHeight - loadingGifHeight) / 2 + 'px');
        $(".loadingGif").hide();
        $(".bluredBackground").hide();
    }

    function setSteps(){
        $('.stepNumber').each(function(index, element){
             $(element).children('em').text(index + 1);
        });

        if($('select#convenio').length === 0){
            $(".stepNumber:eq(0)").parent().removeClass('col-md-4');
            $(".stepNumber:eq(0)").parent().addClass('col-md-6');
            $(".stepNumber:eq(1)").parent().removeClass('col-md-4');
            $(".stepNumber:eq(1)").parent().addClass('col-md-6');
        }
    }

    function getPrestacionesConvenioAjax(idConvenio = 0){
        $.ajax({
            type: "GET",
            async: true,
            url: '/getPrestacionesConvenioAjax/',
            data: {
                'idConvenio': idConvenio
            },
            success: function (data) {
                console.log(idConvenio, data);
                var prestaciones = data[0];
                $("select#prestacion").html('');

                $.each(prestaciones, function(key, prestacion){
                    $("select#prestacion").append('<option value="' + prestacion['idprestacion'] +'">'+prestacion['descripcionespecialidad']+'</option>');
                });

                getPrestadoresPrestacionAjax($("select#convenio").val(), $("select#prestacion").val(),$("select#location").val());
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }

    function getPrestadoresPrestacionAjax(idConvenio = 0, idPrestacion, location){
        $.ajax({
            type: "GET",
            async: true,
            url: '/getPrestadoresPrestacionAjax/',
            data: {
                'idConvenio': idConvenio,
                'idPrestacion': idPrestacion,
                'location': location
            },
            success: function (data) {
                console.log(idConvenio, data);
                var prestadores = data[0];
                $("select#prestador").html('');
                $("select#prestador").append('<option value="x">TODOS LOS PROFESIONALES</option>');

                $.each(prestadores, function(key, prestador){
                    $("select#prestador").append('<option value="' + prestador['id'] +'">'+prestador['nombres']+' '+prestador['apellidoPaterno']+' '+prestador['apellidoMaterno']+'</option>');
                });

                getAgendaPrestadorAjax($("#convenio").val(), $("#prestacion").val(), $("#prestador").val(), $("#location").val());
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }

    function getAgendaPrestadorAjax(idConvenio = 0, idPrestacion, idPrestador, location) {
        $.ajax({
            type: "GET",
            async: true,
            url: '/agendaPrestadorAjax/',
            data: {
                'idConvenio': idConvenio,
                'idPrestacion': idPrestacion,
                'idPrestador': idPrestador,
                'location': location
            },
            success: function (data) {
                var proxima_hora_disponible = data[2];
                var fechas_disponibles = data[1].map(function(elem){
                    return elem['fecha'];
                });
                var proximaFechaDisponible =  fechas_disponibles[0] !== undefined ? formatoFecha(proxima_hora_disponible['fecha'], proxima_hora_disponible['hora']) : 'No hay horas disponibles';

                $("#proxHoraDisponible span").text(proximaFechaDisponible);
                $("#proxHoraDisponible button").remove();

                if(fechas_disponibles[0] !== undefined){
                    $("#proxHoraDisponible span").after('' +
                        '<button type="button" class="btn btn-sm btn-primary btnReservarProxHora"' +
                        'onclick="seleccionarProxHoraDisponible(\''+proxima_hora_disponible['fecha']+'\', \''+proxima_hora_disponible['idHora']+'\')">Seleccionar</button>');
                }

                $("#alertaPrevia").css('display', 'inherit');
                $("#alterInfoHora").css('display', 'none');
                $(".horasContent").css('display', 'none');
                $(".btnContentAgendar").css('display', 'none');

                $( "#datepicker" ).datepicker( "option", "beforeShowDay", function(date){
                    fechas_disponibles = fechas_disponibles === undefined || fechas_disponibles === null ? [] : fechas_disponibles;

                    var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                    return [ fechas_disponibles.indexOf(string) != -1 ];
                });

                // Remove the style for the default selected day (today)
                $('.ui-state-active').removeClass('ui-state-active');
                $('.ui-state-hover').removeClass('ui-state-hover');

                // Reset the current selected day
                $('#date').val('');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }

    function getHorasPrestadorAjax(idConvenio = 0, idPrestacion, idPrestador, fecha, idPaciente, location){
        console.log(idConvenio, idPrestacion, idPrestador, fecha, idPaciente);
        $.ajax({
            type: "GET",
            async: true,
            url: '/horasPrestadorAjax/',
            data: {
                'idConvenio': idConvenio,
                'idPrestacion': idPrestacion,
                'idPrestador': idPrestador,
                'fecha': fecha,
                'idPaciente': idPaciente,
                'location': location
            },
            success: function (data) {
                console.log(data);
                var horas_disponibles = data[0];

                $("#alterInfoHora").css('display', 'inherit');
                $(".horasContent").css('display', 'inherit');
                $(".btnContentAgendar").css('display', 'inherit');
                $("#btnContinuar").prop('disabled', true);
                $("#alertaPrevia").css('display', 'none');
                $(".horasContent").html('');

                var windowHeight = $(window).outerHeight();
                var tablePosition = $(".horasContent").offset().top;
                var horasContentHeight = windowHeight - tablePosition - 50;
                if(horasContentHeight > 0){
                    $(".horasContent").css('height', horasContentHeight + 'px');
                }

                $.each(horas_disponibles, function(){
                    if(this['bloquear'] === false){
                        $(".horasContent").append("" +
                            "<div class='col-xs-12 col-md-12 hora' onclick='checkHora(this)'>" +
                            "<input type='radio' name='horaSeleccionada' value='" + this['idHora'] + "'>" +
                            "<span class='dataHora'>" + this['hora'] + "</span>" +
                            "<span class='dataPrestador'>" + this['nombrePrestador'] + "</span>" +
                            "<span class='dataEspecialidad'>" + $('select#prestacion option:selected').text() + "</span>" +
                            "</div>")
                    } else if(this['bloquear'] === true){
                        $(".horasContent").append("" +
                            "<div class='col-xs-12 col-md-12 hora disabled-hour' data-toggle='popover'>" +
                            "<input type='radio' name='horaSeleccionada' value='" + this['idHora'] + "' disabled>" +
                            "<span class='dataHora'>" + this['hora'] + "</span>" +
                            "<span class='dataPrestador'>" + this['nombrePrestador'] + "</span>" +
                            "<span class='dataEspecialidad'>" + $('select#prestacion option:selected').text() + "</span>" +
                            "</div>")
                    }
                });

                $('.disabled-hour').popover({
                    trigger: "hover",
                    placement: "bottom",
                    title: "Hora bloqueada",
                    content: "Esta hora coincide con una hora ya reservada."
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }

    function validarReservaConReglasAjax(idConvenio = 0, idPrestacion, idPaciente, horaSeleccionada){
        $.ajax({
            type: "GET",
            async: false,
            url: '/validarReservaConReglasAjax/',
            data: {
                'idConvenio': idConvenio,
                'idPrestacion': idPrestacion,
                'idPaciente': idPaciente,
                'idHora': horaSeleccionada
            },
            success: function (data) {
                console.log(data);
                if(data[0] == 1){
                    /*GENERAR RESERVA*/
                    window.location.href = '/detalleReserva/' + data[3] + '/' + data[4] + '/' + data[5];
                } else if(data[0] == 0){
                    var convenio = $("#convenio").val() === undefined ? 'Libre elección' : $("#convenio option:selected").text();

                    /*MOSTRAR MENSAJE INDICANDO QUE NO PUEDE GENERAR LA RESERVA*/
                    alertify.alert('Atención', 'No es posible continuar con la reserva. Usted ya tiene '+ data[1] +
                        ' agendamientos para el convenio ' + convenio + '. Este es el máximo de agendamientos' +
                        ' que puede realizar. Sí desea continuar, deberá liberar una hora de sus reservas relacionada a ' + convenio +
                        '.');
                } else if (data[0] == -1){
                    /*MOSTRAR MENSAJE INDICANDO QUE LA HORA YA FUE TOMADA*/
                    alertify.alert('Atención', 'Esta hora ya no se encuentra disponible. Puede continuar con su reserva' +
                        ' seleccionando otro horario.');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }

    $(function(){
        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '< Ant',
            nextText: 'Sig >',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);

        var fechas_disponibles = new Array();


        @foreach($fechas_disponibles as $fecha)
        fechas_disponibles.push( {!!  json_encode($fecha['fecha']) !!} );
        @endforeach

        console.log(fechas_disponibles);

        var proximaHoraDisponible =  fechas_disponibles[0] !== undefined ? formatoFecha({!! json_encode($proxima_hora_disponible['fecha']) !!}, {!! json_encode($proxima_hora_disponible['hora']) !!}) : 'No hay horas disponibles';
        $("#proxHoraDisponible span").text(proximaHoraDisponible);
        $("#proxHoraDisponible button").remove();

        if(typeof fechas_disponibles[0] !== undefined){
            $("#proxHoraDisponible span").after('' +
                '<button type="button" class="btn btn-sm btn-primary btnReservarProxHora"' +
                'onclick="seleccionarProxHoraDisponible(\''+{!! json_encode($proxima_hora_disponible["fecha"]) !!}+'\',\''+{!! json_encode($proxima_hora_disponible['idHora']) !!}+'\')">Seleccionar</button>');
        }

        var current_date = new Date();
        var minDate = new Date(current_date.getFullYear(),
            current_date.getMonth(),
            current_date.getDate());
        var maxDate = new Date( current_date.getFullYear()+1, 12, 31);

        $("#datepicker").datepicker({
            altField: '#date', // ID of the hidden field
            altFormat: "yyyy-mm-dd",
            numberOfMonths: 1,
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            minDate: minDate,
            maxDate: maxDate,
            regional: 'es',
            beforeShowDay: function(date){
                var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                return [ fechas_disponibles.indexOf(string) != -1 ];
            },
            onSelect: function(){
                var selectedDate = new Date($("#datepicker").datepicker('getDate'));
                var dateFormat = selectedDate.getFullYear() + '-' +
                    (selectedDate.getMonth()+1) + '-' +
                    selectedDate.getDate();

                getHorasPrestadorAjax($("#convenio").val(), $("#prestacion").val(), $("#prestador").val(), dateFormat, {!! json_encode($paciente->id)!!}, $("#location").val());
            }
        });

        // Remove the style for the default selected day (today)
        $('.ui-state-active').removeClass('ui-state-active');
        $('.ui-state-hover').removeClass('ui-state-hover');

        // Reset the current selected day
        $('#date').val('');

    });

    function formatoFecha(fecha, hora){
        var splittedDate = fecha.split('-');
        var formattedDate = splittedDate[2] + '-' + splittedDate[1] + '-' + splittedDate[0];

        return formattedDate + ' a las ' + hora;
    }

    function seleccionarProxHoraDisponible(fecha, idHora){
        var splittedDate = fecha.split('-');

        $("#datepicker").datepicker("setDate", new Date(parseInt(splittedDate[0]), parseInt(splittedDate[1]) -1, parseInt(splittedDate[2])));
        $.each($("td[data-month="+ (parseInt(splittedDate[1]) - 1) +"][data-year='" + parseInt(splittedDate[0]) + "']"), function(){
            if(this.innerText == parseInt(splittedDate[2])){
                $(this).click();
            }
        });

        $(document)
            .ajaxStop(function () {
                $.each($("input[type='radio']"), function(){
                    if($(this).val() == idHora){
                        $(this).parent().click();
                    }
                });
                enableButton();
            });
    }

    function enableButton() {
        if($("input[name='horaSeleccionada']:checked").val()){
            $("#btnContinuar").prop('disabled', false);
        }
    }

    function checkHora(t) {
        $('.selectedHora').removeClass('selectedHora');
        $(t).toggleClass('selectedHora');
        $(t).children("input").prop("checked", true);
        if($("input[name='horaSeleccionada']:checked").val()){
            $("#btnContinuar").prop('disabled', false);
        }
    }
</script>
