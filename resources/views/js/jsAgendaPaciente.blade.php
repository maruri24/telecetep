<script>

    $(function () {
       $("#btnAgendar").click(function(){
           window.location.href = "{{ route('reservaHora') }}";
       })
    });

    function administrarHora(idHora){
        getInfoPagarHora(idHora);
    }

    function modalAjuntarBono(idHora){
        $("#modalAdministrarHora").modal('hide');
        $("#modalBono").modal('show');

        getInfoPagarBono(idHora);
    }

    function ifAllowToLiberar(idHora){
        $.ajax({
            type: "GET",
            async: false,
            url: '/ifAllowToLiberarAjax/',
            data: {
                'idHora': idHora
            },
            success: function (data)  {
                if(data[0]){
                    alertify.confirm(
                        'Liberar reserva de hora',
                        'Para liberar la hora reservada debes hacer click en el botón "Continuar". Si no deseas continuar, haz clic en el botón "Cancelar"',
                        function () {
                            liberarHora(idHora);
                        },
                        function (){

                        }
                    ).set('labels', {ok:'Continuar', cancel:'Cancelar'});
                } else {
                    alertify.alert(
                        'Liberar reserva de hora',
                        'No es posible liberar esta reserva. Para liberar una hora debe hacerlo '+data[1]+'hrs antes de su comienzo.',
                        function(){  });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }

    function liberarHora(idHora){
        $.ajax({
            type: "GET",
            async: false,
            url: '/liberarHoraAjax/',
            data: {
                'idHora': idHora
            },
            success: function (data) {
                console.log(data);
                if(data[0]){
                    location.reload();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }

    function getInfoPagarHora(idHora){
        $.ajax({
            type: 'GET',
            async: true,
            url: "/getInfoPagarHora/",
            data: { 'idHora': idHora },
            success: function(data){
                console.log(data);

                $("form#formPagoWebpay").prop('action', data['initResult_url']);
                $("input#token_ws").val(data['initResult_token']);
                if(data['permitePagoWebpay'] === 0){
                    alert("asdfasdfasdf");
                    $("#btnPagarWebPay").css('display', 'none');
                } else {
                    $("#btnPagarWebPay").css('display', 'initial');
                }
                // if(data['permiterPagoBono']){
                //     $("#btnAdjuntarBono").css('display', 'none');
                // } else {
                //     $("#btnAdjuntarBono").css('display', 'initial');
                // }

                $("#modalAdministrarHora").modal('show');
                $("#modalAdministrarHora #btnAdjuntarBono").attr('onclick', 'modalAjuntarBono('+idHora+')');
                $("#modalAdministrarHora #btnLiberar").attr('onclick', 'ifAllowToLiberar('+idHora+')');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }

    function getInfoPagarBono(idHora){
        $.ajax({
            type: 'GET',
            async: false,
            url: "/getInfoHoraBono/",
            data: { 'idHora': idHora },
            success: function(data){
                console.log(data);
                $("#modalBono input[name='idHora']").val(data.idHora);
                $("#modalBono input[name='monto']").val(data.monto);
                $("#modalBono input[name='convenio']").val(data.idConvenio);
                $("#modalBono input[name='prestacion']").val(data.idPrestacion);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }

    $(function(){
        $("form#ingBono").submit(function(e){
            e.preventDefault();
            var form_data = new FormData($("#ingBono")[0]);

            $.ajax({
                url: "{{ route('ingresoBono') }}",
                method: "POST",
                data: form_data,
                dataType: 'JSON',
                contentType: false,
                cache:false,
                processData: false,
                success: function (data) {
                    if(data.registered){
                        $("#modalBono").modal('hide');
                        alertify
                            .alert("Bono registrado",
                                "El bono ha sido registrado con exito y está pendiente de aprobación.",
                                function(){
                                    alertify.message('OK');
                                });
                    } else{
                        $("#errores").css('display', 'inherit');
                        $("#errores").html('');
                        $.each(data.errors, function(i, error){
                            $("#errores").append('<p>' + error + '</p>');
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        })
    });

    function comenzarTeleconsulta(idPaciente, idHora){
        window.location =  '/teleconsulta/'+ idHora;
    }
</script>