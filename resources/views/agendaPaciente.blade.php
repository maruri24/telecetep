@section('customCss')
    @parent
    <link rel="stylesheet" href="{{asset('css/paciente/pacienteAgenda.css')}}">
@append

@section('agendaPaciente')
    <h1 id="pageTitle" class="pageTitleLeft">Mis Consultas</h1>
    <p id="bajada">
        Permite reservar horas de Teleconsulta con nuestros profesionales, además de listar sus consultas ya reservadas. Nuestro sistema permite mantener hasta dos reservas agendadas, además de liberar una reserva hasta con 24 horas de anticipación.
    </p>
    <div class="row d-flex justify-content-center">
        <div class="col-md-8">
            <button id="btnAgendar" class="btn btn-primary btn-lg btn-block">
                Reservar una nueva hora
            </button>
{{--            <a href="{{ route('reservaHora') }}" role="button" id="btnAgendar" type="button" class="btn btn-primary btn-lg btn-block">--}}
{{--                Reservar una nueva hora haciendo clic aquí--}}
{{--            </a>--}}
        </div>
    </div>
    <hr>

    <div class="row d-flex justify-content-center mt-5">
        <div class="col-md-12">
            <h3>Horas reservadas</h3>
            @if ($horasReservadas->isNotEmpty())
                <div class="table-container">
                    <table id="tableHorasReservadas" class="table table-striped">
                        <thead class="">
                        <tr>
                            <th scope="col">Hora</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Especialidad</th>
                            <th scope="col">Profesional</th>
                            <th scope="col">Convenio</th>
                            <th scope="col" style="text-align: center">Acción</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($horasReservadas as $reserva)
                            <tr>
                                <td>{{ $reserva->hora }}</td>
                                <td>{{ $reserva->fecha}}</td>
                                <td>{{ $reserva->especialidad }}</td>
                                <td>{{ $reserva->prestador }}</td>
                                <td>{{ $reserva->convenio }}</td>
                                <td style="text-align: center">
                                    @if(strtotime($dateTimeActual) >= strtotime($reserva->horaComienzo) && strtotime($dateTimeActual) <= strtotime($reserva->horaExpiracion))
                                        <button class="btn btn-sm btn-success btnComenzar" onclick="comenzarTeleconsulta({{$paciente->id}}, {{ $reserva->idHora }})"><i class="fa fa-video-camera" aria-hidden="true"></i> Comenzar</button>
                                    @elseif($reserva->estado_pago == 9 || $reserva->idconvenio != 0)
                                        <button type="button" class="btn btn-sm btn-danger btnEliminar" onclick="ifAllowToLiberar({{$reserva->idHora}})"><i class="fa fa-minus-circle" aria-hidden="true"></i> Liberar</button>
                                    @else
                                        <button type="button" class="btn btn-sm btn-secondary" onclick="administrarHora({{$reserva->idHora}})"><i class="fas fa-hand-holding-usd"></i> Pagar</button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            @else
                <div class="alert alert-info" role="alert">
                    No tienes horas de teleconsulta reservadas.
                </div>
            @endif
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modalAdministrarHora" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="modalAdministrarHoraLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalAdministrarHoraLabel">Pagar reserva</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Usted tiene pendiente de pago al menos una reserva. Para proceder con el pago de la reserva, favor elegir uno de los siguientes métodos, o en su defecto, liberar la hora.</p>
                    <form action="" method="POST" role="form" class="form-inline" id="formPagoWebpay">
                        <div class="buttons" style="text-align: center; margin:auto;">
                                {{ csrf_field() }}
                                <input type="hidden" name="token_ws" id="token_ws" value="">
{{--                                <button type="button" id="btnAdjuntarBono" class="btn btn-outline-secondary" onclick="modalAjuntarBono()">Adjuntar Bono</button>--}}
                                <button type="submit" id="btnPagarWebPay" class="btn btn-outline-success">Pagar con WebPay</button>
                                <button type="button" id="btnLiberar" class="btn btn-outline-danger" onclick="ifAllowToLiberar()">Liberar</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalBono" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="modalBonoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalBonoLabel">Pago con bono</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Estimad@ {{$paciente->nombres}} {{$paciente->apellidoPaterno}} {{$paciente->apellidoMaterno}},
                    en esta sección podrá pagar con bono, indicando el número de bono y adjuntando una imagen completa de este.
                    El bono será validado con posterioridad por un operario de TELECETEP.</p>

                    <form name="ingBono" id="ingBono" action="{{'/ingresoBono'}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="idBono">Número Bono</label>
                            <input id="idBono" name="idBono" type="text" class="form-control" pattern="[0-9]*" placeholder="Ingrese numero de bono" value="" required >
                        </div>

                        <div class="form-group">
                            <label for="fechab">Fecha emisión Bono</label>
                            <input type="date" name="fechab" id="fechab" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="bonoFile"> Adjuntar Bono</label>
                            <em>(Solo formatos: pdf, png, jpeg, jpg)</em>
                            <input type="file" class="form-control" name="bonoFile" id="bonoFile" required>
                        </div>

                        <input type="text" id="idHora" name="idHora" hidden="hidden" value="" required>
                        <input type="text" id="monto" name="monto" hidden="hidden" value="27760" required>
                        <input type="text" id="convenio" name="convenio" hidden="hidden" value="">
                        <input type="text" id="prestacion" name="prestacion" hidden="hidden" value="">

                        <div id="errores" class="alert alert-danger" role="alert" style="display:none;">
                        </div>
                        
                        <input type="submit" class="btn btn-primary" value="Guardar">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('jsScripts')
    @parent
    @include('js.jsAgendaPaciente')
@append