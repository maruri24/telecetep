@extends('auth.layouts.master')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-8 offset-lg-2">

            <form method="POST"  action="{{ route('register_paciente') }}" aria-label="{{ __('Register') }}" class="tab-form">
                <div class="row">
                    <div class="col-12">
                        <div class="d-flex justify-content-center">
                        @csrf
                        <img class="mb-4" src="{{ asset('grupo_cetep.png') }}" alt="" width="120" height="120">
                        </div>
                        <div class="d-flex justify-content-center">
                        <h1 class="h3 mb-3 font-weight-normal">TELECETEP</h1>

                        </div>

                    </div>
                </div>
                <?php $errors ?>
                @if ($errors->any())
                    @if($errors->has('info'))
                        <div class="alert alert-info">
                    @else
                        <div class="alert alert-danger">
                    @endif
                        <h5>¡Atención!</h5>
                        <ul style="list-style: none; margin: 0; padding: 0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
               

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label" for="rut">Rut</label>
                            <input id="rut" type="rut" class="form-control{{ $errors->has('rut') ? ' is-invalid' : '' }}" regexp="[0-9]{0,10}(\-([0-9kK]{0,1})){0,1}" name="rut" value="{{ old('rut', $rut) }}" placeholder="1234567-8" required autofocus readonly data-toggle="popover" title="" data-content="Debe ingresar el RUT sin puntos, con guión y con dígito verificador">
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="nombres">Nombre</label>
                            <input id="nombres" type="text" class="form-control{{ $errors->has('nombres') ? ' is-invalid' : '' }}" name="nombres" value="{{ old('nombres', $paciente->nombres) }}" placeholder="Nombre" required autofocus readonly>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="apellidoPaterno">Apellido paterno</label>
                            <input id="apellidoPaterno" type="text" class="form-control{{ $errors->has('apellidoPaterno') ? ' is-invalid' : '' }}" name="apellidoPaterno" value="{{ old('apellidoPaterno', $paciente->apellidoPaterno) }}" placeholder="Apellido paterno" required autofocus readonly>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="apellidoMaterno">Apellido materno</label>
                            <input id="apellidoMaterno" type="text" class="form-control{{ $errors->has('apellidoMaterno') ? ' is-invalid' : '' }}" name="apellidoMaterno" value="{{ old('apellidoMaterno', $paciente->apellidoMaterno) }}" placeholder="Apellido materno" required autofocus readonly>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label" for="celular">Celular</label>
                            <input id="celular" type="text" class="form-control{{ $errors->has('celular') ? ' is-invalid' : '' }}" name="celular" regexp="[0-9]{0,9}" minlength="9" maxlength="9" value="{{ old('celular') }}" placeholder="Celular" required autofocus>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="email">Email</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="password">Contraseña</label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Password') }}" required data-toggle="popover" title="" data-content="La contraseña debe contener al menos 6 caracteres">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="password-confirm">Confirmar contraseña</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="{{ __('Confirm Password') }}" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label" for="isapre">Seguro de salud</label>
                            <select name="isapre" id="isapre"  class="form-control{{ $errors->has('isapre') ? ' is-invalid' : '' }}" required>
                                <option {{ (old("isapre") ? 'asdasd' : 'selected' ) }}>SELECCIONE SU SEGURO DE SALUD</option>
                                @foreach($isapres as $isapre)
                                    <option value="{{ $isapre->id }}" {{ (old("isapre") === $isapre->id  ? "selected":"") }}>{{ strtoupper($isapre->isapre) }}</option>
                                @endforeach
                            </select>                    
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label" for="contacto_emergencia">Contacto de emergencia</label>
                            <input type="text" name="contacto_emergencia" class="form-control{{ $errors->has('contacto_emergencia') ? ' is-invalid' : '' }}" placeholder="( Nombre y número de contacto )" value="{{ old('contacto_emergencia') }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group text-center">
                            <label class="text-center">
                              <input class="" type="checkbox" id="chk_tyc" name="acept" required> He leído y acepto los <a href="#" id="tyc">términos y condiciones</a> de uso
                            </label>                            
                            <button id="registrar" type="submit" class="btn btn-primary btn-block" disabled>
                                {{ __('Register') }}
                            </button>
                            <em style="display: block; font-size: .8em;padding-top: 8px">(Todos los campos son requeridos)</em>
                        </div>
                    </div>
                </div>
                <p class="mt-2 text-muted text-center">&copy; Grupo Cetep {{date('Y')}}</p>
            </form>
        </div>
    </div>
</div>
@endsection

@section('jsScripts')
<script type="text/javascript">
    $('#chk_tyc').click(function(){
        if( $(this).prop('checked') ) {
            $('#registrar').prop('disabled', false);
        }
        else
        {
            $('#registrar').prop('disabled', true   );
        }
    });
        $(function () {
            $('[data-toggle="popover"]').popover({trigger: 'focus', placement: 'top'}); 
        });
</script>

@endsection