    @extends('auth.layouts.master')

    @section('content')
    <div class="col-lg-offset-6 col-lg-3">

    <p class="mt-2 text-muted">&copy; 2019 - {{date('Y')}}</p>
    <a class="btn btn-link" href="{{ route('/') }}">
        Inicio
    </a>
    </div>


    @endsection
