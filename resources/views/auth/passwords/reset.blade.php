    @extends('auth.layouts.master')

    @section('content')
    <div class="container">        

        <div class="row-fluid">
            <div class="row">
                
                    <div class="col-lg-4 offset-lg-4">
                        
                        <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}" class="tab-form">
                            @csrf
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-center">
                @csrf
                <img class="mb-4" src="{{ asset('grupo_cetep.png') }}" alt="" width="120" height="120">
                </div>
                <div class="d-flex justify-content-center">
                <h1 class="h3 mb-3 font-weight-normal">TELECETEP</h1>

                </div>

            </div>
        </div>
        @if ($errors->any())
        <div class="form-group">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-center">
                        <div class="alert alert-danger">
                            <h5>¡Atención!</h5>
                            <ul style="list-style: none; margin: 0; padding: 0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                         
                    </div>      

                </div>
            </div>
        </div>
        @endif 
                            <input type="hidden" name="token" value="{{ $token }}">


                            <div class="control-group">
                                <label for="email" class="sr-only">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" placeholder="E-Mail" required autofocus>

        
                            </div>
                            <div class="control-group">     
                                <label for="password" class="sr-only">{{ __('Password') }}</label>


                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contraseña" required>

                            </div>
                            <div class="control-group">
                                <label for="password-confirm" class="sr-only">{{ __('Confirm Password') }}</label>


                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar contraseña" required>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-block btn-primary">
                                {{ __('Reset Password') }}
                            </button>
                            <br>
                        <p class="mt-2 text-muted text-center">&copy; Grupo Cetep {{date('Y')}}</p>                            
                        </form>

                    </div>
               
            </div>
        </div>

    </div>
 


    @endsection
