@extends('auth.layouts.master')

@section('content')
<div class="">
    
        <div class="row ">
        <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}" class="tab-form">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-center">
                    @csrf
                    <img class="mb-4" src="{{ asset('grupo_cetep.png') }}" alt="" width="120" height="120">
                    </div>
                    <div class="d-flex justify-content-center">
                    <h1 class="h3 mb-3 font-weight-normal">TELECETEP</h1>

                    </div>

                </div>
            </div>

            @if ($errors->any())
            <div class="form-group">
                <div class="row">
                    <div class="col-12">
                        <div class="d-flex justify-content-center">
                            <div class="alert alert-danger">
                                <h5>¡Atención!</h5>
                                <ul style="list-style: none; margin: 0; padding: 0">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach

                                </ul>
                            </div>
                             
                        </div>      

                    </div>
                </div>
            </div>
            @endif 
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif   
            <label for="email" class="sr-only">{{ __('E-Mail Address') }}</label>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-Mail" required>
            <br>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar link de recuperación al E-Mail</button>   

            {{ csrf_field() }}
            <p class="mt-2 text-muted text-center">&copy; 2019 - {{date('Y')}}</p>

        </form>
    </div>
@endsection

