@extends('auth.layouts.master')

@section('title', 'Iniciar sesión')


@section('content')
    @isset($url)
    <form class="form-signin tab-form" autocomplete="off" method="POST" action='{{ url("login/$url") }}' aria-label="{{ __('Login') }}">
    @else
    <form class="form-signin tab-form" autocomplete="off" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
    @endisset
                <div class="row">
                    <div class="col-12">
                        <div class="d-flex justify-content-center">
                        @csrf
                        <img class="mb-4" src="{{ asset('grupo_cetep.png') }}" alt="" width="120" height="120">
                        </div>
                        <div class="d-flex justify-content-center">
                        <h1 class="h3 mb-3 font-weight-normal">TELECETEP</h1>

                        </div>

                    </div>
                </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <h5>¡Atención!</h5>
                <ul style="list-style: none; margin: 0; padding: 0">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <label for="username" class="sr-only">Usuario</label>
        <input type="number" id="username" name="username" class="form-control" placeholder="Usuario" required  autocomplete="off" regexp="[0-9]{0,8}" minlength="7" maxlength="8" value="{{ old('username') }}"  data-toggle="popover" title="" data-content="Debe ingresar el RUT sin puntos, sin guión y sin dígito verificador">

        <label for="password" class="sr-only">Contraseña</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Contraseña" required>

        {{ csrf_field() }}

        <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar sesión</button>
        <a class="btn btn-link btn-block btn-small text-center" href="{{ route('register') }}">
            Crear una cuenta
        </a>
        <a class="btn btn-link btn-block btn-small text-center" href="{{ route('password.request') }}">
            {{ __('Forgot Your Password?') }}
        </a>
        <p class="mt-2 text-muted text-center">&copy; Grupo Cetep {{date('Y')}}</p>

        @if(session()->has('success'))
            <div class="alert alert-success">
                <ul style="list-style: none; margin: 0; padding: 0">
                            {{ session()->get('success') }}
                </ul>            
            </div>
        @endif
        @if(session()->has('register'))
            <div class="alert alert-danger">
                <ul style="list-style: none; margin: 0; padding: 0">
                            No se encuentra en el sistema. Si desea registrarse, haga click <a href="{{ route('register') }}">Aquí</a>.'                
                </ul>            
            </div>
        @endif
        @if(session()->has('danger'))
            <div class="alert alert-danger">
                <ul style="list-style: none; margin: 0; padding: 0">
                            {{ session()->get('danger') }}               
                </ul>            
            </div>
        @endif        
    </form>

@endsection

@section('jsScripts')
    <script>
        $(function () {
            $('[data-toggle="popover"]').popover({trigger: 'focus', placement: 'top'}); 
        });
      $(document).ready(function () {
        $('#username').bind('copy paste', function (e) {
           e.preventDefault();
        });
  });        
    </script>

@endsection


@section('cssCustom')
    <style type="text/css">
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
          -webkit-appearance: none;
          margin: 0;
        }

        /* Firefox */
        input[type=number] {
          -moz-appearance: textfield;
        }        
    </style>
@endsection
