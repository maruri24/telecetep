
<div class="modal fade" id="ModalV">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalReqPBLabel">Validar Bono</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>×</span>
                </button>
            </div>
            <form id="validaBono" name="validaBono" id="validaBono" method="POST" action="{{'/ConfirmarBono'}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <h4>Ingrese número de bono</h4>
                        <input type="text" name="numBonoComprobar" id="numBonoComprobar" class="form-control numBonoComprobar" placeholder="Numero de bono de la imagen">
                        <input type="text" name="numBono" id="numBono"class="form-control" value="" hidden="hidden" >
                    </div>

                        <input type="text" name="idRegBonoHora" id="idRegBonoHora"class="form-control" value="" hidden="hidden" >
                        <input type="text" name="conf" id="conf" value="" hidden="hidden">
                        <input type="text" name="idHora" id="idHora" value="" hidden="hidden">

                        <div id="errores" class="alert alert-danger" role="alert" style="display:none;">
                        </div>
                    <button type="submit" class="btn btn-primary" value="Guardar">Guardar </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>

            </form>
        </div>
    </div>
</div>