@extends('layout')
@section('customCss')
    @parent
    <link rel="stylesheet" href="{{ asset('css/paciente/pacienteHistorial.css') }}">

@endsection


@section('content')

    <h1 id="pageTitle">Historial</h1>
    <div class="row d-flex justify-content-center">
        <div class="col-md-12">
            <p class="text-justify">
                Listado de horas pendientes de pago.
            </p>
            @if(isset($status))
                <p class="alert alert-{{ $status_type }}" >{{ $status }}</p>
            @endif
        </div>
    </div>
    <hr>
    <h3>Horas reservadas pendientes de pago</h3>
    <div class="row d-flex justify-content-center mt-5">
        <div class="col-md-push-12">

            @if ($horasReservadas->isNotEmpty())
                <div class="table-container">
                    <table id="tableHorasReservadas" class="table table-striped">
                        <thead class="">
                        <tr>
                            <th scope="col">Hora</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Especialidad</th>
                            <th scope="col">Profesional</th>
                            <th scope="col">Convenio</th>
                            <th scope="col">Rut</th>
                            <th scope="col" >Paciente</th>
                            <th scope="col">Teléfono</th>
                            <th scope="col">Monto</th>
                            <th scope="col">Validar</th>
                            <th scope="col">Liberar</th>
                            <th scope="col">Editar</th>

                        </tr>
                        </thead>
                        <tbody>

                        @foreach($horasReservadas as $reserva)
                            <tr>
                                <td>{{ $reserva->hora }}</td>
                                <td>{{ $reserva->fecha}}</td>
                                <td>{{ $reserva->especialidad }}</td>
                                <td>{{ $reserva->prestador }}</td>
                                <td>{{ $reserva->convenio }}</td>
                                <td>{{$reserva->rut}}</td>
                                <td>{{$reserva->nombre}}</td>
                                <td>{{$reserva->celular}}</td>
                                <td>{{number_format($reserva->monto,0, ',', '.') }}</td>
                                <td style="display: none;">{{$reserva->idHora}}</td>

                                <td>
                                    <button type="button" class="btn btn-success editbtn" id="editbtn"><i class="fa fa-check" aria-hidden="true"></i></button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger delbtn" id="delbtn"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary delbtn" id="delbtn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <!--INCLUIDOS AQUI PORQUE SON LLAMADOS SOLO SI HAY HORAS PENDIENTES,
                DE LO CONTRARIO GENERAN ERROR AL NO ENCONTRAR LAS VARIABLES -->
                @include('ModalBonoConf')
            @section('jsScripts')

                @include('js.jsBonos')
            @append

            @else
                <div class="alert alert-info" role="alert">
                    No tienes horas de teleconsulta pagadas con bono por revisar.
                </div>
            @endif

        </div>
    </div>

    <!-- MODAL LIBERAR HORA, SE ENCUENTRA AQUI PORQUE DENTRO DEL ARCHIVO GENERA ERROR-->
    <div class="modal fade" id="ModalD">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalReqPBLabel">Liberar hora</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>×</span>
                    </button>
                </div>
                <form id="delBono" name="delBono"  method="POST" action="{{'/liberarBono'}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">

                        <div class="col-md-9">
                            <h4>Ingrese razón de liberación </h4>
                            <input type="text" name="motivo" id="motivo" class="form-control" >
                        </div>

                        <div class="col-md-9">
                            <input type="text" name="idHora" id="idHora"class="form-control" value="" hidden="hidden">
                            <input type="text" name="idRegBonoHoras" id="idRegBonoHoras"class="form-control" value="" hidden="hidden">
                        </div>
                        <div class="col-md-10">
                            <div id="error" class="alert alert-danger" role="alert" style="display:none;">

                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger" value="Liberar">Liberar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


@endsection


