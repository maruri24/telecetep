@extends('prestador.layout')

@section('customCss')
    <link rel="stylesheet" href="{{ asset('css/prestador/perfilPaciente.css') }}">
@endsection


@section('content')
    <div class="informacionPaciente">
        <hr>
        <div class="contenido">
            <h3>Información del paciente</h3>
            <div class="infoPrincipal col-md-12 col-lg-12 col-xl-10 offset-xl-1">
                <div class="row">
                    <div class="dato col-md-4">
                        <span>RUT: </span><em>{{ formatoRut($paciente->rut)  }}</em>
                    </div>
                    <div class="dato col-md-4">
                        <span>Nombre: </span><em>{{$paciente->nombres}} {{$paciente->apellidoPaterno}} {{$paciente->apellidoMaterno}}</em>
                    </div>
                    <div class="dato col-md-4">
                        @if($paciente->fechaNacimiento === '0000-00-00' || $paciente->fechaNacimiento == null)
                            <span>Edad: </span><em>No especifica</em>
                        @else
                            <span>Edad: </span><em>{{calcularEdad($paciente->fechaNacimiento)->y}} años {{calcularEdad($paciente->fechaNacimiento)->m}} meses {{calcularEdad($paciente->fechaNacimiento)->d}} días</em>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="dato col-md-4">
                        <span>Género: </span> <em>{{getGenero($paciente->idsexo)}}</em>
                    </div>
                    <div class="dato col-md-4">
                        <span>Isapre: </span> <em>{{getIsapre($paciente->isapre)}}</em>
                    </div>
                    <div class="dato col-md-4">
                        <span>Fecha nacimiento: </span> <em>{{ getFechaNacimiento($paciente->fechaNacimiento) }}</em>
                    </div>
                </div>
            </div>
            <div class="infoAdicional col-md-12 col-lg-12 col-xl-10 offset-xl-1">
                <div class="row">
                    <div class="dato col-md-4">
                        <span>Correo electrónico: </span><em>{{ replaceIfNull($paciente->email) }}</em>
                    </div>
                    <div class="dato col-md-4">
                        <span>Teléfono: </span><em>{{ replaceIfNull($paciente->telefono) }}</em>
                    </div>
                    <div class="dato col-md-4">
                        <span>Celular: </span><em>{{ replaceIfNull($paciente->celular) }}</em>
                    </div>
                </div>
                <div class="row">
                    <div class="dato col-md-4">
                        <span>Dirección: </span><em>{{ replaceIfNull($paciente->direccion) }}</em>
                    </div>
                    <div class="dato col-md-4">
                        <span>Comuna: </span><em>{{ getComuna($paciente->comuna) }}</em>
                    </div>
                    <div class="dato col-md-4">
                        <span>Ciudad: </span><em> {{ replaceIfNull($paciente->ciudad) }}</em>
                    </div>
                </div>
                <div class="row">
                    <div class="dato col-md-4">
                        <span>Isapre: </span><em>{{ getIsapre($paciente->isapre) }}</em>
                    </div>
                    <div class="dato col-md-4">
                        <span>Estado civil: </span><em>{{ getEstadoCivil($paciente->estadoCivil) }}</em>
                    </div>
                    <div class="dato col-md-4">
                        <span>Nacionalidad: </span><em> No especifica</em>
                    </div>
                </div>
                <div class="row">
                    <div class="dato col-md-4">
                        <span>Contacto de emergencia: </span><em>{{ replaceIfNull($paciente->contactoEmergencia_1) }}</em>
                    </div>
                </div>
                <div style="width: 100%; text-align: center">
                    <a href="javascript:void(0)" id="updateInfoHeader">Actualizar Información</a>
                </div>
            </div>
            <div class="btnContent">
                <span class="expandirInfoPaciente" onclick="expandirInfo()"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
            </div>
        </div>
    </div>
    <div class="contenedor">
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Agenda del paciente <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </button>
                    </h2>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p>
                            Permite reservar horas futuras de Teleconsulta con nuestros profesionales, además de listar sus consultas ya reservadas. Nuestro sistema permite mantener hasta dos reservas agendadas, además de liberar una reserva hasta con 24 horas de anticipación.
                        </p>
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-8">
                                @if($limiteDeReservas == $countReservas)
                                    <a href="{{ route('agenda', $paciente) }}" id="btnAgendar" type="button" class="btn btn-primary btn-lg btn-block disabled">
                                        Reserva una nueva hora haciendo click aquí
                                    </a>
                                @else
                                    <a href="{{ route('agenda', $paciente) }}" id="btnAgendar" type="button" class="btn btn-primary btn-lg btn-block">
                                        Reserva una nueva hora haciendo click aquí
                                    </a>
                                @endif
                            </div>
                        </div>
                        <hr>

                        <div class="row d-flex justify-content-center mt-5">
                            <div class="col-md-8">
                                <h3>Horas reservadas</h3>
                                @if ($horasReservadas->isNotEmpty())
                                    <div class="table-container">
                                        <table id="tableHorasReservadas" class="table table-striped">
                                            <thead class="">
                                            <tr>
                                                <th scope="col">Hora</th>
                                                <th scope="col">Fecha</th>
                                                <th scope="col">Especialidad</th>
                                                <th scope="col">Profesional</th>
                                                <th scope="col" style="text-align: center">Acción</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($horasReservadas as $reserva)

                                                <tr>
                                                    <td>{{ $reserva->hora }}</td>
                                                    <td>{{ $reserva->fecha}}</td>
                                                    <td>{{ $reserva->especialidad }}</td>
                                                    <td>{{ $reserva->prestador }}</td>
                                                    <td style="text-align: center">

                                                        @if(strtotime($dateTimeActual) >= strtotime($reserva->horaComienzo) && strtotime($dateTimeActual) <= strtotime($reserva->horaExpiracion))
                                                            <button class="btn btn-sm btn-success btnComenzar" onclick="comenzarTeleconsulta({{$paciente->id}}, {{ $reserva->idHora }})"><i class="fa fa-video-camera" aria-hidden="true"></i> Comenzar</button>
                                                        @else
                                                            @if(strtotime($tomorrowDate) < strtotime($reserva->horaCompleta))
                                                                <button type="button" class="btn btn-sm btn-danger btnEliminar" data-toggle="modal" data-target="#modalLiberarHora" onclick="setidHora({{ $paciente->id }}, {{ $reserva->idHora }})"><i class="fa fa-minus-circle" aria-hidden="true"></i> Liberar</button>
                                                            @else
                                                                <button type="button" class="btn btn-sm btn-danger btnEliminar" data-toggle="modal" data-target="#modalWarningLiberar"><i class="fa fa-minus-circle" aria-hidden="true"></i> Liberar</button>
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <!-- Modal -->
                                    <div class="modal fade" id="modalLiberarHora" tabindex="-1" role="dialog" aria-labelledby="liberarModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="liberarModalLabel">Liberar reserva de hora</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Para liberar la hora reservada debes hacer click en el botón "Continuar". Si no deseas continuar, haz clic en el botón "Cancelar"
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                    <button type="button" class="btn btn-primary" id="btnLiberarHora">Continuar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Modal -->
                                    <div class="modal fade" id="modalWarningLiberar" tabindex="-1" role="dialog" aria-labelledby="WarningliberarModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="WarningliberarModalLabel">Liberar reserva de hora</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    No es posible liberar esta reserva. Para liberar una hora debe hacerlo 24hrs antes de su comienzo.
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Entendido</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @if($countReservas >= $limiteDeReservas)
                                        <div class="alert alert-warning" role="alert">
                                            <h4 class="alert-heading">
                                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ¡Atención!
                                            </h4>
                                            <p>Usted ya ha alcanzado el máximo de agendamientos que puede realizar. Si desea reservar una nueva hora debe liberar una de las horas que ya tiene agendada, haciendo clic en el boton "Liberar"</p>
                                        </div>
                                    @endif
                                @else
                                    <div class="alert alert-info" role="alert">
                                        El paciente no tiene horas de teleconsulta reservadas.
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Historial del paciente <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-12">
                                <p class="text-justify">
                                    Historial de reservas de horas y de atenciones de Salud Mental en Telecetep.
                                </p>
                            </div>
                        </div>
                        <hr>

                        <div class="row d-flex justify-content-center mt-5">
                            <div class="col-md-10">
                                <h3>Historial de reservas</h3>
                                <table id="tablaHistorial" name="tableHistorial" class="display compact" style="width: 100%">
                                    <thead class="thead-light">
                                    <tr>
                                        <td>Hora</td>
                                        <td>Fecha</td>
                                        <td>Profesional</td>
                                        <td>Especialidad</td>
                                        <td>Prestación</td>
                                        <td>Asiste</td>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" id="cardUpdateInfo" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Actualizar información <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                    </h2>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-justify">
                                    Para una mejor atención, favor mantener datos del paciente actualizados.
                                </p>
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-md-10 m-auto">

                                <h3>Actualizar información</h3>
                                <form  class="form-control" action="{{ url('/mis_datos/'.$paciente->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}

                                    <div class="form-row">
                                        <div class="col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <label for="nombres">Nombres</label>
                                                @if($errors->any())
                                                    <input type="text" name="nombres" class="form-control" placeholder="Nombre" value="{{ old('nombres') }}">
                                                @else
                                                    <input type="text" name="nombres" class="form-control" placeholder="Nombre" value="{{ $paciente->nombres }}">
                                                @endif
                                            </div>

                                        </div>
                                        <div class="col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <label for="apellidoPaterno">Apellido Paterno</label>
                                                @if($errors->any())
                                                    <input type="text" name="apellidoPaterno" class="form-control" placeholder="Apellido Paterno" value="{{ old('apellidoPaterno') }}">
                                                @else
                                                    <input type="text" name="apellidoPaterno" class="form-control" placeholder="Apellido Paterno" value="{{ $paciente->apellidoPaterno }}">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <label for="apellidoMaterno">Apellido Materno</label>
                                                @if($errors->any())
                                                    <input type="text" name="apellidoMaterno" class="form-control" placeholder="Apellido Materno" value="{{ old('apellidoMaterno') }}">
                                                @else
                                                    <input type="text" name="apellidoMaterno" class="form-control" placeholder="Apellido Materno" value="{{ $paciente->apellidoMaterno }}">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <label for="rut">RUT</label>
                                                <input type="text" name="rut" class="form-control" placeholder="RUT" value="{{ \App\Http\Controllers\PacienteController::formatoRut($paciente->rut) }}" disabled>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <label for="email">Correo electrónico</label>
                                                @if($errors->any())
                                                    <input type="text" name="email" class="form-control" placeholder="Correo electrónico" value="{{ old('email') }}">
                                                @else
                                                    <input type="text" name="email" class="form-control" placeholder="Correo electrónico" value="{{ $paciente->email }}">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <label for="celular">Teléfono celular</label>
                                                @if($errors->any())
                                                    <input type="text" name="celular" class="form-control" placeholder="Teléfono de contacto" value="{{ old('celular') }}">
                                                @else
                                                    <input type="text" name="celular" class="form-control" placeholder="Teléfono de contacto" value="{{ $paciente->celular }}">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btnContent d-flex justify-content-end">
                                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('jsScripts')
<script>
    $(fucntion(){

    })
    $("nav ul li a").removeClass('active');
    $(".menuPaciente").addClass('active');

    $('li:nth-child(3n)').next().css({'clear': 'both'});

    $(".card .btn").click(function(){
        $('.card .card-header .fa').removeClass('fa-angle-up');
        $('.card .card-header .fa').addClass('fa-angle-down');

        if($(this).closest('.card').find('.card-body').is(':visible') === false){
            $(this).find('.fa').removeClass('fa-angle-down');
            $(this).find('.fa').addClass('fa-angle-up');
        } else{
            $(this).find('.fa').removeClass('fa-angle-up');
            $(this).find('.fa').addClass('fa-angle-down');
        }
    });

    $("#updateInfoHeader").click(function(){
        if($("#cardUpdateInfo").closest('.card').find('.card-body').is(':visible') === false){
            $("#cardUpdateInfo").click();
        }
        expandirInfo();

        setTimeout(function(){
            $('html, body').animate({
                scrollTop: $("#cardUpdateInfo").closest('.card').offset().top
            }, 500);
        }, 700);

    });
</script>

<script>
    function expandirInfo(){
        if($(".infoAdicional").is(':hidden')){
            $(".infoAdicional").slideToggle('slow');
            $(".expandirInfoPaciente").addClass('rotar');
        }
        else if($(".infoAdicional").is(':visible')){
            $(".infoAdicional").slideToggle('slow');
            $(".expandirInfoPaciente").removeClass('rotar');
        }
    }

    $(function($) {
        jQuery(document).ready(function() {
            jQuery.extend( jQuery.fn.dataTableExt.oSort, {
                "date-uk-pre": function ( a ) {
                    var ukDatea = a.split('-');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                },
                "date-uk-asc": function ( a, b ) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },
                "date-uk-desc": function ( a, b ) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            } );

            /* TABLA SOLICITUDES */
            var tablaHistorial = $('#tablaHistorial').DataTable({
                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "No hay datos disponibles",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "buttons": {
                        "copy": "Copiar",
                        "colvis": "Visibilidad"
                    }
                },
                dom: '<"top"<"float-right"l>>rtip',
                pageLength: 10,
                scrollCollapse: true,
                scrollX: true,
                autoWidth: true,
                ajax: {
                    type: 'GET',
                    url: '/mi_historial_ajax/',
                    data: {
                        'id_paciente': {!! $paciente->id !!},
                    },
                    'dataSrc': function(json){
                        return json.data;
                    }
                },
                columns: [
                    {data: "hora"},
                    {data: "fecha"},
                    {data: "prestador"},
                    {data: "especialidad"},
                    {data: "prestacion"},
                    {data: "asiste"},
                ],
                aoColumnDefs: [
                    {"type": "date-uk", "aTargets": [1]},
                ],
                order: [[1, "desc"]],
                deferRender: true

            });

        });
    });
</script>
@endsection