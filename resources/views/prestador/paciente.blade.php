@extends('prestador.layout')

@section('customCss')
    <link rel="stylesheet" href="{{ asset('css/prestador/agendamiento.css') }}">
@endsection


@section('content')

    <div class="row">
        <div class="col-md-12">
            <h2 id="pageTitle">Perfil del Paciente</h2>
            <hr>
        </div>
    </div>
    <div class=" row">
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-4">
                    <form action="">
                        <div class="form-group">
                            <span class="stepNumber"><em class="active">1</em> <label for="rutPaciente">Rut del paciente</label></span>
                            <div class="input-group">
                                <input type="number" name="rutPaciente" class="form-control" placeholder="Sin puntos, guión ni dígito verificador" autocomplete="off">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2" onclick="buscarPaciente()"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="formRegistroPaciente">
                <hr>
                <div class="row mt-4">
                    <div class="col-md-4">
                        <div class="form-group">
                            <span class="stepNumber"><em>2</em> <label for="nombres">Nombres</label></span>
                            <input type="text" name="nombres" class="form-control" placeholder="Ingrese nombre del paciente" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <span class="stepNumber"><em>3</em> <label for="apellidoPaterno">Apellido paterno</label></span>
                            <input type="text" name="apellidoPaterno" class="form-control" placeholder="Apellido Paterno" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <span class="stepNumber"><em>4</em> <label for="apellidoMaterno">Apellido materno</label></span>
                            <input type="text" name="apellidoMaterno" class="form-control" placeholder="Apellido Materno" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-4">
                        <div class="form-group">
                            <span class="stepNumber"><em>5</em> <label for="correoElectronico">Correo electrónico</label></span>
                            <input type="text" name="correoElectronico" class="form-control" placeholder="Correo del paciente" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <span class="stepNumber"><em>6</em> <label for="telefonoFijo">Teléfono fijo</label></span>
                            <input type="text" name="telefonoFijo" class="form-control" placeholder="Ingrese teléfono fijo del paciente" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <span class="stepNumber"><em>7</em> <label for="telefonoCelular">Teléfono celular</label></span>
                            <input type="text" name="telefonoCelular" class="form-control" placeholder="Ingrese teléfono celular del paciente" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="btnContent d-flex justify-content-end">
                    <button type="button" class="btn btn-info" onclick="registrarPaciente()">Registrar paciente</button>
                </div>
                <div class="erroresContent row">
                    <div class="col-md-12">
                        <div class="alert alert-danger">
                            <h5>¡Atención!</h5>
                            <ul style="list-style: none; margin: 0; padding: 0">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('jsScripts')
    <script>
        $("nav ul li a").removeClass('active');
        $(".menuPaciente").addClass('active');

        $("input[name='rutPaciente']").keydown(function(e){

            var inputValue = $("input[name='rutPaciente']").val();

            if(e.keyCode === 38 || e.keyCode === 40){e.preventDefault()}
            else if(e.key === '.' || e.key === ',' || e.key === '-' || e.key === 'e' || e.key === 'E'){ e.preventDefault();}
            else if(e.key === "Enter"){e.preventDefault(); buscarPaciente();}
            else if(e.key !== 'Backspace' && inputValue.length > 7){e.preventDefault();}
        });

        $("input").focusin(function(){
            $("span.stepNumber em").removeClass('active');
            if($(this).parent().hasClass('input-group')){
                $(this).parent().parent().children('span').children('em').addClass('active');
            } else {
                $(this).parent().children('span').children('em').addClass('active');
            }
        });
    </script>
    <script>
        function showFormRegistroPaciente(){
            $(".formRegistroPaciente input").val('');
            if($(".formRegistroPaciente").is(':hidden')){
                $(".formRegistroPaciente").slideToggle('fast');
            }
        }
        function buscarPaciente(){
            var rutPaciente = $("input[name='rutPaciente']").val();

            $.ajax({
                type: "GET",
                url: '/paciente/buscarPacienteAjax',
                data: {
                    'rut': rutPaciente
                },
                success: function(response){
                    if(response.pacienteExists){
                        var url = "{{route('showPerfilPaciente', ['prestador' => $prestador, 'paciente' => ":idPaciente"])}}";
                        url = url.replace(':idPaciente', response.paciente.id);
                        window.location.href = url;
                    } else{
                        showFormRegistroPaciente();
                    }
                },
                error: function(x,y,z){
                    console.log(x,y,z);
                }
            });
        }
        function showErrores(errores){
            if($(".erroresContent").is(':hidden')){$(".erroresContent").slideToggle('fast');}

            $(".erroresContent ul").html('');
            $.each(errores, function(key, error){
                $(".erroresContent ul").append('<li>'+error+'</li>');
            })
        }
        function registrarPaciente(){
            var rut = $("input[name='rutPaciente']").val();
            var nombres = $("input[name='nombres']").val();
            var apellidoPaterno = $("input[name='apellidoPaterno']").val();
            var apellidoMaterno = $("input[name='apellidoMaterno']").val();
            var correoElectronico = $("input[name='correoElectronico']").val();
            var telefonoFijo = $("input[name='telefonoFijo']").val();
            var telefonoCelular = $("input[name='telefonoCelular']").val();

            $.ajax({
                type: "GET",
                async: false,
                url: '/paciente/registrar',
                data: {
                    'rut': rut,
                    'nombres' : nombres,
                    'apellidoPaterno' : apellidoPaterno,
                    'apellidoMaterno' : apellidoMaterno,
                    'email' : correoElectronico,
                    'telefono' : telefonoFijo,
                    'celular' : telefonoCelular
                },
                success: function(jsonResponse){
                    console.log(jsonResponse);
                    if(jsonResponse.registered){
                        alertify.alert('Registrar Paciente',
                            'Se ha registrado al paciente '+nombres+' '+apellidoPaterno+' '+apellidoMaterno+' exitosamente. Presiona \'Continuar\' para seguir con el agendamiento.',
                            function(){
                                var url = "{{route('showPerfilPaciente', ['prestador' => $prestador, 'paciente' => ":idPaciente"])}}";
                                url = url.replace(':idPaciente', jsonResponse.paciente.id);
                                window.location.href = url;
                            })
                            .set('label', 'Continuar');
                    } else{
                        showErrores(jsonResponse.errors);
                    }
                },
                error: function(x,y,z){
                    console.log(x,y,z);
                }
            });
        }
    </script>
@endsection