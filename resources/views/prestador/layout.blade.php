<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Telecetep | Grupo Cetep</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,700&display=swap" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Fontawesome CSS-->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- jQuery UI -->
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.structure.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.theme.min.css') }}">

    <!-- include the style -->
    <link rel="stylesheet" href="{{ asset('css/alertify.min.css')}}" />
    <!-- include a theme -->
    <link rel="stylesheet" href="{{ asset('css/themes/default.min.css')}}" />

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- DataTable CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>

    @yield('customCss')

</head>
<body>

<header>
    <div class="d-flex justify-content-between align-items-center mobileHeader">
        <figure class="navLogo">
            <img src="{{ asset('grupo_cetep_no_name.png') }}" alt="Logo Cetep">
        </figure>
        <h1>Telecetep</h1>

        <a href="javascript:void(0)" onclick="toggleMenu()" class="btnNav"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <nav class="mobileNav">
            <ul>
                <li><a href="{{ route('welcomePrestador', $prestador) }}" class="menuInicio active">Inicio</a></li>
                <li><a href="{{ route('showPacientePrestador') }}" class="menuAgendamiento">Paciente</a></li>
                <li><a href="{{ route('showUpdatePasswordPrestador', $prestador) }}" class="menuAdministrar_perfil">Administrar perfil</a></li>
                <li><a href="{{ route('logout') }}">Cerrar sesión</a></li>
            </ul>
        </nav>
    </div>

    <div class="desktopHeader">
        <nav class="sideNav">
            <div class="navHeader">
                <figure class="navLogo">
                    <img src="{{ asset('grupo_cetep_no_name.png') }}" alt="Logo Cetep">
                </figure>
                <h1>Telecetep</h1>

                <ul>
                    <li><a href="{{ route('welcomePrestador', $prestador) }}" class="menuInicio active">Inicio</a></li>
                    <li><a href="{{ route('showPacientePrestador') }}" class="menuPaciente">Paciente</a></li>
                </ul>
            </div>
        </nav>
        <div class="headerContent">
            <h2>¡Hola, {{$prestador->nombres}} {{$prestador->apellidoPaterno}} {{$prestador->apellidoMaterno}}!</h2>
        </div>
        <div class="btnContent navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle btn-logout" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> Mi perfil</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('showUpdatePasswordPrestador', $prestador)}}">Cambiar contraseña</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}">Cerrar sesión</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</header>

<div class="content">
    @yield('content')
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- DataTable JS -->
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.19/sorting/date-uk.js"></script>

<!-- jQuery UI JS -->
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<!-- Alertify JS -->
<script src="{{ asset('js//alertify.min.js')}}"></script>
{{-- Custom JS --}}
<script src="{{ asset('js/menu.js') }}"></script>
@yield('jsScripts')
</body>
</html>