<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**Grupo de rutas para recuperar password
 * Y registro
 * Marcello López Cáceres
 */
// Inicio 
Route::group([
    'namespace' => 'Auth',    
    'middleware' => 'api',    
    'prefix' => 'password'
], function () {    
    Route::post('create', 'PasswordResetController@create');
   
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
    Route::get('register', 'RegisterController@reset');
    Route::post('register', 'RegisterController@reset');


});
    Route::get('register_paciente/{id}', 'Auth\RegisterPacienteController@showRegistrationForm')->name('register_paciente');
    Route::post('register_paciente', 'Auth\RegisterPacienteController@register')->name('register_paciente');

Route::get('activar_cuenta/{token}', 'Auth\ActivarCuentaController@reset');
// Fin

Auth::routes();

Route::get('/pruebas', 'welcomeController@pruebas')
    ->name('pruebas');

Route::get('/paciente', function (){
    return redirect()->route('welcome');
});
Route::get('/prestador', function (){
    return redirect()->route('welcomePrestador');
});

Route::get('/', 'WelcomeController@welcome')
    ->name('welcome')
    ->middleware('auth:paciente');

Route::get('/prestador/{prestador}', 'WelcomeController@welcomePrestador')
    ->where('prestador', '\d+')
    ->name('welcomePrestador');

// -------------------------------------------------------

Route::get('/login', 'Auth\LoginController@showPacienteLoginForm');
Route::get('/login/prestador', 'Auth\LoginController@showPrestadorLoginForm')
    ->name('showLoginPrestador');
Route::get('/login/paciente', 'Auth\LoginController@showPacienteLoginForm')
    ->name('showLoginPaciente');
Route::post('/login', 'Auth\LoginController@pacienteLogin');
Route::post('/login/prestador', 'Auth\LoginController@prestadorLogin');
Route::post('/login/paciente', 'Auth\LoginController@pacienteLogin');

Route::get('/createPassword/prestador', 'Auth\ResetPasswordController@showCreatePasswordPrestador')
    ->name('showCreatePasswordPrestador')
    ->middleware('auth:prestador');
Route::get('/createPassword/paciente', 'Auth\ResetPasswordController@showCreatePasswordPaciente')
    ->name('showCreatePasswordPaciente')
    ->middleware('auth');
Route::put('/createPassword/paciente', 'Auth\ResetPasswordController@createPasswordPaciente');
Route::put('/createPassword/prestador', 'Auth\ResetPasswordController@createPasswordPrestador');

Route::get('/updatePassword/paciente/', 'Auth\ResetPasswordController@showUpdatePasswordPaciente') /*PROTEGIDA*/
    ->name('showUpdatePasswordPaciente');
Route::get('/updatePassword/prestador/{prestador}', 'Auth\ResetPasswordController@showUpdatePasswordPrestador') /*PROTEGIDA*/
    ->name('showUpdatePasswordPrestador');

Route::get('/logout', 'Auth\LoginController@logout')
    ->name('logout');

// -------------------------------------------------------
/** --------------------------------------------
 * PACIENTE
 */
Route::get('/mis_consultas/', 'HoraController@horasReservadas')
    ->where('paciente', '\d+')
    ->name('mis_consultas');

Route::get('/mis_datos/', 'PacienteController@mis_datos')
    ->name('mis_datos')
    ->middleware('auth');

Route::put('/mis_datos/', 'PacienteController@actualizar_mis_datos')
    ->name('actualizar_mis_datos')
    ->middleware('auth');

Route::get('/mi_historial/', 'PacienteController@historialDelPaciente')
    ->name('mi_historial')
    ->middleware('auth');

Route::get('/mi_historial_ajax/', 'PacienteController@historialDelPaciente_ajax')
    ->name('mi_historial_ajax');
// -------------------------------------------------------

// -------------------------------------------------------

Route::get('/reservaHora/', 'HoraController@reservaHora')
    ->name('reservaHora')
    ->middleware('auth');

Route::get('/getPrestacionesConvenioAjax', 'ConvenioController@getPrestacionesConvenio_ajax')
    ->name('getPrestacionesConvenioAjax');

Route::get('/getPrestadoresPrestacionAjax', 'ConvenioController@getPrestadoresPrestacion_ajax')
    ->name('getPrestadoresPrestacionAjax');

Route::get('/agendaPrestadorAjax', 'HoraController@agendaPrestadorAjax')
    ->name('agendaPrestadorAjax');

Route::get('/horasPrestadorAjax', 'HoraController@horasAjax')
    ->name('horasPrestadorAjax');

Route::get('/validarReservaConReglasAjax', 'HoraController@validarReservaConReglasAjax')
    ->name('validarReservaConReglasAjax');

Route::get('/detalleReserva/{idHora}/{idPrestacion}/{idConvenio}', 'HoraController@detalleReserva')
    ->name('detalleReserva')
    ->middleware('auth');

Route::get('/confirmarReserva/{idHora}/{idConvenio}/{idPrestacion}', 'HoraController@confirmarReserva')
    ->name('confirmarReserva')
    ->middleware('auth');

Route::get('/validarStillAvailableAjax', 'HoraController@validarStillAvailableAjax')
    ->name('validarStillAvailableAjax');

Route::get('/mensajeConfirmacion/{flag}', 'HoraController@mensajeConfirmacion')
    ->where('flag', '\d+')
    ->name('mensajeConfirmacion')
    ->middleware('auth');

Route::get('/ifAllowToLiberarAjax', 'HoraController@ifAllowToLiberarAjax')
    ->name('ifAllowToLiberarAjax');

Route::get('/liberarHoraAjax', 'HoraController@liberarHoraAjax')
    ->name('liberarHoraAjax');


// -------------------------------------------------------

// -------------------------------------------------------

Route::get('/teleconsulta/{hora}', 'TeleconsultaController@viewTeleconsulta')
    ->where('hora', '\d+')
    ->name('teleconsulta')
    ->middleware('auth');

/** --------------------------------------------
 * PRESTADOR
 */

Route::get('/prestador/paciente', 'HoraController@showPacientePrestador')
    ->name('showPacientePrestador')
    ->middleware('auth:prestador');
Route::get('/perfilPaciente/{prestador}/{paciente}', 'PacienteController@showPerfilPaciente')
    ->name('showPerfilPaciente')
    ->middleware('auth:prestador');

/**
 * AJAX
 */

Route::get('/paciente/buscarPacienteAjax', 'PacienteController@buscarPacienteAjax')
    ->name('buscarPacienteAjax');
Route::get('/paciente/registrar', 'PacienteController@registrarPacienteAjax')
    ->name('registrarPacienteAjax');

/**
 * TRANSBANK
 */
Route::post('/comprobante/returnpago/{idHora}/{idConvenio}/{idPrestacion}', 'TransbankController@getReturnTransaction')
    ->name('getReturnTransaction');

Route::post('/comprobante/returnsuccess/{idHora}/{idConvenio}/{idPrestacion}', 'TransbankController@getResultTransaction')
    ->name('getResultTransaction');


Route::get('/clearcache', 'TransbankController@clearcache')
    ->name('clearcache');

Route::get('/atencion/{idHora}', 'TeleconsultaController@atencionExterna')
    ->name('atencionExterna');

//Route::get('/confirmacionPago', 'TransbankController@reservaHora')
//    ->name('confirmacionPago');


/*BONO
*/


Route::get('/administrarBonos', 'WelcomeController@bono');

Route::post('/ingresoBono','HoraController@ingBono')
    ->name('ingresoBono');

Route::post('/rechazarBono','HoraController@rechazarHoraBonoAjax')
    ->name('rechazarBono');

Route::post('/liberarBono','HoraController@liberarHoraBonoAjax')
    ->name('liberarBono');

Route::post('confirmarBonos','HoraController@confirmarBonos')
    ->name('confirmarBonos');

Route::get('repago','HoraController@repagoBono')
    ->name('repago');

Route::get('/getInfoHoraBono', 'HoraController@getInfoHoraBono')
    ->name('getInfoHoraBono');

Route::get('/getInfoPagarHora','HoraController@getInfoPagarHora')
    ->name('getInfoPagarHora');

//--------------------------------------------------------
//--------------------------------------------------------