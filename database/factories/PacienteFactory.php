<?php

use Faker\Generator as Faker;

$factory->define(App\Paciente::class, function (Faker $faker) {
    return [
        'rut' => strval($faker->unique()->numberBetween(3000000,25000000)),
        'dv' => $faker->numberBetween(0,9),
        'nombre' => $faker->firstName,
        'apellido_paterno' => $faker->lastName,
        'apellido_materno' => $faker->lastName,
        'isapre_id' => $faker->numberBetween(1,16),
        'fecha_nacimiento' => $faker->dateTimeBetween('1930-01-01', '2012-12-31'),
        'email' => $faker->email,
        'telefono' => $faker->phoneNumber
    ];
});
