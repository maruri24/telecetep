<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsapreIdToPaciente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pacientes', function(Blueprint $table){
            $table->unsignedInteger('isapre_id')->default(0)->after('apellido_materno');
            $table->foreign('isapre_id')->references('id')->on('isapres');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('pacientes', function (Blueprint $table){
            $table->dropForeign(['isapre_id']);
            $table->dropColumn('isapre_id');
        });
    }
}
