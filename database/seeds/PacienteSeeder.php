<?php

use App\Entities\Isapre;
use App\Entities\Paciente;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PacienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Paciente::create([
            'rut' => '18462396',
            'dv' => 'K',
            'nombre' => 'Lucas',
            'apellido_paterno' => 'Maruri',
            'apellido_materno' => 'Vivanco',
            'isapre_id' => Isapre::where('isapre', 'Consalud')->value('id'),
            'fecha_nacimiento' => '1993-06-24',
            'email' => 'maruri24@gmail.com',
            'telefono' => '934224417'
        ]);
        factory(Paciente::class, 50)->create();
    }
}
