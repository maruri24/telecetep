<?php

use App\Entities\Paciente;
use App\Entities\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $paciente = Paciente::where('rut', '18462396');

        factory(User::class)->create([
            'username' => $paciente->value('rut'),
            'paciente_id' => $paciente->value('id'),
            'password' => bcrypt('Cetep626')
        ]);
    }
}
