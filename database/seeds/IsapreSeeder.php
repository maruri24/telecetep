<?php

use App\Isapre;
use Illuminate\Database\Seeder;

class IsapreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        Isapre::create([
            'isapre' => "sin isapre",
        ]);
        Isapre::create([
            'isapre' => "Banmedica",
        ]);
        Isapre::create([
            'isapre' => "Vida Tres",
        ]);
        Isapre::create([
            'isapre' => "Cruz Blanca",
        ]);
        Isapre::create([
            'isapre' => "Colmena",
        ]);
        Isapre::create([
            'isapre' => "Mas Vida",
        ]);
        Isapre::create([
            'isapre' => "Fonasa",
        ]);
        Isapre::create([
            'isapre' => "Consalud",
        ]);
        Isapre::create([
            'isapre' => "Fundación",
        ]);
        Isapre::create([
            'isapre' => "Cruz del Norte",
        ]);
        Isapre::create([
            'isapre' => "Particular",
        ]);
        Isapre::create([
            'isapre' => "Achs",
        ]);
        Isapre::create([
            'isapre' => "Rio Blanco",
        ]);
        Isapre::create([
            'isapre' => "San Lorenzo",
        ]);
        Isapre::create([
            'isapre' => "Chuquicamata",
        ]);
        Isapre::create([
            'isapre' => "Fusat",
        ]);
    }
}
